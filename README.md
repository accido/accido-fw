###Example simple domain structure:

    class Application extends Model {
      //... defintions
      protected function init(){
        $this->handle('example/post/content')
          ->done(function(View $view, Controller $ctrl, Model $_this){
            return array($ctrl, $ctrl->request->stream('parameter.id', FILTER_VALIDATE_INT), $view, $model);
          })
          ->iterate()
          ->then(function($ctrl, $id, $view, $model){
            $view->title  = 'Post ID ' . $id;
            $data         = Controller::execute('sql/post', $id);
            $post         = $model->register('Post\\View', $data['id'], $view);
            //... do something
            return array($ctrl, $view, $model);
          })
          ->then($this->register('Render\\Header'))
          ->then($this->register('Render'));

      }
    }

    class View /* Post\View */ extends Model{
      //....
      public function capture(Injector $inj, $id, View $view){
        $this->ensure(3 !== count(func_get_args()), 'Model Post\\View capturing requered 2 parameters.');
        $post             = $this->register('Post', $id);
        //... binded $view to $post
      }
    }

    class Post extends Model{
      //...
      public function capture(Injector $inj, $id){
        $this->ensure(2 !== count(func_get_args()), 'Model Post capturing requered 1 parameters.');
        //...
      }
    }

###Application/Domain structure

    Dic(Global Domain and Injector)
      Model (Local Domain and Injector)
      Model (Local ...
      Model (Local ...
      ... ... ...

###Method for capturing data on the fly

     abstract/* realy not*/ function Model::capture(Injector $injector, ... $args)
     //instance of current model capture place in Global Domain structure
     //and can define capture local Domain too


####P.S. But it's not Object Relations Mapping(ORM),

because not exists any mapper layer attached to DB and Domain for granted

it support pseudo mapping out of any ORM's patterns and maybe can says

mapping as is between DB and Domain not existing.

And yes, application can take data from DB(or other) without any mapping between,

*IT* means data *CAN* be used on the *FLY*.

I wish all flights creativity.

Thank you for your attention.
