<?php
namespace Accido;
use Accido\Model;
use Accido\Promise;
use Accido\Models\Utils;
use Exception,stdclass,Closure,Generator;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Option
 *
 * @package Option
 * 
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 andrew scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2013 <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Option {
  const ATTR_NONE                                           = null;
  const ATTR_THEN                                           = 1;
  const ATTR_ITERATE                                        = 2;
  const ATTR_REDUCE                                         = 4;
  const ATTR_APPLY                                          = 8;
  const ATTR_FILTER                                         = 16;
  const ATTR_EFFECT                                         = 32;
  const ATTR_MERGE                                          = 64;
  const ATTR_WHEN                                           = 128;
  const ATTR_TICK_TIMEOUT                                   = 17;

  /**
   * callbacks
   *
   * @var array
   */
  public $callbacks                                         = array();
  /**
   * storage
   *
   * @var mixed
   */
  public $storage                                           = null;
  /**
   * sleep
   *
   * @var bool
   */
  public $sleep                                             = true;
  /**
   * merged
   *
   * @var array
   */
  public $merged                                            = array();
  /**
   * ancestor
   *
   * @var Option|null
   */
  public $ancestor                                          = null;
  /**
   * is_commit
   *
   * @var bool
   */
  public $is_commit                                         = false;
  /**
   * no_commit
   *
   * @var mixed
   */
  public $no_commit                                         = false;
  /**
   * filter
   *
   * @var string|array|int
   */
  public $filter                                            = '';
  /**
   * option
   *
   * @var int
   */
  public $option                                            = '';
  /**
   * _done
   *
   * @var callable
   */
  public $_done                                             = null;
  /**
   * _fail
   *
   * @var callable
   */
  public $_fail                                             = null;
  /**
   * is_fail
   *
   * @var bool
   */
  public $is_fail                                           = false;
  /**
   * key
   *
   * @var mixed
   */
  public $key                                               = null;
  /**
   * utils
   *
   * @var \Accido\Models\Utils
   */
  public $utils                                             = null;
  /**
   * resolve_array
   *
   * @var boolean
   */
  public $resolve_array                                     = false;
  /**
   * debug_type
   *
   * @var mixed
   */
  public $debug_type                                        = null;
  /**
   * next_tick
   *
   * @static
   *
   * @var callable
   */
  public static $next_tick                                  = null;

  /**
   * init
   * 
   * @param mixed $key
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function init($key = null){
    $dic                                                    = Model::$dic;
    if(!$dic->exists('option/utils')){
      $dic->add('option/utils', $dic->reuse(function(){
          return new Utils;
        }));
    }
    $this->utils                                            = $dic->get('option/utils');
    $this->key                                              = $key;
    $this->sleep                                            = true;
    $this->merged                                           = array();
    $this->callbacks                                        = array();
    $this->ancestor                                         = null;
    $this->is_commit                                        = false;
    $this->no_commit                                        = false;
    $this->storage                                          = null;
    $this->_done                                            = null;
    $this->_fail                                            = null;
    if(!empty($key)){
      $this->is_fail                                        = false;
      $object                                               = new stdclass;
      $object->detail                                       = $key;
      $object->done                                         = true;
      $this->storage                                        = $object;
      $this->sleep                                          = false;
    }
  }

  /**
   * async
   * 
   * @param callable $callback
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function async($callback){
    if(!empty(self::$next_tick)){
      self::$next_tick->send($callback);
    }
    else{
      call_user_func($callback);
    }
  }

  /**
   * resolve_callable
   * 
   * @param callable $callback
   * @param callable $ret
   * @param mixed $arg
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function resolve_callable($callback, $ret, $arg = null){
    $object                                                 = new stdclass;
    if(!$this->is_fail){
      $object->done                                         = true;
    }
    try{
      $object->detail                                       = 
        $this->resolve_array ? call_user_func_array($callback, $arg) : call_user_func($callback, $arg);
      if($object->detail instanceof Generator){
        $this->resolve_generator($object->detail, $ret);
        return;
      }
    }
    catch(\Accido\Exceptions\Option $error){
      $this->is_fail                                        = true;
      $object->done                                         = false;
      unset($object->done);
      $object->detail                                       = $error->detail;
    }
    catch(\Exception $error){
      $this->is_fail                                        = true;
      $object->done                                         = false;
      unset($object->done);
      $object->detail                                       = $error;
    }
    call_user_func($ret, $object);
  }

  /**
   * resolve_generator
   * 
   * @param Generator $data
   * @param callable|null $ret
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function resolve_generator(Generator $data, $ret = null){
    if(empty(self::$next_tick)){
      while($data->valid()){
        if($ret){
          call_user_func($ret, $data->current());
        }
        $data->next();
      }
      return;
    }
    $_this                                                  = $this;
    $fact                                                   = 0;
    $func                                                   = function() use (&$data, &$ret, &$fact, &$_this) {
      while($data->valid()){
        yield;
        $_this->async(function() use (&$data, &$ret, &$_this, &$fact){
          $object                                           = new stdclass;
          $object->detail                                   = $data->current();
          if(!$_this->is_fail){
            $object->done                                   = true;
          }
          if($ret){
            call_user_func($ret, $object);
          }
          $fact->next();
        });
        $data->next();
      }
    };
    $fact                                                   = $func();
    $fact->next();
  }

  /**
   * do_update
   * 
   * @param stdclass $event
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function do_update(stdclass $event){
    $this->sleep                                            = false;
    $this->storage                                          = $event;
    foreach($this->callbacks as $item){
      $this->do_callback($item['option'], clone $event);
    }
  }

  /**
   * do_callback
   * 
   * @param &Option $option
   * @param &stdclass $event
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function do_callback(Option &$option, stdclass &$event){
    $this->async(function() use (&$option, &$event){
      $option->update($event);
    });
  }

  /**
   * add_listener
   * 
   * @param callable|null $done
   * @param callable|null $fail
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function add_listener($done = null, $fail = null){
    $option                                                 = new Option();
    $option->_done                                          = $done;
    $option->_fail                                          = $fail;
    $option->ancestor                                       = $this;
    $this->callbacks[]                                      = array('option' => $option);
    if($this->sleep) return $option;
    $event                                                  = clone $this->storage;
    $this->async(function() use(&$option, &$event){
      $option->update($event);
    });
    return $option;
  }

  /**
   * resolve_filter
   * 
   * @param mixed $filter
   * @param mixed $option
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return array
   */
  public function resolve_filter($filter = null, $option = null){
    if(empty($filter)) return array('filter' => $filter, 'option' => $option);
    if($this->utils->is_callable($filter)){
      $option                                               = array('options' => $filter);
      $filter                                               = FILTER_CALLBACK;
    }
    elseif(FILTER_CALLBACK === $filter && $this->utils->is_callable($option)){
      $option                                               = array('options' => $option);
    }
    if(is_array($filter)){
      foreach($filter as $index => $value){
        if(is_array($value)){
          $args_filter                                      = $value['filter'];
          $args_option                                      = array();
          if(array_key_exists('options', $value)){
            $args_option['options']                         = $value['options'];
          }
          if(array_key_exists('flags', $value)){
            $args_option['flags']                           = $value['flags'];
          }
          $args_filter                                      = $this->resolve_filter($args_filter, $args_option);
          $filter[$index]                                   = $args_filter['option'];
          $filter[$index]['filter']                         = $args_filter['filter'];
          continue;
        }
        $temp                                               = $this->resolve_filter($value, null);
        $filter[$index]                                     = $temp['option'];
        $filter[$index]['filter']                           = $temp['filter'];
      }
      return array('filter' => $filter, 'option' => null);
    }
    if(is_string($filter) && preg_match('/^([^\pL\pN]).+?(?1)[xsimgXuUA]*$/um', $filter)){
      return array(
        'filter'                                            => FILTER_VALIDATE_REGEXP,
        'option'                                            => array('options' => array('regexp' => $filter)),
      );
    }
    return array('filter' => $filter, 'option' => $option);
  }

  /**
   * do_filter
   * 
   * @param mixed $filter
   * @param mixed $option
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Closure|Option
   */
  public function do_filter($filter = null, $option = null){
    if($filter instanceof Option) return $filter;
    $sanitized                                              = $this->resolve_filter($filter, $option);
    $filter                                                 = $sanitized['filter'];
    $option                                                 = $sanitized['option'];
    return function($var) use (&$filter, &$option){
      $return                                               = array();
      $confirm                                              = true;
      if(!empty($filter)){
        if(is_int($filter)){
          $boolean                                          = 
            (FILTER_VALIDATE_BOOLEAN === $filter && FILTER_NULL_ON_FAILURE === $option);
          $args                                             = array($var, $filter);
          if(!empty($option)){
            $args[]                                         = $option;
          }
          $var                                              = call_user_func_array('filter_var', $args);
          if((false === $var && !$boolean) || (null === $var && $boolean)){
            $confirm                                        = false;
            $var                                            = null;
          }
        }
        else{
          $temp                                             = filter_var_array($var, $filter);
          $var                                              = array();
          foreach($temp as $ident => $value){
            $boolean                                        = (FILTER_VALIDATE_BOOLEAN === $filter[$ident]['filter'] &&
              FILTER_NULL_ON_FAILURE === $filter[$ident]['flags']);
            if((false === $value && !$boolean) || (null === $value && $boolean)){
              $confirm                                      = false;
              $var[$ident]                                  = null;
            }
            else{
              $var[$ident]                                  = $value;
            }
          }
        }
      }
      return array('confirmed' => $confirm, 'value' => $var);
    };
  }

  /**
   * __construct
   * 
   * @param mixed $key
   * @uses init
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function __construct($key = null){
    $this->init($key);
  }

  /**
   * destroy
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function destroy(){
    if(!empty($this->ancestor) && $this->ancestor instanceof Option){
      foreach($this->ancestor->callbacks as $index => $item){
        if($item['option'] === $this){
          array_splice($this->ancestor->callbacks, $index, 1);
          break;
        }
      }
      $this->ancestor                                       = null;
    }
    $this->end();
    $this->init();
    return $this;
  }

  /**
   * do_then
   * 
   * @param mixed $data
   * @param Option|callable|null $done
   * @param Option|callable|null $fail
   * @param bool $resolve_array
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function do_then($data, $done = null, $fail = null, $resolve_array = true){
    $completed                                              = 0;
    $is_full_filled                                         = false;
    $err                                                    = 0;
    $len                                                    = count($this->merged) + 1;
    $arr                                                    = &$this->merged;
    $state                                                  = [];
    $result                                                 = new Option();
    $result->resolve_array                                  = $resolve_array;
    $result->_done                                          = $done;
    $result->_fail                                          = $fail;
    $trigger                                                = function() 
      use (&$len, &$completed, &$is_full_filled, &$err, &$state, &$result){
        if($len !== $completed || !$is_full_filled) return;
        $object                                             = new stdclass;
        $object->detail                                     = $state;
        if(0 === $err){
          $object->done                                     = true;
          $result->update($object);
          return;
        }
        $result->update($object);
      };
    $do_ensure                                              = function()
      use (&$err, &$result, &$state, &$is_full_filled){
        $object                                             = new stdclass;
        $object->detail                                     = $state;
        $is_full_filled                                     = true;
        if(0 === $err){
          $object->done                                     = true;
          $result->update($object);
          return;
        }
        $result->update($object);
      };
    $iterate                                                = function()
      use (&$len, &$completed, &$do_ensure){
        if($len === (++$completed)){
          $do_ensure();
        }
      };
    $set                                                    = function($index)
      use (&$state, &$arr, &$err, &$iterate, &$trigger){
        $failed                                             = false;
        $filled                                             = 0;
        $stream                                             = $arr[$index - 1];
        if($stream instanceof Option){
          $stream
            ->then(function($data) use (&$state, &$err, &$failed, &$iterate, &$trigger, &$index, &$filled){
              $state[$index]                                = $data;
              if($failed){
                $err--;
                $failed                                     = false;
              }
              if(!($filled++)) $iterate();
              else $trigger();
            }, function($reasone) use (&$state, &$err, &$failed, &$iterate, &$trigger, &$index){
              $state[$index]                                = $reasone;
              if(!$failed){
                $err++;
                $failed                                     = true;
              }
              if(!($filled++)) $iterate();
              else $trigger();
            })
            ->nocommit();
          return;
        }
        $state[$index]                                      = $stream;
        $iterate();
      };
    $state[0]                                               = $data;
    $iterate();
    for($jndex = 1; $jndex < $len; ++$jndex){
      $set($jndex);
    }
    return $result;
  }

  /**
   * then
   * 
   * @param Option|callable|null $done
   * @param Option|callable|null $fail
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function then($done = null, $fail = null){
    $_this                                                  = $this;
    $return                                                 = $this->add_listener(function($data)
      use (&$_this, &$done, &$fail){
        if((is_object($done) && !method_exists($done, '__invoke')) || !empty($_this->merged)){
          return $_this->do_then($data, $done, $fail);
        }
        elseif(!empty($done)){
          return call_user_func($done, $data);
        }
        else return $data;
      }, $fail);
    $return->debug_type                                     = self::ATTR_THEN;
    return $return;
  }

  /**
   * reduce
   * 
   * @param mixed $start_acc
   * @param Option|callable $callback
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function reduce($start_acc, $callback){
    $_this                                                  = $this;
    $init                                                   = false;
    $stemp                                                  = new Option();
    $result                                                 = null;
    $func                                                   = $stemp->then(function($callback)
      use (&$start_acc, &$init, &$result){
        if(is_object($callback) && isset($callback->done))
          $callback                                         = $callback->done;
        elseif(is_object($callback) && method_exists($callback, 'done'))
          $callback                                         = array($callback, 'done');
        elseif(is_array($callback) && isset($callback['done']))
          $callback                                         = $callback['done'];
        else
          $callback                                         = $callback['event'];
        $return                                             = new stdclass;
        $return->done                                       = function($state)
          use (&$start_acc, &$callback, &$init, &$result){
            $accum                                          = $start_acc;
            $index                                          = 1;
            $data                                           = array_shift($state);
            foreach($state as $index => $item){
              $accum                                        = $callback($accum, $item, $data, $index);
            }
            if($init){
              array_splice($result->merged, -1, 1);
            }
            $init                                           = true;
            $result->listen($accum);
            return $data;
          };
        return $return;
      });
    $result                                                 = $this->add_listener(function($data)
      use (&$func, &$_this){
        return $_this->do_then($data, $func, null, false);
      });
    $result->debug_type                                     = self::ATTR_REDUCE;

    $object                                                 = new stdclass;
    $object->done                                           = true;
    if($callback instanceof Option){
      $object->detail                                       = &$callback;
    }
    else{
      $object->detail                                       = array('event' => &$callback);
    }
    $stemp->update($object);

    return $result;
  }

  /**
   * iterate
   * 
   * @param int $pos
   * @param Option|callable|null $callback
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function iterate($pos = 0, $callback = null){
    $_this                                                  = $this;
    $init                                                   = false;
    $last                                                   = 0;
    $stemp                                                  = new Option();
    $result                                                 = null;
    $func                                                   = $stemp->then(function($callback)
      use (&$pos, &$last, &$init, &$result){
        if(is_object($callback) && isset($callback->done))
          $callback                                         = $callback->done;
        elseif(is_object($callback) && method_exists($callback, 'done'))
          $callback                                         = array($callback, 'done');
        elseif(is_array($callback) && isset($callback['done']))
          $callback                                         = $callback['done'];
        else
          $callback                                         = $callback['event'];
        $return                                             = new stdclass;
        $return->done                                       = function($state)
          use (&$pos, &$last, &$callback, &$init, &$result){
            $ctrl                                           = null;
            $index                                          = 0;
            $pointer                                        = is_numeric($pos) ? intval($pos) : 0;
            $count                                          = isset($state[$pointer]) && is_numeric($state[$pointer]) ? intval($state[$pointer]) : null;
            $calle                                          = !empty($callback);
            if(null !== $count){
              $state[$pointer]                              = range(0, $count - 1);
            }
            if(is_array($state[$pointer])){
              if($calle){
                $temp                                       = &$state[$pointer];
                foreach($temp as $index => &$item){
                  $item                                     = call_user_func($callback, $item);
                }
              }
              $state                                        =
                array_merge(array_splice($state, 0, $pointer), array_shift($state), $state);
            }
            $ctrl                                           = array_shift($state);
            $last                                           = count($state);
            if($init){
              array_splice($result->merged, -$last, $last);
            }
            foreach($state as $index){
              $result->listen($index);
            }
            $init                                           = true;
            return $ctrl;
          };
        return $return;
      });
    $result                                                 = $this->add_listener(function($data)
      use (&$func, &$_this){
        return $_this->do_then($data, $func, null, false);
      });
    $result->debug_type                                     = self::ATTR_ITERATE;

    $object                                                 = new stdclass;
    $object->done                                           = true;
    if($pos instanceof Option){
      $object->detail                                       = &$pos;
    }
    elseif($this->utils->is_callable($pos)){
      $object->detail                                       = array('event' => &$pos);
    }
    elseif($callback instanceof Option){
      $object->detail                                       = &$callback;
    }
    else{
      $object->detail                                       = array('event' => &$callback);
    }
    $stemp->update($object);

    return $result;
  }

  /**
   * apply
   * 
   * @param array|int $range
   * @param Option|callable|null $callback
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function apply($range = null, $callback = null){
    $_this                                                  = $this;
    $init                                                   = false;
    $stemp                                                  = new Option();
    $result                                                 = null;
    $func                                                   = $stemp->then(function($callback)
      use (&$range, &$init, &$result){
        if(is_object($callback) && isset($callback->done))
          $callback                                         = $callback->done;
        elseif(is_object($callback) && method_exists($callback, 'done'))
          $callback                                         = array($callback, 'done');
        elseif(is_array($callback) && isset($callback['done']))
          $callback                                         = $callback['done'];
        else
          $callback                                         = $callback['event'];
        $return                                             = new stdclass;
        $return->done                                       = function($state)
          use (&$range, &$callback, &$init, &$result){
            $args                                           = [];
            $index                                          = 0;
            $call                                           = !is_null($callback) ? $callback : $state[0];
            $temp_arr                                       = [];
            if(is_null($range)){
              $range                                        = [];
              for($index = 1; $index < count($state); ++$index){
                $range[]                                    = $index;
              }
            }
            foreach($range as $index => $item){
              if(isset($state[$item])){
                $args[$index]                               = $state[$item];
                continue;
              }
              $args[$index]                                 = null;
            }
            $temp                                           = array_flip($range);
            foreach($state as $index => $item){
              if(!isset($temp[$index])){
                $temp_arr[]                                 = $item;
              }
            }
            $last                                           = count($temp_arr);
            if($init){
              array_splice($result->merged, -$last, $last);
            }
            foreach($temp_arr as $index){
              $result->listen($index);
            }
            $init                                           = true;
            if($call instanceof Closure || (is_object($call) && method_exists($call, '__invoke'))){
              $data                                         = $call($args);
            }
            elseif(is_object($call) && method_exists($call, 'done')){
              $data                                         = $call->done($args);
            }
            elseif(is_object($call) && isset($call->done) && $call->done instanceof Closure){
              $data                                         = call_user_func($call->done, $args);
            }
            else{
              $data                                         = $args;
            }
            return $data;
          };
        return $return;
      });
    
    $result                                                 = $this->add_listener(function($data)
      use (&$func, &$_this){
        return $_this->do_then($data, $func, null, false);
      });
    $result->debug_type                                     = self::ATTR_APPLY;

    $object                                                 = new stdclass;
    $object->done                                           = true;
    if($callback instanceof Option){
      $object->detail                                       = &$callback;
    }
    else{
      $object->detail                                       = array('event' => &$callback);
    }
    $stemp->update($object);

    return $result;
  }

  /**
   * filter
   * 
   * @param array|int $range
   * @param mixed $filter
   * @param mixed $option
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function filter($range, $filter, $option = null){
    $_this                                                  = $this;
    $init                                                   = false;
    $last                                                   = 0;
    $callback                                               = $this->do_filter($filter, $option);
    $stemp                                                  = new Option();
    $result                                                 = null;
    $func                                                   = $stemp->then(function($callback)
      use (&$range, &$init, &$result, &$last){
        if(is_object($callback) && isset($callback->done))
          $callback                                         = $callback->done;
        elseif(is_object($callback) && method_exists($callback, 'done'))
          $callback                                         = array($callback, 'done');
        elseif(is_array($callback) && isset($callback['done']))
          $callback                                         = $callback['done'];
        else
          $callback                                         = $callback['event'];
        $return                                             = new stdclass;
        $return->done                                       = function($state)
          use(&$callback, &$range, &$init, &$last, &$result){
            $ctrl                                           = null;
            $index                                          = 0;
            $err                                            = 0;
            $one                                            = !is_array($range);
            $range                                          = !$one ? $range : array($range);
            $var                                            = array();
            foreach($range as $index => $item){
              $var[$index]                                  = array_key_exists($item, $state) ? $state[$item] : null;
            }
            $var                                            = $one ? $callback(reset($var)) : $callback($var);
            if(!$var['confirmed']){
              $error                                        = new \Accido\Exceptions\Option('Filter failed');
              $error->detail                                = $var['value'];
              throw $error;
            }
            $ctrl                                           = array_shift($state);
            $last                                           = count($state);
            if($init){
              array_splice($result->merged, -$last, $last);
            }
            foreach($state as $index){
              $result->listen($index);
            }
            $init                                           = true;
            return $ctrl;
          };
        return $return;
      });
    
    $result                                                 = $this->add_listener(function($data)
      use (&$func, &$_this){
        return $_this->do_then($data, $func, null, false);
      });
    $result->debug_type                                     = self::ATTR_FILTER;

    $object                                                 = new stdclass;
    $object->done                                           = true;
    if($callback instanceof Option){
      $object->detail                                       = &$callback;
    }
    else{
      $object->detail                                       = array('event' => &$callback);
    }
    $stemp->update($object);

    return $result;
  }

  /**
   * effect
   * 
   * @param Option $stream
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function effect(Option $stream){
    $_this                                                  = $this;
    $init                                                   = 0;
    $result                                                 = new Option();
    $this->add_listener(function($data) use (&$init, &$_this, &$result, &$stream){
      if($init++){
        return $data;
      }
      $_this->do_then($data, function() use (&$stream, &$result){
        $comm                                               = $stream->commit();
        $comm->join($result);
      }, null);
    });
    $result->debug_type                                     = self::ATTR_EFFECT;
    return $result;
  }

  /**
   * join
   * 
   * @param Option $stream
   * @param callable|null $callback
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function join(Option $stream, $callback = null){
    $root                                                   = $stream->commit($callback);
    $result                                                 = $root;
    while($root->ancestor instanceof Option){
      $root                                                 = $root->ancestor;
    }
    foreach($root->merged as $index => $item){
      $this->merged[]                                       = $item;
    }
    $root->ancestor                                         = $this;
    $root->storage                                          = null;
    $root->sleep                                            = true;
    $root->merged                                           = &$this->merged;
    $this->callbacks[]                                      = array('option' => $root);
    if($this->sleep) return $result;
    $root->update(clone $this->storage);
    return $result;
  }

  /**
   * nocommit
   * 
   * @uses $no_commit
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function nocommit(){
    $this->no_commit                                        = true;
    return $this;
  }

  /**
   * commit
   * 
   * @param callable|null $callback
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function commit($callback = null){
    $result                                                 = array('commit' => false, 'stream' => $this);
    $strm                                                   = null;
    $calle                                                  = !empty($callback);
    if(empty($this->callbacks)){
      return ((!$this->is_commit || ($calle && $callback($this))) ? $this : $result);
    }
    foreach($this->callbacks as &$callbs){
      $strm                                                 = $callbs['option'];
      if($strm->no_commit){
        continue;
      }
      $result['stream']                                     = $strm;
      $strm->is_commit                                      = true;
      $temp                                                 = $strm->commit($callback);
      $strm->is_commit                                      = false;
      if($temp instanceof Option){
        $result['commit']                                   = $temp;
        $result['stream']                                   = true;
        return $this->is_commit ? $result : $temp;
      }
      elseif(!empty($temp) && is_array($temp) && isset($temp['commit']) && $temp['commit']){
        return $this->is_commit ? $temp : $temp['stream'];
      }
      else{
        $result                                             = &$temp;
      }
    }
    return $this->is_commit ? $result : $result['stream'];
  }

  /**
   * head
   *
   * @since 0.1 Start Version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  function head(){
    return empty($this->ancestor) ? $this : $this->ancestor->head();
  }

  /**
   * end
   * 
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function end(){
    foreach($this->callbacks as $callback){
      $callback['option']->destroy();
    }
    unset($this->callbacks);
    $this->callbacks                                        = array();
    return $this;
  }

  /**
   * off
   * 
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function off(){
    if($this->ancestor instanceof Option){
      foreach($this->ancestor->callbacks as $index => $item){
        if($item['option'] === $this){
          array_splice($this->ancestor->callbacks, $index, 1);
          break;
        }
      }
    }
    foreach($this->callbacks as $item){
      $item['option']->ancestor                             = null;
    }
    $this->callbacks                                        = array();
    $this->ancestor                                         = null;
    return $this;
  }
  
  /**
   * done
   * 
   * @param Option|callable $done
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function done($done){
    return $this->then($done, null);
  }

  /**
   * fail
   * 
   * @param Option|callable $fail
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function fail($fail){
    return $this->then(null, $fail);
  }
  
  /**
   * update
   * 
   * @param stdclass $event
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function update(stdclass $event){
    $data                                                   = $event->detail;
    $this->is_fail                                          = !isset($event->done) || !$event->done ? true : false;
    $done                                                   = &$this->_done;
    $fail                                                   = &$this->_fail;
    $_this                                                  = $this;

    if($data instanceof Option){
      $data->is_fail                                        = $data->is_fail || $this->is_fail;
      $data->then(function() use (&$_this, &$position){
          $args                                             = func_get_args();
          $var                                              = array_shift($args);
          $object                                           = new stdclass;
          $object->detail                                   = $var;
          $object->done                                     = true;
          $position                                         = !empty($this->ancestor) && !empty($args) ? count($this->ancestor->merged) : -1;
          if(0 < $position){
            $_this->ancestor->listen_array($args);
          }
          $_this->update($object);
          if(0 < $position){
            array_splice($_this->ancestor->merged, $position);
          }
        }, function($reasone) use (&$_this){
          $_this->is_fail                                   = true;
          $object                                           = new stdclass;
          $object->detail                                   = $reasone;
          $_this->update($object);
        })
        ->nocommit();
      return;
    }
    elseif($this->utils->is_callable($data)){
      $this->resolve_callable($data, array($this, 'update'));
      return;
    }
    elseif(!$this->is_fail && !empty($done)){
      if($done instanceof Option){
        $done->then(function($func) use(&$_this, &$data) {
            $_this->resolve_callable($func->done, array($_this, 'do_update'), $data);
          }, function($func) use (&$_this, &$data) {
            $_this->is_fail                                 = true;
            $_this->resolve_callable($func->fail, array($_this, 'do_update'), $data);
          })
          ->nocommit();
        return;
      }
      $this->resolve_callable($done, array($this, 'do_update'), $data);
      return;
    }
    elseif($this->is_fail && !empty($fail)){
      if($fail instanceof Option){
        $fail->then(function($func) use (&$_this, &$data) {
            $_this->resolve_callable($func->done, array($_this, 'do_update'), $data);
          }, function($func) use (&$_this, &$data) {
            $_this->resolve_callable($func->fail, array($_this, 'do_update'), $data);
          })
          ->nocommit();
        return;
      }
      $this->resolve_callable($fail, array($this, 'do_update'), $data);
      return;
    }
    $this->do_update($event);
  }

  /**
   * promise
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Promise
   */
  public function promise($resolver){
    $return                                                 = new Promise($resolver);
    $return->ancestor                                       = $this;
    $this->callbacks[]                                      = array('option' => $return);
    if($this->sleep) return $return;
    $event                                                  = clone $this->storage;
    $this->async(function() use (&$event, &$return){
      $return->update($event);
    });
    return $return;
  }

  /**
   * when
   * 
   * @param callable $resolver
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Promise
   */
  public function when($resolver){
    $return                                                 = new Promise($resolver, null, null, false);
    $return->ancestor                                       = $this;
    $this->callbacks[]                                      = array('option' => $return);
    if($this->sleep) return $return;
    $event                                                  = clone $this->storage;
    $this->async(function() use (&$event, &$return){
      $return->update($event);
    });
    return $return;
  }

  /**
   * merge
   * @params mixed
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function merge(){
    $result                                                 = $this->add_listener();
    $result->debug_type                                     = self::ATTR_MERGE;
    call_user_func_array(array($result, 'listen'), func_get_args());
    return $result;
  }

  /**
   * merge_array
   * 
   * @param array $array
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   * @throws \Accido\Exceptions\Option
   *
   * @return Option
   */
  public function merge_array(array $array){
    if(!is_array($array)){
      $throw                                                = new \Accido\Exceptions\Option('Merge error.');
      $throw->detail                                        = 'It\'s wanted listen only array, yep.';
      throw $throw;
    }
    return call_user_func_array(array($this, 'merge'), $array);
  }

  /**
   * listen
   *
   * @params mixed
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function listen(){
    foreach(func_get_args() as $index){
      $this->merged[]                                       = $index;
    }
    return $this;
  }

  /**
   * listen_array
   * 
   * @param array $array
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   * @throws \Accido\Exceptions\Option
   *
   * @return Option
   */
  public function listen_array(array $array){
    if(!is_array($array)){
      $throw                                                = new \Accido\Exceptions\Option('Listen error.');
      $throw->detail                                        = 'It\'s wanted listen only array, yep.';
      throw $throw;
    }
    return call_user_func_array(array($this, 'listen'), $array);
  }

}

