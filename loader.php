<?php
namespace Accido;

class Loader{

  const ATTR_MODEL_NAMESPACE = 'Accido\\Models\\';

  /**
   * @var Injector
   */
  public $dic               = null;

  /**
   * @param string $model
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  private function sanitize_model($model){
    $model                            = preg_replace('`[\/\\ \s]+`', '\\', $model);
    return self::ATTR_MODEL_NAMESPACE . trim($model, '\\');
  }

  /**
   * @param Injector $dic
   * @uses dic
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function __construct(Injector $dic){
    $this->dic              = $dic;
  }

  /**
   * register_model
   * 
   * @param mixed $model
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Model
   */
  public function model($model){
    $dic                                = $this->dic;
    $model                              = $this->sanitize_model($model);
    $arguments                          = func_get_args();
    $arguments[0]                       = $model;
    if(!isset($dic[$model])){
      $_model                           = $dic->reuse(function() use ($model, $dic){
          $return                       = new $model;
          $return->injector             = $dic;
          $args                         = func_get_args();
          call_user_func_array(array($return, 'capture'), $args);
          $return->is_instance_related  = true;
          return $return;
        });
      $dic->add($model, $_model);
    }
    $return                             = call_user_func_array(array($dic, 'call'), $arguments);
    return $return;
  }

  /**
   * 
   * @param mixed $model
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Model
   */
  public function instance($model){
    $model                              = $this->sanitize_model($model);
    $return                             = new $model;
    $args                               = func_get_args();
    $args[0]                            = $this->dic;
    call_user_func_array(array($return, 'capture'), $args);
    $return->is_instance_related        = false;
    return $return;
  }
}
