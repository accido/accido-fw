<?php
namespace Accido;
use Exception;
defined('PROJECT_ROOT') or define('PROJECT_ROOT', realpath( dirname( dirname( __FILE__ ) ) ) );
defined('CORE_ROOT') or define('CORE_ROOT', realpath( dirname( __FILE__ ) ) );

/**
 * Autoloader 
 * 
 * @package 
 * @version $id$
 * @copyright 2013
 * @author andrew scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
final class Autoloader
{

  /**
   * namespaces
   *
   * @var array
   */
  private $namespaces                                = array();

  /**
   * __construct
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function __construct()
  {
    set_include_path( get_include_path() . PATH_SEPARATOR . PROJECT_ROOT . PATH_SEPARATOR . CORE_ROOT );
    spl_autoload_register( array($this, 'controller'), true, true );
  }

  // public controller(className) {{{ 
  /**
   * controller
   * 
   * @param mixed $className 
   * @access public
   * @return void
   */
  public function controller( $classname )
  { 
    if(!$this->find($classname)){
      spl_autoload_extensions('.php');
      spl_autoload($classname);
    }
  }
  // }}}

  /**
   * register_namespace
   *
   * @param mixed $namespace
   * @param mixed $path_to_root
   * @uses namespaces
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * return bool
   */
  public function register_namespace($namespace, $path_to_root){
    $path_to_root                                 = rtrim($path_to_root, '\\/');
    if(is_dir($path_to_root)){
      $this->namespaces[$namespace]               = $path_to_root;
      return true;
    }
    return false;
  }

  /**
   * register_prefix
   * 
   * @param string $prefix
   * @param string $path_to_root
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function register_prefix($prefix, $path_to_root){
    return $this->register_namespace($prefix, $path_to_root);
  }

  /**
   * find
   * 
   * @param mixed $classname
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function find($classname){
    $filename           = trim(preg_replace('/[\\/\\\\_]++/iu', '/', $classname), '/');
    $find               = false;
    try{
      foreach($this->namespaces as $namespace => $path_to_root){
        if (0 === strpos($classname, $namespace)){
          $modulename   = $path_to_root . '/' . $filename . '.php';
          if(!($find    = include_once($modulename))){
            $find       = include_once(strtolower($modulename));
          }
          break;
        }
      }
      if(!$find){
        $filename       = strtolower($filename);
        $filename       = preg_replace('/^accido\//', '', $filename);
        $find           = include_once($filename . '.php');
      }
    }
    catch(Exception $error){
      trigger_error($error->getMessage(), E_USER_ERROR);
      $find             = false;
    }
    return $find;
  }
}

global $accautoloader;
$accautoloader = new Autoloader;
