<?php
namespace Accido;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * View{ 
 * 
 * @package 
 * @version $id$
 * @copyright 2013 Accido
 * @author Andrew Scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
class View{

  /**
   * ATTR_VIEW_EXTENSION 
   * 
   * @const string
   */
  const ATTR_VIEW_EXTENSION = '.php';
  /**
   * ATTR_VIEW_BASE_DIR 
   * 
   * @const string
   */
  const ATTR_VIEW_BASE_DIR  = 'views/';

  /**
   * vars 
   * 
   * @var array
   * @access private
   */
  private $vars = array();

  /**
   * filename 
   * 
   * @var string
   * @access private
   */
  private $filename = '';

  // public __construct(filename) {{{ 
  /**
   * __construct
   * 
   * @param string $filename 
   * @access public
   * @return void
   */
  public function __construct( $filename ){
    $this->set_filename( $filename );  
  }
  // }}}

  // public factory(filename) {{{ 
  /**
   * factory
   * 
   * @param string $filename 
   * @static
   * @access public
   * @return View
   * @throws Exception file not found
   */
  public static function factory( $filename ){
    return new self( $filename );
  }
  // }}}

  // public set_filename(file) {{{ 
  /**
   * set_filename
   * 
   * @param string $file 
   * @access public
   * @return void
   */
  public function set_filename( $file ){
    if (file_exists($file) && is_file($file)){
      $this->filename = $file;
      return;
    }
    $filename         = self::ATTR_VIEW_BASE_DIR . $file . self::ATTR_VIEW_EXTENSION;
    if (file_exists(CORE_ROOT . '/' . $filename) || file_exists(PROJECT_ROOT . '/' . $filename)){
      $this->filename = $file;
    }
    else throw new \Accido\Exceptions\View( 'View: file not found - "' . $file . '"');
  }
  // }}}

  // public get_filename() {{{ 
  /**
   * get_filename
   * 
   * @access public
   * @return string
   */
  public function get_filename(){
    return $this->filename;
  }
  // }}}

  // public __set(key,value) {{{ 
  /**
   * __set
   * 
   * @param string $key 
   * @param mixed $value 
   * @access public
   * @return mixed
   */
  public function __set( $key, $value){
    $this->vars[$key] = $value;
    return $value;
  }
  // }}}

  // public __get(key) {{{ 
  /**
   * __get
   * 
   * @param string $key 
   * @access public
   * @throws Exception key not exists
   * @return mixed
   */
  public function __get( $key ){
    if ( !array_key_exists( $key, $this->vars) )
      throw new \Accido\Exceptions\View('View[path::' . $this->filename . ']: key <' . $key . '> not exists.');
    return $this->vars[$key];
  }
  // }}}

  // public __isset(key) {{{ 
  /**
   * __isset
   * 
   * @param string $key 
   * @access public
   * @return bool
   */
  public function __isset( $key ){
    return array_key_exists( $key, $this->vars );
  }
  // }}}

  // public set(key,value) {{{ 
  /**
   * set
   * 
   * @param string $key 
   * @param mixed $value 
   * @access public
   * @return mixed
   */
  public function set( $key = null, $value = null){
    if(null === $key && is_array($value)){
      $this->vars                   = $value;
      return $value;
    }
    if (is_array($key)){
      foreach($key as $k => $v)
        $this->vars[$k]             = $v;
      return $key;
    }
    else
      return $this->vars[$key]      = $value;
  }
  // }}}

  // public get(key) {{{ 
  /**
   * get
   * 
   * @param string $key 
   * @access public
   * @return mixed
   */
  public function get( $key = null ){
    if(null === $key) return $this->vars;
    return $this->$key;
  }
  // }}}

  // public bind(key,&value) {{{ 
  /**
   * bind
   * 
   * @param mixed $key 
   * @param mixed $value 
   * @access public
   * @return void
   */
  public function bind( $key, &$value){
    if (is_object($value)){
      $this->vars[$key] = $value;
    }
    else{
      $this->vars[$key] = &$value;
    }
  }
  // }}}

  // public render(file=NULL) {{{ 
  /**
   * render
   * 
   * @param string $file 
   * @access public
   * @throw Exceptions_View
   * @return string
   */
  public function render( $file = NULL ){
    try{
      if ( $file !== NULL ){
        $this->set_filename( $file );
      }

      extract( $this->vars, EXTR_SKIP );
      ob_start();
      try{
        $filename = $this->filename;
        if(0 !== strlen($filename) && (!file_exists($filename) || !is_file($filename))){
          $filename = self::ATTR_VIEW_BASE_DIR . $filename . self::ATTR_VIEW_EXTENSION;
        }
        if (0 !== strlen($filename))
          include($filename);
      }
      catch( Exception $e ){
        ob_end_clean();
        throw new \Accido\Exceptions\View($e); 
      }
      $cot    = ob_get_clean();
      return $cot;
    }
    catch( Exception $e ){
      throw new \Accido\Exceptions\View($e);
    }
  }
  // }}}

}
