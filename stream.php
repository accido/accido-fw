<?php
namespace Accido;
use Accido\Option;
use Accido\Model;
use Accido\Models\Socket;
use Accido\Models\Socket\Session as SocketSession;
use Accido\Models\Request;
use Accido\Controller;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Stream
 *
 * @package Stream
 * @subpackage Option
 * 
 * 
 * @see Option
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Stream extends Option {

  /**
   * @var Model
   */
  public $model                                             = null;

  /**
   * @var string
   */
  public $offset                                            = null;

  /**
   * @var string
   */
  public $alias                                             = null;

  /**
   * __construct
   * 
   * @param Model $model
   * @param scalar $key
   * @param scalar $alias
   * @uses model,offset,alias
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function __construct(Model $model, $key, $alias = null){
    parent::__construct($model[$key]);
    $this->model                                            = $model;
    $this->offset                                           = $key;
    $this->alias($alias);
  }

  /**
   * alias
   * 
   * @param string $alias
   * @uses alias,offset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function alias($alias = null){
    $this->alias                                            = $alias ? $alias : $this->offset;
    return $this;
  }

  /**
   * @param Controller $ctrl
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return self
   */
  public function relate(Controller $ctrl){
    $loader                                                 = new Loader(Model::$dic);
    $helper                                                 = $loader->instance('Socket/Helper', $ctrl);
    $session                                                = $loader->model('Socket/Session', $helper->session());
    $data                                                   = &$session->ref(SocketSession::OPT_DATA);
    $name                                                   = $this->alias;
    $model                                                  = $this->model;
    $key                                                    = $this->offset;
    $unique                                                 = $helper->unique();
    if(!isset($data[$name])){
      $data[$name]                                          = [];
    }
    if(isset($data[$name][$unique])){
      return $this;
    }
    $stream                                                 = $this
      ->then(function($data) use (&$name, &$session, $model){
        $session->pack([$name, $data]);
        return $data;
      })
      ->nocommit();
    $data[$name][$unique]                                   = [$model, $key, $stream];
    return $this;
  }

  /**
   * @param Controller $ctrl
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Stream
   */
  public function recv(Controller $ctrl){
    $loader                                                 = new Loader(Model::$dic);
    $helper                                                 = $loader->instance('Socket/Helper', $ctrl);
    $session                                                = $loader->model('Socket/Session', $helper->session());
    $data                                                   = &$session->ref(SocketSession::OPT_DATA);
    $name                                                   = $this->alias;
    $model                                                  = $this->model;
    $key                                                    = $this->offset;
    $unique                                                 = $helper->unique();
    if(!isset($data[$name])){
      $data[$name]                                          = [];
    }
    if(isset($data[$name][$unique])){
      return $this;
    }
    $data[$name][$unique]                                   = [$model, $key, false];
    return $this;
  }

  /**
   * @param Controller $ctrl
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Stream
   */
  public function send(Controller $ctrl){
    $loader                                                 = new Loader(Model::$dic);
    $helper                                                 = $loader->instance('Socket/Helper', $ctrl);
    $session                                                = $loader->model('Socket/Session', $helper->session());
    $name                                                   = $this->alias;
    $data                                                   = &$session->ref(SocketSession::OPT_DATA);
    $unique                                                 = $helper->unique();
    if(!isset($data[$name])){
      $data[$name]                                          = [];
    }
    if(isset($data[$name][$unique])){
      return $this;
    }
    $stream                                                 = $this
      ->then(function($data) use (&$name, &$session){
        $session->pack([$name, $data]);
        return $data;
      })
      ->nocommit();
    $data[$name][$unique]                                   = [false, false, $stream];
    return $this;
  }

  /**
   * @param Controller $ctrl
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Stream
   */
  public function derelate(Controller $ctrl){
    $loader                                                 = new Loader(Model::$dic);
    $helper                                                 = $loader->instance('Socket/Helper', $ctrl);
    $session                                                = $loader->model('Socket/Session', $helper->session());
    $data                                                   = &$session->ref(SocketSession::OPT_DATA);
    $name                                                   = $this->alias;
    $unique                                                 = $helper->unique();
    if(!isset($data[$name]) || !isset($data[$name][$unique])){
      return $this;
    }
    list($model, $key, $stream)                             = $data[$name][$unique];
    if($stream){
      $stream->destroy();
    }
    unset($data[$name][$unique]);
    return $this;
  }

}
