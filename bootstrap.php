<?php
namespace Accido;
error_reporting(0);
ini_set('display_errors', 0);
try{
  include_once("autoloader.php");
  include_once(PROJECT_ROOT . "/bootstrap.php");
  Controller::execute('application/run');
}
catch(\Exception $error){
  echo $error->getTraceAsString();
}
/* Ex.
 *  $app                                                  = new Application();
 *  $app->handle('application/run')
 *    ->then($app->register('Action/Init/Profiler'))
 *    ->then($app->register('Action/Init/Shutdown'))
 *    ->then($app->register('Action/Init/Logger'))
 *    ->then($app->register('Action/Init/Install'))
 *    ->then($app->register('Action/Init/Repository'))
 *    ->then($app->register('Action/Init/Router'));
 */
