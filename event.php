<?php
namespace Accido;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Event 
 * 
 * @package 
 * @version $id$
 * @copyright 2013 Accido
 * @author Andrew Scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
class Event {

  /**
   * ATTR_LOW_EVENT_PRIORITY 
   * 
   * @const int
   */
  const ATTR_LOW_EVENT_PRIORITY         = 100;
  /**
   * ATTR_NORMAL_EVENT_PRIORITY 
   * 
   * @const int
   */
  const ATTR_NORMAL_EVENT_PRIORITY      = 50;
  /**
   * ATTR_HIGH_EVENT_PRIORITY 
   * 
   * @const int
   */
  const ATTR_HIGH_EVENT_PRIORITY        = 10;
  /**
   * ATTR_EXPRESS_EVENT_PRIORITY 
   * 
   * @const int
   */
  const ATTR_EXPRESS_EVENT_PRIORITY     = 0;

  /**
   * array $events
   * @access protected
   */
  protected $events                     = array();
  /**
   * locked
   *
   * @var int
   */
  protected $locked                     = 0;
  /**
   * waited
   *
   * @var array
   */
  protected $current                    = array();
  /**
   * eevent_id
   *
   * @var array
   */
  protected $event_id                   = array();
  /**
   * instance_args
   *
   * @var mixed
   */
  public $instance_args                 = null;
  /**
   * instance_hash
   *
   * @var mixed
   */
  public $instance_hash                 = null;
  /**
   * is_instance_related
   *
   * @var mixed
   */
  public $is_instance_related           = null;

  /**
   * next
   *
   * @param string $index
   * @param array $array
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  private function next($index, array &$array){
    $find                     = false;
    foreach($array as $jndex => $value){
      if($find || null === $index) return $jndex;
      if($index === $jndex){
        $find                 = true;
      }
    }
    return false;
  }

  /**
   * do_bind
   *
   * @param array $array
   * @param string $key
   * @param callable $callback
   * @param int $priority
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  protected function do_bind(array &$array, $key, $callback, $priority){
    $result                           = false;
    if(is_callable($callback) && is_int($priority)){
      if(!array_key_exists($key, $array))
        $array[$key]                  = array();
      if(array_key_exists($priority, $array[$key]))
        $array[$key][$priority][]     = $callback;
      else{
        $array[$key][$priority]       = array($callback);
        ksort($array[$key]);
      }
      $temp                           = &$array[$key][$priority];
      end($temp);
      $result                         = uniqid();
      $this->event_id[$result]        = array(&$temp, key($temp), $key, $callback, $priority);
      reset($temp);
    }

    return $result;
  }

  /**
   * __construct
   *
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  public function __construct(){
    
  }

  // public bind(key,callback,priority=self::ATTR_NORMAL_EVENT_PRIORITY) {{{ 
  /**
   * bind
   * 
   * @param string $key 
   * @param callable $callback 
   * @param int $priority 
   * @access public
   * @return bool
   */
  public function bind($key, $callback, $priority = self::ATTR_NORMAL_EVENT_PRIORITY){
    $return                 = $this->do_bind($this->events, $key, $callback, $priority);
    if(!$this->locked) return $return;
    $current                = &$this->current;
    foreach($current as $event => &$callbs){
      if(!preg_match('/^' . $key . '$/u', $event)) continue;
      if(!array_key_exists($priority, $callbs)) $callbs[$priority] = array();
      $callbs[$priority][]  = $callback;
    }
    ksort($current);
    return $return;
  }
  // }}}
  
  /**
   * unbind
   * 
   * @param string $key
   * @param callable $callback
   * @param int $priority
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function unbind($uniqid){
    if(!isset($this->event_id[$uniqid])){
      return false;
    }
    list($array, $index, $key, $callback, $priority)  
                            = $this->event_id[$uniqid];
    $array                  = &$this->event_id[$uniqid][0];
    unset($array[$index]);
    if(!$this->locked) return true;
    $current                = &$this->current;
    foreach($current as $event => &$callbs){
      if(!preg_match('/^' . $key . '$/u', $event)){
        continue;
      }
      if(!array_key_exists($priority, $callbs)){
        continue;
      }
      $array                = &$callbs[$priority];
      foreach($array as $index => $item){
        if($item === $callback){
          unset($array[$index]);
          break;
        }
      }
    }
    return true;
  }

  // public trigger(key) {{{ 
  /**
   * trigger
   * 
   * @param string $key 
   * @access public
   * @return bool
   */
  public function trigger($key){
    $result                 = false;
    if (isset($this->current[$key])) return $result;
    $this->current[$key]    = array();
    $args                   = func_get_args();
    array_shift($args);
    $this->locked++;
    $event                  = null;
    $current                = &$this->current[$key];
    foreach($this->events as $event => $params){
      if(!preg_match('/^' . $event . '$/u', $key)) continue;
      foreach($this->events[$event] as $priority => $callbs){
        foreach($callbs as $callback){
          if(!array_key_exists($priority, $current)) $current[$priority] = array();
          $current[$priority][] = $callback;
        }
      }
    }
    if (empty($current)) return $result;
    ksort($current);
    $index                  = null;
    while(false !== ($index = $this->next($index, $current))){
      $jndex                = null;
      while(false !== ($jndex = $this->next($jndex, $current[$index]))){
        call_user_func_array($current[$index][$jndex], $args);
        $result = true;
      }
    }
    unset($this->current[$key]);
    $this->locked--;
    return $result;
  }
  // }}}

  /**
   * debug
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function debug(){
    foreach($this->events as $key => $priors){
      foreach($priors as $prior => $callbs){
        foreach($callbs as $enque => $callback){
          echo 'Key: ' . $key . ' priority: ' . $prior . ' id: ' . $enque . ' class: ' . get_class($callback[0]) . "\n";
        }
      }
    }
  }

}
