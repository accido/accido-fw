<?php
namespace Accido;
use Accido\Injector;
use Accido\Models\Argument;
use Exception,Closure,ReflectionClass,ReflectionProperty,ReflectionObject,ArrayAccess;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: DIC
 *
 * @package Dependency Injector
 *
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class DIC implements Injector,ArrayAccess {

  /**
   * vars
   *
   * @var array
   */
  private $vars                       = array();

  /**
   * singletons
   *
   * @var array
   */
  public $singletons                  = array();

  /**
   * args
   *
   * @var array
   */
  public $args                        = array();

  /**
   * related
   *
   * @var array
   */
  public $related                     = array();

  /**
   * ensure
   *
   * @param mixed $err
   * @param mixed $msg
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   * @throws Exception
   */
  protected function ensure($err, $msg){
    if($err){
      throw new Exception($msg);
    }
  }

  /**
   * __construct
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function __construct(){
  }

  // public __isset(key) {{{ 
  /**
   * __isset
   * 
   * @param mixed $key 
   * @access public
   * @return void
   */
  public function __isset( $key ){
    return isset($this->vars[$key]);
  }
  // }}}

  /**
   * exists
   *
   * @param mixed $key
   * @uses vars
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function exists($key){
    return isset($this->vars[$key]);
  }

  // public get(key) {{{ 
  /**
   * get
   * 
   * @param mixed $key 
   * @access public
   * @return mixed
   */
  public function get( $key ){
    return $this->__get( $key );
  }
  // }}}

  // public set(key,value) {{{ 
  /**
   * set
   * 
   * @param mixed $key 
   * @param mixed $value 
   * @access public
   * @return mixed
   */
  public function set( $key, $value ){
    return $this->__set( $key, $value );
  }
  // }}}

  /**
   * Call the ctor function for $name dependency with $arguments
   *
   * @param string $name
   * @param array $arguments
   * @return mixed
   * @throws Exceptions_Dependency
   */
  public function __call($name, array $arguments) {
    $this->ensure(!isset($this->vars[$name]), "Dependency '" . $name . "' not defined.");
    $return = $this->vars[$name];
    $this->ensure(!$return instanceof Closure, "Dependency is not constructable: '$name'");
    array_unshift($arguments, $this);
    return call_user_func_array($return, $arguments);
  }

  /**
   * call
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function call(){
    $args = func_get_args();
    $name = array_shift($args);
    $this->ensure(!isset($this->vars[$name]), "Dependency '" . $name . "' not defined.");
    $return = $this->vars[$name];
    $this->ensure(!$return instanceof Closure, "Dependency is not constructable: '$name'");
    array_unshift($args, $this);
    return call_user_func_array($return, $args);
  }

  // public __get(key) {{{ 
  /**
   * __get
   * 
   * @param mixed $key 
   * @access public
   * @return mixed
   */
  public function __get( $name ){
    $this->ensure(!isset($this->vars[$name]), "Dependency '" . $name . "' not defined.");
    $return = $this->vars[$name];
    return $return instanceof Closure ? call_user_func($return, $this) : $return;
  }
  // }}}

  // public __set(key,value) {{{ 
  /**
   * __set
   * 
   * @param mixed $key 
   * @param mixed $value 
   * @access public
   * @return mixed
   */
  public function __set( $key, $value ){
    $this->vars[$key] = $value;
    return $value;
  }
  // }}}

  /**
   * add
   *
   * @param mixed $key
   * @param mixed $value
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function add( $key, $value ){
    $return             = false;
    if (!isset($this->vars[$key])){
      $this->vars[$key] = $value;
      $return           = true;
    }
    return $return;
  }

  /**
   * __unset
   *
   * @param mixed $key
   * @param mixed $value
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function __unset($key){
    unset($this->vars[$key]);
  }

  /**
   * sanitize_model
   * 
   * @param string $model
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function sanitize_model($model){
    $model                                      = preg_replace('`[\/\\ \s]+`', '\\', $model);
    return 'Accido\\Models\\' . trim($model, '\\');
  }

  /**
   * Reuse the instance created by $ctor
   *
   * @param Closure $ctor
   * @return Closure
   */
  public function reuse(Closure $ctor) {
    $injector                                   = $this;
    return function($self) use ($ctor, $injector) {
      $args                                     = (array)func_get_args();
      $instance                                 = null;
      $argument                                 = null;
      $hash                                     = spl_object_hash($ctor);
      $temp                                     = null;
      if(!isset($injector->singletons[$hash])){
        $injector->singletons[$hash]            = array();
      }
      $hash_key                                 = $injector->get_hash($args);
      if(isset($injector->singletons[$hash][$hash_key])){
        return $injector->singletons[$hash][$hash_key]->__instance;
      }
      $instance                                 = call_user_func_array($ctor, $args);
      $argument                                 = new Argument();
      $argument->capture($injector, $hash, $args, $hash_key);
      $argument->relate($instance);
      return $instance;
    };
  }

  /**
   * get_hash
   * 
   * @param $param
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function get_hash($param){
    if(is_object($param)){
      return spl_object_hash($param);
    }
    elseif(!is_array($param)){
      return sha1((string)$param);
    }
    $hash                                       = '';
    foreach($param as $index => $item){
      $hash                                     .= '~' . $index . '~' .
        $this->get_hash($item);
    }
    return empty($hash) ? '' : sha1($hash);
  }

  /**
   * Returns a closure wrapped for usage as a dependency,
   * to distinguish from a ctor-closure.
   *
   * @param Closure $value
   * @return Closure
   */
  public function wrap_closure(Closure $value) {
    return function() use ($value) {
        return $value;
    };
  }

  /**
   * debug
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function debug(){
    foreach($this->vars as $key => $depend){
      echo 'Key: ' . $key . ' value: ' . get_class($this->get($key)) . "\n";
    }
    foreach($this->singletons as $hash => $item){
      var_dump('Singleton Class: ~' . get_class($item['instance']) . "~\nClosure hash: ~" . $hash . "~\n");
    }
  }

  /**
   * get_closure
   *
   * @param mixed $name
   * @uses vars
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Closure
   */
  public function get_closure($name){
    $this->ensure(!isset($this->vars[$name]), "Dependency '" . $name . "' not defined.");
    $return = $this->vars[$name];
    $this->ensure(!$return instanceof Closure, "Dependency is not constructable: '$name'");
    return $return;
  }

  /**
   * offsetSet
   *
   * @param mixed $offset
   * @param mixed $value
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function offsetSet($offset, $value) {
    if (!is_null($offset)) {
      return $this->__set($offset, $value);
    }
  }

  /**
   * offsetExists
   *
   * @param mixed $offset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function offsetExists($offset) {
    return $this->__isset($offset);
  }

  /**
   * offsetUnset
   *
   * @param mixed $offset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  public function offsetUnset($offset) {
    $this->__unset($offset);
  }

  /**
   * offsetGet
   *
   * @param mixed $offset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function offsetGet($offset) {
    return $this->__get($offset);
  }

}

