<?php defined('CORE_ROOT') or die('No direct script access.'); ?>
<!DOCTYPE HTML>
<html>
	<head>
    <base href="<?php echo $base_url?>/">
		<meta http-equiv="content-type" content="text/html; charset=<?php echo $charset?>">
    <title>Logs</title>
    <!-- <link type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet"> -->
    <link type="text/css" href="assets/css/log.css" rel="stylesheet">
    <script type="text/javascript" language="javascript">
    var _root_core_path = {
      path              : '<?php echo $base_url . "/assets/core/" ?>',
      app               : '<?php echo $base_url . "/assets/js/" ?>',
      model             : 'backend',
      base              : '<?php echo $base_url?>',
      area              : '<?php echo $area;?>'
    };
    </script>
    <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js" language="javascript"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" language="javascript"></script>-->
    <script type="text/javascript" src="assets/core/autoload.js" language="javascript"></script>
  </head>
  <body>
    <ul class="logs group">
    <?php foreach($logs as $log): extract($log);?>
      <li class="log-entity inner">
        <div class="date">
        <?php echo $date?>
        </div>
        <div class="file">
        <?php echo $file;?>
        </div>
        <div class="line">
        Line <?php echo $line?> in
        </div>
        <div class="type" data-id="<?php echo strtolower($type)?>">
        <?php echo $type;?>
        </div>
        <div class="message">
        <?php echo $message;?>
        </div>
      </li>
    <?php endforeach;?>
    </ul>
  </body>
</html>
