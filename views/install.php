<?php defined('CORE_ROOT') or die('No direct script access.');
$start = true;
foreach( $queries as $query ){
  $echo = (is_array($query) && array_key_exists( $type, $query ) && strlen( $query[$type] ) ) ? str_ireplace( array('{:prefix}', '{:charset}'), array($prefix, $charset), $query[$type] ) : '';
  if(!empty($echo))
    echo $start ? $echo . ($start = false) : PHP_EOL . '%%query%%' . PHP_EOL . $echo;
}
