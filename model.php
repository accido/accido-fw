<?php
namespace Accido;
use Accido\Event;
use Accido\DIC;
use Accido\Option;
use Accido\Stream;
use Accido\Injector;
use Accido\Models\Transaction;
use Accido\Models\Query;
use Accido\Loader;
use ArrayAccess,Closure,stdclass;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Model
 *
 * @package Model
 * 
 * @see ArrayAccess
 * @see Injector
 * 
 * @abstract
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
abstract class Model implements ArrayAccess,Injector {

  /**
   * dic
   *
   * @var mixed
   */
  public static $dic              = null;

  /**
   * injector
   *
   * @var Injector
   */
  public $injector                = null;

  /**
   * autoloader
   *
   * @var mixed
   */
  public static $autoloader       = null;

  /**
   * instance_args
   *
   * @var \Accido\Models\Argument;
   */
  public $instance_args           = null;

  /**
   * instance_hash
   *
   * @var string
   */
  public $instance_hash           = null;

  /**
   * is_instance_related
   *
   * @var mixed
   */
  public $is_instance_related     = false;

  /**
   * option
   *
   * @var array
   */
  public $option                  = array();

  /**
   * ATTR_PARAM_DELIMITER
   *
   * @const string
   */
  const ATTR_PARAM_DELIMITER      = '.';
  
  /**
   * ATTR_NULL_PARAMETER
   *
   * @const null
   */
  const ATTR_NULL_PARAMETER       = null;

  /**
   * VAR_EVENTS 
   * 
   * @const string
   */
  const OPT_EVENTS                = 'events';

  /**
   * OPT_SQL_INIT_CODE 
   * 
   * @const string
   */
  const OPT_SQL_INIT_CODE         = 'sql_init_code';

  /**
   * OPT_CHARSET_ENCODING
   *
   * @const string
   */
  const OPT_CHARSET_ENCODING      = 'application.charset';

  /**
   * OPT_BASE_URL
   *
   * @const string
   */
  const OPT_BASE_URL              = 'application.base_url';

  /**
   * OPT_LANGUAGE
   *
   * @const string
   */
  const OPT_LANGUAGE              = 'application.language';
  
  /**
   * vars 
   * 
   * @var array
   * @access protected
   */
  protected $vars                 = array(
    self::OPT_EVENTS              => array(),
  );

  /**
   * init_model
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  private function init_model(){
    $this->bind_events();
    $this->init();
  }

  // protected init() {{{ 
  /**
   * init
   * 
   * @access protected
   * @return void
   */
  protected function init(){

  }
  // }}}

  // protected bind_events() {{{ 
  /**
   * bind_events
   *
   * 
   * @access protected
   * @return void
   */
  protected function bind_events(){
    $dic          = self::$dic;
    $dic->add('event', $this->reuse(function(){
      return new Event;
    }));
    if($this->exists($this->vars, self::OPT_EVENTS) && !empty($this[self::OPT_EVENTS])){
      $event      = $dic->event;
      foreach( (array)$this->vars[self::OPT_EVENTS] as $_event => $config ){
        if(null !== $config){
          $uniqid = $event->bind($_event, array( $this, 'event_' . $_event), $config);
          $this->set(self::OPT_EVENTS . '.' . $_event, $uniqid);
        }
      }
    }
  }
  // }}}

  /**
   * register_event
   *
   * @param string $name
   * @param callable $callback
   * @param int $priority
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  protected function register_event($name, $callback, $priority = Event::ATTR_NORMAL_EVENT_PRIORITY){
    if($this->register_param($this->vars, self::OPT_EVENTS . '.' . $name, null)){
      $dic      = self::$dic;
      $dic->add('event', $this->reuse(function(){
        return new Event;
      }));
      $event    = $dic->event;
      $uniqid   = $event->bind($name, $callback, $priority);
      $this->set(self::OPT_EVENTS . '.' . $name, $uniqid);
      return true;
    }
    return false;
  }

  /**
   * handle
   *
   * @param string $route
   * @param int $priority
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function handle($route, $priority = Event::ATTR_NORMAL_EVENT_PRIORITY){
    $name                             = 'controllers_' . trim(preg_replace('/[\\_\\/]+/u', '_', $route), '_');
    $route                            = str_ireplace('/', '\/', $route);
    $stream                           = new Option;
    $func                             = function(View $view, Controller $ctrl) use ($stream) {
      $promise = $stream->commit()->promise(function(Controller $find, $resolve, $reject) use ($ctrl, &$promise){
          if($find === $ctrl){
            $resolve($find);
            $promise->off();
          }
        })
        ->nocommit();
      $ctrl->stream->listen($promise);
      $object                         = new stdclass;
      $object->done                   = true;
      $object->detail                 = $ctrl;
      $stream->update($object);
    };
    $this->register_event($route, $func, $priority);
    $this->register_event($name, $func, $priority);
     
    return $stream;
  }

  /**
   * off
   * 
   * @param string $name
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function off($name){
    if($this->exists($this->vars, self::OPT_EVENTS . '.' . $name)){
      $dic      = self::$dic;
      $dic->add('event', $this->reuse(function(){
        return new Event;
      }));
      $event    = $dic->event;
      $return   = $event->unbind($this->get(self::OPT_EVENTS . '.' . $name));
      unset($this[self::OPT_EVENTS . '.' . $name]);
      return $return;
    }
    return false;
  }

  /**
   * ensure
   *
   * @param bool $error
   * @param string $msg
   * @param array $params
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   * @throws \Accido\Exceptions\Model
   */
  public function ensure($error, $msg, array $params = array()){
    if(!$error){
      return;
    }
    $msg        = $this->resolve_error($msg, $params);
    $trace      = debug_backtrace();
    $trace      = (array)$trace[2];
    
    $trace      =  $trace['line'] . ' in ' 
                . $trace['file'] . ' ' 
                .$trace['class']
                .$trace['type']
                .$trace['function'];
    $msg        = 'Error: (' . $msg  . ') line ' . $trace;
    //trigger_error($msg, E_USER_ERROR);
    throw new \Accido\Exceptions\Model($msg);
  }

  /**
   * resolve_error
   * 
   * @param mixed $msg
   * @param array $params
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function resolve_error($msg, array $params){
    if (!is_string($msg)){
      $msg = (string)$msg;
    }
    foreach($params as $id => $value){
      if (!is_string($value)){
        $value = var_export($value, true);
      }
      $msg = str_ireplace($id, $value, $msg);
    }
    return $msg;
  }

  /**
   * array_merge
   *
   * @param array $one
   * @param array $two
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  protected function array_merge(array &$one, array &$two){
    $four               = array_unique(array_merge(array_keys($one), array_keys($two)));
    foreach($four as $key){
      if (array_key_exists($key, $two)){
        if (!is_array($two[$key]))
          $one[$key]    = $two[$key];
        elseif(!array_key_exists($key, $one) || !is_array($one[$key]))
          $one[$key]    = $two[$key];
        else{
          $this->array_merge($one[$key], $two[$key]);
        }
      }
    }
  }

  /**
   * get_param
   *
   * @param array $data
   * @param mixed $offset
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   * @throws \Accido\Exceptions\Model
   */
  protected function get_param(&$data, $offset){
    try{
      if(!is_array($data)){
        throw new \Accido\Exceptions\Model;
      }
      elseif(array_key_exists($offset, $data)){
        return $data[$offset];
      }
      elseif(false !== ($pos = strpos($offset, self::ATTR_PARAM_DELIMITER))){
        $key      = substr($offset, $pos+1);
        $temp     = substr($offset, 0, $pos);
        if (array_key_exists($temp, $data)){
          $offset = $temp;
          return $this->get_param($data[$offset], $key);
        }
      }
      throw new \Accido\Exceptions\Model;
    }
    catch(\Accido\Exceptions\Model $e){
      $msg        = $e->getMessage();
      $msg        = (0 === strlen($msg)) ? $offset : $offset . self::ATTR_PARAM_DELIMITER . $msg;
      throw new \Accido\Exceptions\Model($msg);
    }
  }

  /**
   * 
   * @param &array $data
   * @param scalar $offset
   * @param &mixed $result
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @throws \Accido\Exceptions\Model
   *
   * @return void
   */
  protected function bind_param(&$data, $offset, &$result){
    try{
      if(!is_array($data)){
        throw new \Accido\Exceptions\Model;
      }
      elseif(array_key_exists($offset, $data)){
        $data[$offset]    = &$result;
        return;
      }
      elseif(false !== ($pos = strpos($offset, self::ATTR_PARAM_DELIMITER))){
        $key              = substr($offset, $pos+1);
        $temp             = substr($offset, 0, $pos);
        if (array_key_exists($temp, $data)){
          $offset         = $temp;
          return $this->bind_param($data[$offset], $key, $result);
        }
      }
      throw new \Accido\Exceptions\Model;

      return;
    }
    catch(\Accido\Exceptions\Model $error){
      $msg                = $error->getMessage();
      $msg                = (0 === strlen($msg)) ? $offset : $offset . self::ATTR_PARAM_DELIMITER . $msg;
      throw new \Accido\Exceptions\Model($msg);
    }
  }

  /**
   * &reference_param
   * 
   * @param &array $data
   * @param mixed $offset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   * @throws \Accido\Exceptions\Model;
   *
   * @return &mixed
   */
  protected function &reference_param(&$data, $offset){
    try{
      if(!is_array($data)){
        throw new \Accido\Exceptions\Model;
      }
      elseif(array_key_exists($offset, $data)){
        return $data[$offset];
      }
      elseif(false !== ($pos = strpos($offset, self::ATTR_PARAM_DELIMITER))){
        $key      = substr($offset, $pos+1);
        $temp     = substr($offset, 0, $pos);
        if (array_key_exists($temp, $data)){
          $offset = $temp;
          $temp   = &$this->reference_param($data[$offset], $key);
          return $temp;
        }
      }
      throw new \Accido\Exceptions\Model;
    }
    catch(\Accido\Exceptions\Model $error){
      $msg        = $error->getMessage();
      $msg        = (0 === strlen($msg)) ? $offset : $offset . self::ATTR_PARAM_DELIMITER . $msg;
      throw new \Accido\Exceptions\Model($msg);
    }
  }

  /**
   * set_param
   *
   * @param array $data
   * @param mixed $offset
   * @param mixed $value
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   * @throws \Accido\Exceptions\Model
   */
  protected function set_param(&$data, $offset, $value){
    try{
      if(!is_array($data)){
        throw new \Accido\Exceptions\Model;
      }
      elseif(array_key_exists($offset, $data)){
        $data[$offset]  = $value;
        return;
      }
      elseif(false !== ($pos = strpos($offset, self::ATTR_PARAM_DELIMITER))){
        $key            = substr($offset, $pos+1);
        $temp           = substr($offset, 0, $pos);
        if (array_key_exists($temp, $data)){
          $offset       = $temp;
          return $this->set_param($data[$offset], $key, $value);
        }
      }
      throw new \Accido\Exceptions\Model;

    }
    catch(\Accido\Exceptions\Model $e){
      $msg        = $e->getMessage();
      $msg        = (0 === strlen($msg)) ? $offset : $offset . self::ATTR_PARAM_DELIMITER . $msg;
      throw new \Accido\Exceptions\Model($msg);
    }
  }

  /**
   * unset_param
   *
   * @param array $data
   * @param mixed $offset
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function unset_param(&$data, $offset){
    if(!is_array($data)){
      throw new \Accido\Exceptions\Model;
    }
    elseif(array_key_exists($offset, $data)){
      unset($data[$offset]);
      return;
    }
    elseif(false !== ($pos = strpos($offset, self::ATTR_PARAM_DELIMITER))){
      $key            = substr($offset, $pos+1);
      $temp           = substr($offset, 0, $pos);
      if (array_key_exists($temp, $data)){
        $offset       = $temp;
        return $this->unset_param($data[$offset], $key);
      }
    }
  }

  /**
   * register_param
   *
   * @param array $data
   * @param mixed $offset
   * @param mixed $value
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  protected function register_param(&$data, $offset, $value){
    $return           = false;
    if(!is_array($data) || array_key_exists($offset, $data)){
      return false;
    }
    elseif(false !== ($pos = strpos($offset, self::ATTR_PARAM_DELIMITER))){
      $key            = substr($offset, $pos+1);
      $temp           = substr($offset, 0, $pos);
      $offset         = $temp;
      if (!array_key_exists($temp, $data)){
        $data[$offset]= array();
      }
      return $this->register_param($data[$offset], $key, $value);
    }
    $data[$offset]  = $value;
    return true;
  }

  /**
   * exists
   *
   * @param array $data
   * @param mixed $offset
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  protected function exists(&$data, $offset){
    if(!is_array($data)){
      return false;
    }
    elseif(array_key_exists($offset, $data)){
      return true;
    }
    elseif(false !== ($pos = strpos($offset, self::ATTR_PARAM_DELIMITER))){
      $key            = substr($offset, $pos+1);
      $temp           = substr($offset, 0, $pos);
      if (array_key_exists($temp, $data)){
        $offset = $temp;
        return $this->exists($data[$offset], $key);
      }
    }
    
    return false;
  }

  /**
   * capture
   * 
   * @param Injector $injector
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return null
   */
  public function capture(Injector $injector){
    return null;
  }
  
  /**
   * __construct
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function __construct (){
    $this->init_model();
  }

  /**
   * to_array
   *
   * @uses $_vars
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return array
   */
  public function to_array(){
    if(self::ATTR_ARRAY === $this->_type) return $this->_vars;
    else return null;
  }

  /**
   * merge
   *
   * @param array $vars
   * @uses $this->vars
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function merge(array $vars){
    $this->array_merge($this->vars, $vars);
  }

  /**
   * bind
   * 
   * @param string $path
   * @param mixed $target
   * @uses $vars
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return self
   */
  public function bind($path, &$target){
    $this->bind_param($this->vars, $path, $target);
    return $this;
  }

  /**
   * &reference
   * 
   * @param mixed $path
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return &mixed
   */
  public function &reference($path){
    try{
      if (empty($path)) return $this->vars;
      $return                           = &$this->reference_param($this->vars, $path);
      return $return;
    }
    catch(\Accido\Exceptions\Model $error){
      $this->ensure(true, 'Malfunction parameter ' . $error->getMessage());
    }
  }

  /**
   * &ref
   * 
   * @param mixed $path
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return &mixed
   */
  public function &ref($path = ''){
    try{
      if (empty($path)) return $this->vars;
      $return                           = &$this->reference_param($this->vars, $path);
      return $return;
    }
    catch(\Accido\Exceptions\Model $error){
      $this->ensure(true, 'Malfunction parameter ' . $error->getMessage());
    }
  }

  // public __get(key) {{{ 
  /**
   * __get
   * 
   * @param mixed $key 
   * @access public
   * @return mixed
   */
  public function __get( $key ){
    try{
      $return                           = $this->get_param($this->vars, $key);
      return $return instanceof Closure ? $return($this) : $return;
    }
    catch(\Accido\Exceptions\Model $error){
      $msg                              = $error->getMessage();
      $this->ensure(true, 'Malfunction parameter ' . $msg);
    }
  }
  // }}}
  
  /**
   * trigger
   * 
   * @param string $key
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function trigger($key){
    $current                            = '';
    $stack                              = explode(self::ATTR_PARAM_DELIMITER, $key);
    $result                             = false;
    foreach($stack as $item){
      $current                          .= (empty($current) ? '' : self::ATTR_PARAM_DELIMITER) . $item;
      if(isset($this->option[$current])){
        $event                          = new stdclass;
        $event->done                    = true;
        $event->detail                  = $this->__get($current);
        $this->option[$current]->update($event);
        $result                         = true;
      }
    }
    return $result;
  }

  // public __set(key,value) {{{ 
  /**
   * __set
   * 
   * @param mixed $key 
   * @param mixed $value 
   * @access public
   * @return mixed
   */
  public function __set( $key, $value ){
    try{
      $this->set_param($this->vars, $key, $value);
      $this->trigger($key);
      return $value;
    }
    catch(\Accido\Exceptions\Model $e){
      $msg    = $e->getMessage();
      $this->ensure(true, 'Malfunction parameter ' . $msg);
    }
  }
  // }}}

  /**
   * Call the ctor function for $name dependency with $arguments
   *
   * @param string $name
   * @param array $arguments
   * @return mixed
   * @throws Exceptions_Dependency
   */
  public function __call($name, array $arguments) {
    try{
      $return = $this->get_param($this->vars, $name);
      $this->ensure(!$return instanceof Closure, "Dependency is not constructable: '$name'");
      array_unshift($arguments, $this);
      return call_user_func_array($return, $arguments);
    }
    catch(\Accido\Exceptions\Model $e){
      $msg    = $e->getMessage();
      $this->ensure(true, 'Malfunction parameter ' . $msg);
    }
  }

  /**
   * call
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function call(){
    try{
      $args = func_get_args();
      $name = array_shift($args);
      $return = $this->get_param($this->vars, $name);
      $this->ensure(!$return instanceof Closure, "Dependency is not constructable: '$name'");
      array_unshift($args, $this);
      return call_user_func_array($return, $args);
    }
    catch(\Accido\Exceptions\Model $e){
      $msg    = $e->getMessage();
      $this->ensure(true, 'Malfunction parameter ' . $msg);
    }
  }

  // public __isset(key) {{{ 
  /**
   * __isset
   * 
   * @param mixed $key 
   * @access public
   * @return bool
   */
  public function __isset( $key ){
    return $this->exists($this->vars, $key);
  }
  // }}}

  /**
   * __unset
   *
   * @param mixed $key
   * @uses vars
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function __unset($key){
    $this->unset_param($this->vars, $key);
  }

  // public get(key) {{{ 
  /**
   * get
   * 
   * @param mixed $key 
   * @access public
   * @return mixed
   */
  public function get( $key ){
    if (empty($key)) return $this->vars;
    return $this->__get( $key );
  }
  // }}}

  /**
   * stream
   *
   * @param scalar $key
   * @param scalar $alias
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Stream
   */
  public function stream($key, $alias = null){
    $result                         = null;
    if(!isset($this->option[$key])){
      $this->option[$key]           = new Stream($this, $key, $alias);
    }
    return $this->option[$key];
  }

  // public set(key,value) {{{ 
  /**
   * set
   * 
   * @param mixed $key 
   * @param mixed $value 
   * @access public
   * @return mixed
   */
  public function set( $key, $value ){
    if(1 === func_num_args() && is_array($key)){
      $this->vars                   = $key;
    }
    return $this->__set( $key, $value );
  }
  // }}}

  /**
   * add
   *
   * @param mixed $key
   * @param mixed $value
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function add($key, $value){
    return $this->register_param($this->vars, $key, $value);
  }

  /**
   * Reuse the instance created by $ctor
   *
   *
   * @param Closure $ctor
   * @return Closure
   */
  public function reuse(Closure $ctor) {
    return self::$dic->reuse($ctor);
  }

  /**
   * sanitize_model
   * 
   * @param string $model
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function sanitize_model($model){
    $model                            = preg_replace('`[\/\\ \s]+`', '\\', $model);
    return 'Accido\\Models\\' . trim($model, '\\');
  }

  /**
   * register_model
   * 
   * @param mixed $model
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Model
   */
  public function register_model($model){
    $loader                             = new Loader(self::$dic);
    $arguments                          = func_get_args();
    return call_user_func_array([$loader, 'model'], $arguments);
  }

  /**
   * register_submodel
   * 
   * @param string $model
   * @param string $locale
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Model
   */
  public function register_submodel($model, $locale){
    $loader                             = new Loader($this);
    $arguments                          = func_get_args();
    return call_user_func_array([$loader, 'model'], $arguments);
  }

  /**
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Model
   */
  public function register(){
    $loader                             = new Loader(self::$dic);
    $arguments                          = func_get_args();
    return call_user_func_array([$loader, 'model'], $arguments);
  }

  /**
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Model
   */
  public function instance(){
    $loader                             = new Loader(self::$dic);
    $arguments                          = func_get_args();
    return call_user_func_array([$loader, 'instance'], $arguments);
  }

  /**
   * destroy
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright   2014 andrew scherbakov
   * @license MIT http://opensource.org/license/MIT
   *
   * @return void
   */
  public function destroy(){
    if($this->instance_args instanceof Model){
      $this->instance_args->destroy();
    }
    $this->injector                       = null;
    $this->vars                           = null;
    $this->instance_args                  = null;
  }

  /**
   * register_namespace
   *
   * @param string $namespace
   * @param string $path_to_root
   * @uses autoloader
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function register_namespace($namespace, $path_to_root){
    self::$autoloader->register_namespace($namespace, $path_to_root);
  }

  /**
   * Returns a closure wrapped for usage as a dependency,
   * to distinguish from a ctor-closure.
   *
   *
   * @param Closure $value
   * @return Closure
   */
  public function wrapClosure(Closure $value) {
    return function() use ($value) {
        return $value;
    };
  }

  /**
   * delete
   *
   * @param mixed $key
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return true
   */
  public function delete($key){
    $this->set($key, self::ATTR_NULL_PARAMETER);
    return true;
  }

  /**
   * offsetSet
   *
   * @param mixed $offset
   * @param mixed $value
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function offsetSet($offset, $value) {
    if (!is_null($offset)) {
      return $this->__set($offset, $value);
    }
  }

  /**
   * offsetExists
   *
   * @param mixed $offset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function offsetExists($offset) {
    return $this->__isset($offset);
  }

  /**
   * offsetUnset
   *
   * @param mixed $offset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  public function offsetUnset($offset) {
    $this->__unset($offset);
  }

  /**
   * offsetGet
   *
   * @param mixed $offset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function offsetGet($offset) {
    return $this->__get($offset);
  }
}

Model::$dic         = new DIC;
global $accautoloader;
Model::$autoloader  = $accautoloader;
