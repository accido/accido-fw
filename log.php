<?php
namespace Accido;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Log 
 * 
 * @package 
 * @version $id$
 * @copyright 2013 Accido
 * @author Andrew Scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 // }}}
 */
interface Log {
  
  /**
   * add_msg
   *
   * @param mixed $msg
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return int id message
   */
  public function add_msg( $msg );

  /**
   * insert_msg
   *
   * @param mixed $msg
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return int id message
   */
  public function insert_msg( $msg );

  
  /**
   * delete_msg
   *
   * @param string|id $msg message or id
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function delete_msg( $msg );

  // public read_msg(id) {{{ 
  /**
   * read_msg
   * 
   * @param int $id 
   * @access public
   * @return string
   */
  public function read_msg( $id );
  // }}}

}
