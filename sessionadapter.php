<?php
namespace Accido;
defined('CORE_ROOT') or die('No direct script access.');
class SessionAdapter{
  public function __call($func_name, $func_args){
    return call_user_func_array($this->$func_name, $func_args);
  }
}
