<?php
namespace Accido;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Inreface: Cache
 * 
 * @package Cache
 *
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
interface Cache{

  /**
   * OPT_MODEL_HELPER
   *
   * @const string
   */ 
  const OPT_MODEL_HELPER                  = 'model_helper';

  /**
   * get_from_cache
   *
   * @param mixed $id
   * @param mixed $default
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function get_from_cache($id, $default = NULL);

  /**
   * set_to_cache
   *
   * @param mixed $id
   * @param mixed $data
   * @param int $lifetime
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function set_to_cache($id, $data, $lifetime = 3600);

  /**
   * add_to_cache
   *
   * @param mixed $id
   * @param mixed $data
   * @param mixed $lifetime
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function add_to_cache($id, $data, $lifetime = 3600);

  /**
   * delete_from_cache
   *
   * @param mixed $id
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function delete_from_cache($id);

  /**
   * delete_all
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function delete_all();

  /**
   * id
   *
   * @param mixed $id
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function id($id);

  /**
   * save
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function save();
}
