<?php
namespace Accido;
use Accido\Option;
use Accido\Model;
use Generator,stdclass;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Promise
 *
 * @package Option
 * 
 * @final
 * 
 * @see Option
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
final class Promise extends Option {
  public function __construct($resolver, $key = null, Model $name = null, $locked = true){
    parent::__construct($key, $name);
    $this->resolve_promise($resolver, $locked);
  }

  public function do_update(stdclass $event){

  }

  /**
   * resolve_promise
   * 
   * @param &callable $resolver
   * @param bool $locked
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function resolve_promise(&$resolver, $locked){
    $_this                                                = $this;
    $lock                                                 = 0;
    $resolve                                              = function($data) use (&$lock, &$_this, &$locked) {
      if(!($lock++ && $locked)){
        $object                                           = new stdclass;
        $object->detail                                   = $data;
        $object->done                                     = true;
        $_this->do_resolve_promise($object);
      }
    };
    $reject                                               = function($reasone) use (&$lock, &$_this, &$locked) {
      if(!($lock++ && $locked)){
        $object                                           = new stdclass;
        $object->detail                                   = $reasone;
        $_this->do_resolve_promise($object);
      }
    };
    $this->_done                                          = function($data)
      use (&$resolver, &$resolve, &$reject, &$_this, &$lock, &$locked) {
      if($lock++ && $locked) return;
      $lock--;
      $generator                                          = $resolver($data, $resolve, $reject);
      if($generator instanceof Generator){
        $_this->resolve_generator($generator);
      }
    };
    $this->_fail                                          = $this->_done;
  }

  /**
   * do_resolve_promise
   * 
   * @param stdclass $event
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function do_resolve_promise(stdclass $event){
    parent::do_update($event);
  }

}
