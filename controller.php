<?php
namespace Accido;
use Accido\Model;
use Accido\View;
use Accido\Event;
use Accido\View\Null;
use Accido\Models\Request;
use Accido\Models\Response;
use Accido\Option;
use Accido\Loader;
use Exception,stdclass;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Controller
 *
 * @package Controller
 * 
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 andrew scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2013 <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
abstract class Controller {
  /**
   * stream
   *
   * @var array
   */
  public $stream                                            = null;

  /**
   * event 
   * 
   * @var string
   * @access public
   */
  public $event                                             = '';

  /**
   * template 
   * 
   * @var string
   * @access protected
   */
  protected $template                                       = '';

  /**
   * view 
   * 
   * @var View
   * @access public
   */
  public $view                                              = null;

  /**
   * charset
   *
   * @var string
   */
  public $charset                                           = '';

  /**
   * injector
   *
   * @var mixed
   */
  public $injector                                          = null;

  /**
   * request
   *
   * @var Request
   */
  public $request                                           = null;

  /**
   * response
   *
   * @var Response
   */
  public $response                                          = null;   

  /**
   * __construct
   * 
   * @access private
   * @return void
   */
  private function __construct(){

  }

  /**
   * register_model
   *
   * @param string $name
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Model
   */
  protected function register_model($name){
    $loader                             = new Loader(Model::$dic);
    $arguments                          = func_get_args();
    return call_user_func_array([$loader, 'model'], $arguments);
  }

  /**
   * handle
   * 
   * @uses $stream
   * @final
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  final public function handle(){
    $handled                                                = new Option($this);
    $this->stream->listen($handled);
    return $handled;
  }

  /**
   * commit
   * 
   * @uses $stream
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  final public function commit(){
    return $this->stream->commit();
  }

  /**
   * before
   * 
   * @final
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  final public function before(){
    $this->finalize();
  }

  /**
   * after
   * 
   * @final
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  final public function after(){
    $this->initialize();
  }

  /**
   * 
   * @final
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  final public function _action(){
    $this->action();
  }

  /**
   * 
   * @abstract
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  abstract protected function action();

  /**
   * initialize
   *
   * @abstract
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  abstract protected function initialize();

  /**
   * finalize
   *
   * @abstract
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  abstract protected function finalize();

  /**
   * run
   * 
   * @param View|null $view
   * @param array $params
   * @param Request $request
   * @final
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  final public function run(View $view = null, array $params = array(), Request $request = null){
    $this->injector                                         = Model::$dic;
    $template                                               = null;
    $_this                                                  = $this;
    $helper                                                 = $this->register_model('Helper');
    $this->charset                                          = 
      empty($this->charset) ? $helper->get(Model::OPT_CHARSET_ENCODING) : $this->charset;
    $after                                                  = function() use (&$_this, &$template){
      $_this->after();
      if(($_this->view instanceof View) && null !== $template){
        $_this->view->set_filename($template);
      }
      return $_this;
    };
    $this->injector->add('event', $this->injector->reuse(function(){
      return new Event;
    }));
    $event                                                  = $this->injector->event;
    if ($view instanceof View){
      $this->view                                           = $view;
      $template                                             = $view->get_filename();
      $this->view->set_filename($this->template);
    }
    else{
      if (0 !== strlen($this->template))
        $this->view                                         = View::factory($this->template);
      else
        $this->view                                         = Null::factory();
    }

    if (null === $request)
      $request                                              = new Request();
    $this->request($request);
    $this->response                                         = new Response();
    $this->set_params($params);
    $this->stream                                           = new Option();

    try{
      $event->trigger($this->event, $this->view, $this);
      $uri                                                  = $request[Request::OPT_REQUEST_URI];
      $uri                                                  = is_array($uri) ? implode('/', $uri) : $uri;
      if(!empty($uri)){
        $event->trigger($uri, $this->view, $this);
      }
    }
    catch( Exception $error){
      throw new \Accido\Exceptions\Controller($error);
    }
    $object                                                 = new stdclass;
    $object->done                                           = true;
    $object->detail                                         = $this;
    $result                                                 = new Option();
    $return                                                 = $result
      ->then(function(Controller $ctrl){
        $data                                               = new stdclass;
        $data->done                                         = true;
        $data->detail                                       = $ctrl;
        $ctrl->before();
        $ctrl->stream->update($data);
        return $ctrl->stream
          ->then()
          ->nocommit();
      })
      ->then($after)
      ->when(function($ctrl, $resolve, $reject){
        if(!($ctrl instanceof Controller)){
          $reject($ctrl);
          return;
        }
        $ctrl->commit()->then($resolve, $reject);
      });

    $result->update($object);
    return $return;
  }

  /**
   * request
   *
   * @param Request|null $request
   * @uses $this->request
   *
   * @final
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void|Request
   */
  final public function request(Request $request = null){
    if(null !== $request) $this->request = $request;
    else return $this->request;
  }

  /**
   * get_view
   *
   * @uses view
   *
   * @final
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   * @deprecated
   * @throws \Accido\Exceptions\Controller
   *
   * @return View
   */
  final public function get_view(){
    throw new \Accido\Exceptions\Controller('Called deprecated method ::get_view() try access to properties ::$view');
    return $this->view;
  }

  /**
   * get_charset
   *
   * @final
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   * @deprecated
   * @throws \Accido\Exceptions\Controller
   *
   * @return string
   */
  final public function get_charset(){
    throw new \Accido\Exceptions\Controller('Called deprecated method ::get_charset() try access to properties ::$charset');
    return $this->charset;
  }

  /**
   * set_params
   *
   * @param array $params
   *
   * @final
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  final public function set_params(array $params){
    $this->request->set(Request::OPT_PARAMETER_VARS, $params, $this->charset);
  }

  /**
   * execute
   * 
   * @param string $name
   * @param array $params
   * @param Request $request
   *
   * @static
   * @access public
   * @throws \Accido\Exceptions\Controller
   * @return Option
   */
  public static function execute($name, array $params = array(), Request $request = null){
    $classname            = 'Accido\\Controllers\\' . $name;
    $name                 = 'Controllers\\' . $name;
    if(!class_exists($classname, false) && !Model::$autoloader->find($classname)){
      $classname          = 'Accido\\Controllers\\Null';
    }
    $controller           = new $classname;
    if ( !( $controller instanceof self ) ){
      throw new \Accido\Exceptions\Controller( 'The class "' . $classname . '" not extends class Controller.');
    }
    $controller->event    = strtolower(str_ireplace(array( '\\', '/'), '_', $name));
    return $controller->run(null, $params, $request);
  }

  /**
   * extend
   * 
   * @param string $name
   * @param View $view 
   * @param array $params
   * @param Request $request
   *
   * @static
   * @access public
   * @throws Exception
   * @return Option
   */
  public static function extend($name, View $view, array $params = array(), Request $request = null){
    $classname            = 'Accido\\Controllers\\' . $name;
    $name                 = 'Controllers\\' . $name;
    if(!class_exists($classname, false) && !Model::$autoloader->find($classname)){
      $classname          = 'Accido\\Controllers\\Null';
    }
    $controller           = new $classname;
    if ( !( $controller instanceof self ) ){
      throw new Exception( 'The class "' . $classname . '" not extends class Controller.');
    }
    $controller->event    = strtolower(str_ireplace(array( '\\', '/'), '_', $name));

    return $controller->run($view, $params, $request);
  }

}

