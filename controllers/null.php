<?php
namespace Accido\Controllers;
use Accido\Controller;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Null 
 * 
 * @abstract Null
 * @package Controller
 * @version $id$
 * @copyright 2013 Accido
 * @author Andrew Scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
class Null extends Controller{
  
  protected $template = '';

  protected function action(){

  }

  protected function initialize(){

  }

  protected function finalize(){
    
  }
}
