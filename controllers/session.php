<?php 
namespace Accido\Controllers;
use Accido\Controller;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Controllers_Session
 *
 * @package Session
 * @subpackage Controller
 *
 * 
 * @see Controller
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Session extends Controller{

  protected $template = '';

  protected function do_action(){

  }

  protected function initialize(){

  }

  protected function finalize(){

  }
}
