<?php
namespace Accido\Controllers;
use Accido\Controller;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Permissions
 *
 * @package Permissions
 * @subpackage Controller
 *
 * 
 * @see Controller
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version 0.1
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Permissions extends Controller {
  
  protected $template                               = '';

  protected function action(){

  }

  protected function initialize(){
    
  }

  protected function finalize(){

  }
}
