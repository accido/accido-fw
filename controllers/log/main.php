<?php 
namespace Accido\Controllers\Log;
use Accido\Controller;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Log_Main
 *
 * @package Controllers_Log_Main
 * @subpackage Controller
 *
 * 
 * @see Controller
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version 0.1
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Main extends Controller{

  protected $template = 'log';

  protected function do_action(){

  }

  protected function initialize(){

  }

  protected function finalize(){

  }
}
