<?php 
namespace Accido\View;
use Accido\Model;
use Accido\Event;
use Exception;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Null
 *
 * @package Null
 * @subpackage View
 *
 * 
 * @see View
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version 0.1
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Null extends \Accido\View{

  /**
   * set_filename
   *
   * @param mixed $file
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function set_filename($file){
    try{
      parent::set_filename($file);
    }
    catch(Exception $e){
      //;
    }
  }

  /**
   * factory
   *
   * @param string $filename
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public static function factory($filename = ''){
    $result = new self('');
    return $result;
  }

  /**
   * render
   *
   * @param mixed $file
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function render($file = NULL){
    try{
      $content = parent::render($file);
    }
    catch(\Accido\Exceptions\View $e){
      $content = '';
    }

    return $content;
  }

}
