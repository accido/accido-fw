<?php
namespace Accido\Exceptions;
use Exception;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Permission
 *
 * @package Exception
 * @subpackage Permission
 *
 * 
 * @see Exception
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version 0.1
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Permission extends Exception{

}
