<?php
namespace Accido\Exceptions;
use Exception;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Helper 
 * 
 * @uses Exception
 * @package 
 * @version $id$
 * @copyright 2013 Accido
 * @author andrew scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
class Helper extends Exception{}
