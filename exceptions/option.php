<?php
namespace Accido\Exceptions;
use Exception;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Option
 *
 * @package Exception
 * @subpackage Option
 *
 * 
 * @see Exception
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version 0.1
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Option extends Exception{
  /**
   * detail
   *
   * @var mixed
   */
  public $detail                           = null;
}
