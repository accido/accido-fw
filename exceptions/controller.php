<?php
namespace Accido\Exceptions;
use Exception;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Controller
 *
 * @package Exception
 * @subpackage Controller
 *
 * 
 * @see Exception
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version 0.1
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Controller extends Exception{
  public $error                                     = null;
  public function __construct($error){
    $this->error                                    = $error;
    parent::__construct('Failed exceuted controller.');
  }
}
