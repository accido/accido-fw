<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Event;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Embera
 *
 * @package Embera
 * @subpackage Model
 *
 * 
 * @see Model
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Embera extends Model{

  const OPT_EMBERA                                    = 'embera';

  const FUNC_EMBERA_PARSE                             = 'parse';

  const FUNC_EMBERA_INFO                              = 'info';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                                     = array(
    self::OPT_EVENTS                                  => array(
      
    ),
    self::OPT_EMBERA                                  => null,
    self::FUNC_EMBERA_PARSE                           => null,
    self::FUNC_EMBERA_INFO                            => null,
  );

  /**
   * init
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function init(){
    $dic    = Model::$dic;
    $this->register_namespace('Embera', CORE_ROOT . '/embera/Lib');
    $this->register_namespace('Psr\\Log', CORE_ROOT . '/psr/log');
    $serv   = $this->reuse(function(){
      return new \Embera\Embera();
    });
    $dic->add('embera', $serv);
    $this->set(self::OPT_EMBERA, $dic->get_closure('embera'));
    $this->set(self::FUNC_EMBERA_PARSE, function($container, $data){
      return $container->embera->autoEmbed($data);
    });
    $this->set(self::FUNC_EMBERA_INFO, function($container, $dataurl){
      return $container->embera->getUrlInfo($dataurl);
    });
  }

}
