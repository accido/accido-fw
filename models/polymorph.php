<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Injector;
use Accido\Controller;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Polymorph
 *
 * @package Render
 * @subpackage Polymorph
 * 
 * @see Render
 * 
 * @see Polymorph
 *
 * @see Model
 * @final
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 andrew scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2013 <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
final class Polymorph extends Model implements \Accido\Render{
  
  /**
   * OPT_NAME
   *
   * @const string
   */
  const OPT_NAME                                              = 'name';
  
  /**
   * vars
   *
   * @var array
   */
  protected $vars                                             = array(
    self::OPT_NAME                                            => '',
  );

  /**
   * init
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function init(){
  
  }

  /**
   * capture
   * 
   * @param Injector $dic
   * @param string $model
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function capture(Injector $dic, $model = ''){
    $this->ensure(empty($model) || !is_string($model), 'Polymorph: name is wrong or missed.');
    $this[self::OPT_NAME]                                     = $model;
  }

  /**
   * __invoke
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function __invoke(){
    $args                                                     = func_get_args();
    array_unshift($args, $this[self::OPT_NAME]);
    $model                                                    = call_user_func_array(array($this, 'register'), $args);
    $this->ensure(!($model instanceof \Accido\Polymorph), 'Model are not implements interface Polymorph.');
    return call_user_func(array($model, 'done'));
  }

}

