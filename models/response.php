<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Models\Response\Http\Header as HttpHeader;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Response
 *
 * @package Response
 * @subpackage Model
 *
 * @see Response
 * 
 * @see Model
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Response extends Model{
  /**
   * @const string
   */
  const OPT_HEADER                                = 'header';
  /**
   * @const string
   */
  const OPT_BODY                                  = 'body';

  protected $vars                                 = [
    self::OPT_HEADER                              => null,
    self::OPT_BODY                                => null,
  ];

  protected function init(){
    $this[self::OPT_HEADER]                       = new HttpHeader(); 
  }

  /**
   * http
   * 
   * @param array $header
   * @param int $status
   * @param string $message
   * @param string $body
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function http(array $header, $status = HttpHeader::CODE_200, $message = 'Ok', $body = ''){
    $model                                        = $this[self::OPT_HEADER];
    $model[HttpHeader::OPT_RESPONSE_TRANSPORT]    = HttpHeader::ATTR_HTTP_TRANSPORT_1_1;
    if($length = strlen($body)){
      $header[HttpHeader::CONTENT_LENGTH]         = $length;
    }
    $model[HttpHeader::OPT_HEADER]                = array_merge_recursive($model[HttpHeader::OPT_HEADER], $header);
    $model[HttpHeader::OPT_RESPONSE_CODE]         = $status;
    $model[HttpHeader::OPT_RESPONSE_MESSAGE]      = $message;
    $this[self::OPT_BODY]                         = $body;
  }

}

