<?php
namespace Accido\Models\Cache\File;
use Accido\Models\Cache\File;
use Accido\HashMap;
use Accido\ArrayMap;
use SplFileInfo,Exception;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Tag
 *
 * @package Cache
 * @subpackage Model
 *
 * 
 * @see File
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Tag extends File{

  /**
   *
   */
  const TAG_SUFFIX    = '`tag';

  /**
   *
   */
  const LOCK_PREFIX   = 'lock-';

  /**
   *
   */
  const MAX_LOOP_LOCK = 10;

  /**
   * MAX_TAG_COUNT 
   * 
   * with it value of number tags in cache the usage memory will be over 2 MB
   * but on fastCGI PHP the usage memory will be only 512 KB
   *
   * @const int
   */
  const MAX_TAG_COUNT = 65536;  

  /**
   * @var HashMap
   */
  protected $tags = null;

  /**
   * @var string
   */
  protected $_tags_dir = null;

  /**
   * @var string
   */
  protected $_parts_dir = null;

  /**
   * @var string
   */
  protected $id = '';

  /**
   * @var array
   */
  protected $global_tags = array();

  /**
   * @var bool
   */
  private $_istag = false;

  /**
   * @var bool
   */
  private $_ispart = false;

  /**
   * _isdirty 
   * 
   * @var mixed
   * @access private
   */
  private $_isdirty = false;

  /**
   * _start_time
   *
   * @var int
   */
  private $_start_time = 0;

  /**
   * _start_memory
   *
   * @var int
   */
  private $_start_memory = 0;

  /**
   * _resolve_directory
   *
   * @param mixed $filename
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  protected function _resolve_directory($filename)
  {
    if ( $this->_istag )
      return $this->get(self::OPT_CACHE_TAGS_DIR)->getRealPath().DIRECTORY_SEPARATOR.$filename[0].$filename[1].DIRECTORY_SEPARATOR;
    elseif( $this->_ispart )
      return $this->get(self::OPT_CACHE_PARTS_DIR)->getRealPath().DIRECTORY_SEPARATOR.$filename[0].$filename[1].DIRECTORY_SEPARATOR;
    else
      return $this->get(self::OPT_CACHE_DIR)->getRealPath().DIRECTORY_SEPARATOR.$filename[0].$filename[1].DIRECTORY_SEPARATOR;
  }

  /**
   * getKey
   *
   * @param mixed $sid
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return int
   */
  protected function getKey( $sid){
    return abs( crc32( $sid ) );
  }

  /**
   * tag_load
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return ArrayMap
   */
  protected function tag_load(){
    
    $obj = parent::get_from_cache( '--cache-tags', null );

    if(isset($obj['data'])){
      $obj = $obj['data'];
    }
    
    if ( !($obj instanceof HashMap) ) $obj = new HashMap( 'V', 'v', 50000 );
    
    return $obj;
  }

  /**
   * tag_save
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  protected function tag_save(){
    return $this->set_with_tags( '--cache-tags', $this->tags, 0, array( '--cache-tags' ) );
  }

  /**
   * add_tag
   *
   * @param mixed $key
   * @param mixed $value
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function add_tag( $key, $value ){
    $this->tags->attach( $key, $value );
  }

  /**
   * set_tag
   *
   * @param mixed $key
   * @param mixed $value
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function set_tag( $key, $value ){
    $this->tags[$key] = $value;
  }

  protected function getMTime( $id ){
    $filename   = $this->filename($this->_sanitize_id($id));
    $directory  = $this->_resolve_directory($filename);
    // Wrap operations in try/catch to handle notices
    try
    {
      // Open file
      $file = new SplFileInfo($directory.$filename);

      // If file does not exist
      if ( ! $file->isFile())
      {
        // Return default value
        return false;
      }
      else
      {
        $this->set(self::OPT_CACHE_MTIMED, $this->get(self::OPT_CACHE_MTIMED) + 1);
        $mt = $file->getMTime();
        $this->log( 'mimetime', $mt, $filename );
        return $mt;
      }

    }
    catch (ErrorException $error)
    {
      $this->ensure($error->getCode() === E_NOTICE, __METHOD__.' failed time of last modification : '.$error->getMessage());
      // Otherwise throw the exception
      throw $error;
    }
  }

  /**
   * init
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function init(){
    $this->_start_time    = microtime(true);
    $this->_start_memory  = memory_get_usage(true);

    parent::init();
    $options = $this->get(self::OPT_CACHE_OPTIONS);
    //wp_load_translations_early();
    

    $this->get_dir(self::OPT_CACHE_TAGS_DIR, $options['dir'] . 'tags/');

    $this->get_dir(self::OPT_CACHE_PARTS_DIR, $options['dir'] . 'parts/');

    //$this->tags  = new HashMap('V', 'v', 0);

    $this->tags = $this->tag_load();

    $this->_isdirty = false;
    
    /**
     * check if exists cache tag key
     */
    $this->get_tags( array( $this->id_tag( '--cache-tags' ) ) );

    $this->_start_time    = round((microtime(true) - $this->_start_time) * 1000.0, 3);
    $this->_start_memory  = round((memory_get_usage(true) - $this->_start_memory)/1024.0, 3);
    
  }

  /**
   * id_tag
   *
   * @param mixed $sid
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function id_tag ($sid ){
    if (!in_array( $sid, $this->global_tags))
      $sid .= '`' . $this->get(self::OPT_CACHE_ID);
    return $this->getKey($sid . self::TAG_SUFFIX);
  }

  /**
   * @param $sid
   * @return bool
   */
  public function lock($sid){
    return true;
  }

  /**
   * @param $sid
   * @return bool
   */
  public function unlock( $sid){
    return true;
  }

  /**
   * delete_tag
   *
   * @param mixed $sid
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function delete_tag( $sid ){
    $return = false;
    if( $this->lock( $sid)){
      $key          = $this->id_tag($sid);
      $kindex       = $key;
      $process      = $this->_istag;
      $this->_istag = true;
      $flag         = true;
      while ( !( $vindex = $this->increment( $kindex, 1 ) ) ){
        /**
         * the tag will be early removed from cache 
         * try to recover it from current value if exists
         */
        if ( null !== ( $find = $this->tags[$key] ) ){
          $this->add( $kindex, $find, 0);
          $flag = true;
        }
        else{
          $this->add( $kindex, 1, 0 );
          $flag = false;
        }
      }
      if ( $flag ){
        $this->set_tag( $key, $vindex );
      }
      else{
        $this->add_tag( $key, $vindex );
      }
      $return                     = $vindex;
      $this->_istag               = $process;
      $this->unlock($sid);
      $this->_isdirty             = true;
    }
    return $return;
  }

  /**
   * get_tags
   *
   * @param array $request
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function get_tags( array $request ){
    $process                    = $this->_istag;
    $this->_istag               = true;
    $rel                        = array();
    $result                     = array();
    foreach( $request as $tag ){
      $ktemp                    = $tag;
      if ( null === ( $find = $this->tags[$tag] ) )
      {
        $rel[$ktemp]            = $tag;
      }
      else {
        $this->set(self::OPT_CACHE_MTIMED, $this->get(self::OPT_CACHE_MTIMED) + 1);
        if ( $this->get(self::OPT_CACHE_DEBUG) ) $this->log( 'mimetime', $ktemp, $find );
        $result[$tag]           = $find;
      }
    }
    if ( !empty( $rel ) ){
      $this->_isdirty = true;
      foreach( $rel as $ktemp => $tag ){
        $value = false;
        $loop = self::MAX_LOOP_LOCK;
        while( false === ( $value = parent::get_from_cache( $ktemp, false ) ) && $loop-- > 0 ){
          $this->add_to_cache( $ktemp, 1, 0);
        }
        $result[ $tag ]  = $value;
        $this->add_tag( $tag, $value );
      }
    }
    $this->_istag = $process;

    return $result;
  }

  /**
   * set_with_tags
   *
   * @param mixed $id
   * @param mixed $data
   * @param mixed $lifetime
   * @param array $tags
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function set_with_tags($id, $data, $lifetime = null, array $tags = null){
    $tags_temp = array_map( array( $this, 'id_tag' ), (array)$tags );
    $data_temp = array( 'tags' => $this->get_tags( $tags_temp ), 'data' => &$data );
    return $this->set_to_cache( $id, $data_temp, $lifetime );
  }

  /**
   * find
   *
   * @param mixed $tag
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function find($tag)
  {
    throw new Exception('Object File Tags does not support finding by tag');
  }

  /**
   * get_from_cache
   *
   * @param mixed $id
   * @param mixed $default
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function get_from_cache($id, $default = false){
    $cached_value = parent::get_from_cache( $id, $default);
    $temp_value   = null;
    if(!is_array($cached_value)){
      $temp_value = array(&$cached_value);
    }
    else{
      $temp_value = &$cached_value;
    }
    if ( array_key_exists( 'tags', $temp_value) && array_key_exists( 'data', $temp_value)){
      $return = $temp_value['data'];
      foreach( $this->get_tags( array_keys( $temp_value['tags'] ) ) as $tag => $value){
        if ( $value !== $temp_value['tags'][$tag] ){
          $return = $default;
          $this->set(self::OPT_CACHE_REJECTED, $this->get(self::OPT_CACHE_REJECTED) + 1);
          if ( $this->get(self::OPT_CACHE_DEBUG) ) $this->log( 'reject', $id, "invalid tag $tag" );
          break;
        }
      }
    }
    else{
      $return = $cached_value;
    }
    return $return;
  }

  /**
   * global_tags
   *
   * @param array $tags
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function global_tags(array $tags)
  {
    if (!is_array($this->global_tags)) {
      $this->global_tags = array();
    }

    $this->global_tags = array_merge(
      array_values($this->global_tags),
      $tags
    );

    $this->global_tags = array_unique($this->global_tags);
    if ( !empty( $this->global_tags ) )
      $this->global_tags = array_combine($this->global_tags, $this->global_tags);
  }

  /**
   * parts
   *
   * @param mixed $mindex
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function parts( $mindex = null){
    if ( $mindex !== null && is_bool($mindex) )
      $this->_ispart = $mindex;
    return $this->_ispart;
  }

  /**
   * save
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function save(){
    if ( $this->_isdirty ){
      $loop       = self::MAX_LOOP_LOCK;
      $lock       = $this->_sanitize_id( '--cache-save-lock' );
      $over       = ( count( $this->tags ) > self::MAX_TAG_COUNT );
      /**
       * try to lock operation
       */
      while( false === ( $locked = $this->add_to_cache( $lock, 1, 0 ) ) && $loop-- > 0 ){
        usleep(100000);
      }
      if ( false !== $locked ){
        trigger_error('Lock cache tags key successfull.', E_USER_NOTICE);
        $key        = $this->id_tag( '--cache-tags' );
        $cache_tag  = $key;
        $check      = parent::get_from_cache( $cache_tag, false );
        /**
         * try to merge new changes if exists 
         */
        if ( !$over && $check && ( $check > $this->tags[$key] ) ){
          $old_tags = $this->tag_load();
          $ntemp = array();
          foreach( $old_tags as $index => $tag ){
            /**
             * repair and merge
             */
            if ( !( isset( $this->tags[$index] ) && $tag <= $this->tags[$index] ) ){
              if ( isset( $this->tags[$index] ) ){
                $this->set_tag( $index, $tag );
              }
              else{
                $ntemp[$index] = $tag;
              }
              trigger_error('Merge or repair tag ' . $index .'/' . $tag . '.', E_USER_NOTICE);
            }
          }
          foreach( $ntemp as $key => $tag ){
            $this->add_tag( $key, $tag );
          }
          //$this->tags->sort();
          $over = ( count( $this->tags ) > self::MAX_TAG_COUNT );
          $this->set_tag( $key, $check );
        }
        /**
         * save changes and unlock
         */
        $value  = $this->delete_tag( '--cache-tags' );
        $result = false;
        if (  $over || !( $result = $this->tag_save() ) ){
          if ($over)
            trigger_error('Cache tags cleaned.', E_USER_NOTICE);
          else
            trigger_error('Empty cache tags saved.', E_USER_NOTICE);
          /**
           * new default tags array 
           */
          $this->tags = new HashMap('V', 'v', 1);
          $this->tags->attach( $key, $value );
          $this->tag_save();
        }
        else{
          trigger_error('Cache loaded time ' . $this->_start_time 
                        . ' ms. Cache load memory ' . $this->_start_memory . ' KB. '
                        . count( $this->tags ) . ' tags in cache. New cache tags saved.', E_USER_NOTICE);
        }
        $this->delete_from_cache( $lock );
      }
      else{
        $this->delete_from_cache( $lock );
        trigger_error('Cache tags key cann\'t be locked.', E_USER_NOTICE);
      }
    }
    else{
      trigger_error('Cache loaded time ' . $this->_start_time 
                        . ' ms. Cache load memory ' . $this->_start_memory . ' KB. '
                        . count( $this->tags ) . ' tags in cache. Cache tags aren\'t changed.', E_USER_NOTICE);
    }
  }

}
