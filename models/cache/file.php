<?php
namespace Accido\Models\Cache;
use Accido\Model;
use Accido\Cache;
use SplFileInfo,DirectoryIterator;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: File
 *
 * @package Cache
 * @subpackage Model
 *
 * @see Cache
 * 
 * @see Model
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class File extends Model implements Cache{

  /**
   * OPT_CACHE_OPTIONS
   *
   * @const string
   */
  const OPT_CACHE_OPTIONS                 = 'cache';

  const OPT_CACHE_DIR                     = 'cache_dir';

  const OPT_CACHE_GETTED                  = 'cache_getted';

  const OPT_CACHE_SETTED                  = 'cache_setted';

  const OPT_CACHE_REJECTED                = 'cache_rejected';

  const OPT_CACHE_MTIMED                  = 'cache_mtimed';

  const OPT_CACHE_DEBUG                   = 'cache_debug';

  const OPT_CACHE_LOG                     = 'cache_log';

  const OPT_CACHE_DEFAULT_TIMEOUT         = 'cache_timeout';

  const OPT_CACHE_ID                      = 'cache_id';

  const OPT_CACHE_PARTS_DIR               = 'cache_parts_dir';

  const OPT_CACHE_TAGS_DIR                = 'cache_tags_dir';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                         = array(
    self::OPT_EVENTS                      => array(
      
    ),
    self::OPT_CACHE_OPTIONS               => array(),
    self::OPT_CACHE_DIR                   => null,
    self::OPT_CACHE_GETTED                => 0,
    self::OPT_CACHE_SETTED                => 0,
    self::OPT_CACHE_REJECTED              => 0,
    self::OPT_CACHE_MTIMED                => 0,
    self::OPT_CACHE_DEBUG                 => false,
    self::OPT_CACHE_LOG                   => array(),
    self::OPT_CACHE_DEFAULT_TIMEOUT       => 0,
    self::OPT_CACHE_ID                    => 0,
    self::OPT_CACHE_PARTS_DIR             => '',
    self::OPT_CACHE_TAGS_DIR              => '',
  );

  /**
   * filename
   *
   * @param mixed $string
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  protected function filename($string)
	{
		return sha1($string).'.cache';
	}

  /**
   * _sanitize_id
   *
   * @param mixed $id
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  protected function _sanitize_id($id)
	{
		// Change slashes and spaces to underscores
		return str_replace(array('/', '\\', ' '), '_', $id);
	}

  /**
   * _make_directory
   *
   * @param mixed $directory
   * @param int $mode
   * @param mixed $recursive
   * @param mixed $context
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return SplFileInfo
   */
  protected function _make_directory($directory, $mode = 0700, $recursive = FALSE, $context = NULL)
	{
    if (null !== $context) $result = mkdir($directory, $mode, $recursive, $context);
    else $result = mkdir($directory, $mode, $recursive);
    $this->ensure(!$result, 'Failed to create the defined cache directory : :directory', array(':directory' => $directory));
		chmod($directory, $mode);

		return new SplFileInfo($directory);
	}

  /**
   * _resolve_directory
   *
   * @param mixed $filename
   * @uses OPT_CACHE_DIR
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  protected function _resolve_directory($filename)
	{
		return $this->get(self::OPT_CACHE_DIR)->getRealPath().DIRECTORY_SEPARATOR.$filename[0].$filename[1].DIRECTORY_SEPARATOR;
	}

  /**
   * _delete_file
   *
   * @param SplFileInfo $file
   * @param mixed $retain_parent_directory
   * @param mixed $ignore_errors
   * @param mixed $only_expired
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  protected function _delete_file(SplFileInfo $file, $retain_parent_directory = FALSE, $ignore_errors = FALSE, $only_expired = FALSE)
	{
		// Allow graceful error handling
		try
		{
			// If is file
			if ($file->isFile())
			{
				try
				{
					// If only expired is not set
					if ($only_expired === FALSE)
					{
						// We want to delete the file
						$delete = TRUE;
					}
					// Otherwise...
					else
					{
						// Assess the file expiry to flag it for deletion
						$json = $file->openFile('r')->current();
						$data = json_decode($json);
						$delete = $data->expiry < time();
					}

					// If the delete flag is set delete file
					if ($delete === TRUE)
						return unlink($file->getRealPath());
					else
						return FALSE;
				}
				catch (ErrorException $error)
				{
					// Catch any delete file warnings
          $this->ensure($error->getCode() === E_WARNING, __METHOD__.' failed to delete file : :file', array(':file' => $file->getRealPath()));
          throw $error;
				}
			}
			// Else, is directory
			elseif ($file->isDir())
			{
				// Create new DirectoryIterator
				$files = new DirectoryIterator($file->getPathname());

				// Iterate over each entry
				while ($files->valid())
				{
					// Extract the entry name
					$name = $files->getFilename();

					// If the name is not a dot
					if ($name != '.' AND $name != '..')
					{
						// Create new file resource
						$fp = new SplFileInfo($files->getRealPath());
						// Delete the file
						$this->_delete_file($fp);
					}

					// Move the file pointer on
					$files->next();
				}

				// If set to retain parent directory, return now
				if ($retain_parent_directory)
				{
					return TRUE;
				}

				try
				{
					// Remove the files iterator
					// (fixes Windows PHP which has permission issues with open iterators)
					unset($files);

					// Try to remove the parent directory
					return rmdir($file->getRealPath());
				}
				catch (ErrorException $error)
				{
					// Catch any delete directory warnings
          $this->ensure($error->getCode() === E_WARNING, __METHOD__.' failed to delete directory : :directory', array(':directory' => $file->getRealPath()));
          throw $error;
				}
			}
			else
			{
				// We get here if a file has already been deleted
				return FALSE;
			}
		}
		// Catch all exceptions
		catch (Exception $error)
		{
			// If ignore_errors is on
			if ($ignore_errors === TRUE)
			{
				// Return
				return FALSE;
			}
			// Throw exception
			throw $error;
		}
	}

  /**
   * get_dir
   *
   * @param mixed $name
   * @param mixed $path
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function get_dir($name, $path){
    $path               = PROJECT_ROOT . $path;
    try
		{
			$directory        = $this->set($name, new SplFileInfo($path));
      if (!$directory->getRealPath()){
        $directory        = $this->set($name, $this->_make_directory($path, 0700, TRUE));
      }
		}
		// PHP < 5.3 exception handle
		catch (ErrorException $error)
		{
      $directory        = $this->set($name, $this->_make_directory($path, 0700, TRUE));
		}
		// PHP >= 5.3 exception handle
		catch (UnexpectedValueException $error)
		{
      $directory        = $this->set($name, $this->_make_directory($path, 0700, TRUE));
		}
		// If the defined directory is a file, get outta here
    $this->ensure($directory->isFile(), 'Unable to create cache directory as a file already exists : :resource', array(':resource' => $directory->getRealPath()));

		// Check the read status of the directory
    $this->ensure(!$directory->isReadable(), 'Unable to read from the cache directory :resource', array(':resource' => $directory->getRealPath()));

		// Check the write status of the directory
    $this->ensure(!$directory->isWritable(), 'Unable to write to the cache directory :resource', array(':resource' => $directory->getRealPath()));
  }

  /**
   * init
   *
   * @uses OPT_CACHE_DEBUG OPT_CACHE_OPTIONS OPT_MODEL_HELPER OPT_CACHE_DIR
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function init(){
    $helper             = $this->register_model('Helper');
    $options            = $this->set(self::OPT_CACHE_OPTIONS, $helper->get(self::OPT_CACHE_OPTIONS));
    $this->set(self::OPT_CACHE_DEBUG, $options['debug']);
    $this->set(self::OPT_CACHE_DEFAULT_TIMEOUT, $options['ttl']);
    $this->set(self::OPT_CACHE_ID, $options['id']);
    $this->get_dir(self::OPT_CACHE_DIR, $options['dir']);
  }

  /**
   * id
   *
   * @param mixed $id
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function id($id = null)
  {
    if ($id !== null){
      $this->set(self::OPT_CACHE_ID, $id);
    }
    $return = $this->get(self::OPT_CACHE_ID);
    return $return;
  }

  /**
   * get_from_cache
   *
   * @param mixed $id
   * @param mixed $default
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function get_from_cache($id, $default = NULL){
    $filename   = $this->filename($this->_sanitize_id($id));
    $directory  = $this->_resolve_directory($filename);
    $return     = $default;
    // Wrap operations in try/catch to handle notices
    try {
      // Open file
      $file = new SplFileInfo($directory . $filename);

      // If file does not exist
      if (!$file->isFile()) {
        // Return default value
        $this->set(self::OPT_CACHE_REJECTED, $this->get(self::OPT_CACHE_REJECTED)+1);
        $this->log( 'reject', $id, 'value not cached yet');
      }
      else {

        $data = fopen( $directory . $filename, 'r' );
        $ttl = stream_get_line( $data, 256, "\r\n" );
        if ( $ttl < time(  ) && $ttl !== '0' ){
          $this->_delete_file( $file, null, true );
          $this->set(self::OPT_CACHE_REJECTED, $this->get(self::OPT_CACHE_REJECTED)+1);
          $this->log( 'reject', $id, 'expires');
        }
        else{
          ob_start();
          fpassthru( $data);
          $cache = ob_get_clean();
          fclose( $data);
          $cache = unserialize( $cache );
          if ($cache === $default){
            $this->_delete_file( $file, null, true );
            $this->set(self::OPT_CACHE_REJECTED, $this->get(self::OPT_CACHE_REJECTED)+1);
            $this->log( 'reject', $id, 'the default value');
          }
          else {
            $this->set(self::OPT_CACHE_GETTED, $this->get(self::OPT_CACHE_GETTED)+1);
            $return = $cache;
            $this->log( 'get', $id, $directory . $filename);
          }
        }      
      }
      return $return;

    }
    catch (ErrorException $error) {
      // Handle ErrorException caused by failed unserialization
      $this->ensure($error->getCode() === E_NOTICE, __METHOD__ . ' failed to unserialize cached object with message : ' . $error->getMessage());

      // Otherwise throw the exception
      throw $error;
    }
  }

  public function set_to_cache($id, $data, $lifetime = NULL){
    $filename  = $this->filename($this->_sanitize_id($id));
    $directory = $this->_resolve_directory($filename);

    // If lifetime is NULL
    if ($lifetime === NULL) {
      // Set to the default expiry
      $lifetime = $this->get(self::OPT_CACHE_DEFAULT_TIMEOUT);
    }

    // If the directory path is not a directory
    if (!is_dir($directory)) {
      // Create the directory
      $this->ensure(!mkdir($directory, 0700, TRUE), __METHOD__ . ' unable to create directory : :directory', array(':directory' => $directory));

      // chmod to solve potential umask issues
      chmod($directory, 0700);
    }

    // Open file to inspect
    $file    = fopen( $directory . $filename, 'w');

    try {
      $expirationTS = $lifetime ? time() + $lifetime : 0;
      $dindex       = serialize( $data);


      fwrite( $file, $expirationTS . "\r\n", 256);
      fwrite( $file, $dindex);
      $this->set(self::OPT_CACHE_SETTED, $this->get(self::OPT_CACHE_SETTED)+1);
      $this->log( 'set', $id, $expirationTS);
      return (bool)fclose( $file);
    }
    catch (ErrorException $error) {
      // If serialize through an error exception
      $this->ensure($error->getCode() === E_NOTICE, __METHOD__ . ' failed to serialize data for caching with message : ' . $error->getMessage());

      // Else rethrow the error exception
      throw $error;
    }
  }

  /**
   * add_to_cache
   *
   * @param mixed $id
   * @param mixed $data
   * @param mixed $lifetime
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function add_to_cache($id, $data, $lifetime = NULL)
  {
    $filename  = $this->filename($this->_sanitize_id($id));
    $directory = $this->_resolve_directory($filename);

    // If lifetime is NULL
    if ($lifetime === NULL) {
      // Set to the default expiry
      $lifetime = $this->get(self::OPT_CACHE_DEFAULT_TIMEOUT);
    }

    // Open directory
    $dir = new SplFileInfo($directory);

    // If the directory path is not a directory
    if (!$dir->isDir()) {
      // Create the directory
      $this->ensure(!mkdir($directory, 0700, TRUE), __METHOD__ . ' unable to create directory : :directory', array(':directory' => $directory));

      // chmod to solve potential umask issues
      chmod($directory, 0700);
    }

    // Open file to inspect
    $resouce = new SplFileInfo($directory . $filename);
    if (!$resouce->isFile())
      return $this->set_to_cache($id, $data, $lifetime);
    else return FALSE;
  }

  /**
   * increment
   *
   * @param mixed $id
   * @param int $step
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function increment($id, $step = 1)
  {
    $index    = $this->get_from_cache($id, 0);
    if ($this->set_to_cache($id, $index + $step, 0)){
      $this->log( 'increment', $id, $index + $step);
      return $index + $step;
    }
    else return FALSE;
  }

  /**
   * decrement
   *
   * @param mixed $id
   * @param int $step
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function decrement($id, $step = 1)
  {
    $index = $this->get_from_cache($id);
    if (null === $index) $index = 0;
    $index -= $step;
    if ($this->set_to_cache($id, $index, 0)){
      $this->log( 'decrement', $id, $index);
      return $index;
    }
    else return FALSE;
  }

  /**
   * delete_from_cache
   *
   * @param mixed $id
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function delete_from_cache($id){
    $filename = $this->filename($this->_sanitize_id($id));
		$directory = $this->_resolve_directory($filename);

		return $this->_delete_file(new SplFileInfo($directory.$filename), NULL, TRUE);
  }

  /**
   * delete_all
   *
   * @uses OPT_CACHE_DIR
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function delete_all(){
    return $this->_delete_file($this->get(self::OPT_CACHE_DIR), TRUE);
  }

  /**
   * log
   *
   * @param mixed $op
   * @param mixed $key
   * @param mixed $group
   * @uses OPT_CACHE_DEBUG OPT_CACHE_LOG
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return null
   */
  public function log( $op = null, $key = null, $group = null){
    static $timestart = null;
    if (!$timestart) $timestart = microtime(true);
    if ( $this->get(self::OPT_CACHE_DEBUG) ){
      if ( $op === null ) {
        foreach($this->get(self::OPT_CACHE_LOG) as $key_log => $value){
          $value['args'][':id'] = $key_log+1;
          $msg  = $value['message'];
          foreach($value['args'] as $id => $value_log)
            $msg = str_ireplace($id, $value_log, $msg);
          trigger_error($msg, E_USER_NOTICE);
        }
      }
      else{
        $timelength     = microtime(true) - $timestart;
        $log            = $this->get(self::OPT_CACHE_LOG);
        
        switch ( $op){
          case 'set':
            $message = "cache :op#:id\n#key=:key\n#lifetime=:tag\n#time=$timelength\n";
            break;
          case 'get':        
            $message = "cache :op#:id\n#key=:key\n#time=$timelength\n";
            break;
          case 'reject':
            $message = "cache :op#:id\n#key=:key\n#message=:tag\n#time=$timelength\n";
            break;
          case 'mimetime':
            $message = "cache :op#:id\n#tag=:key\n#value=:tag\n#time=$timelength\n";
            break;
          case 'increment':
            $message = "cache :op#:id\n#key=:key\n#lifetime=:tag\n#time=$timelength\n";
            break;
          case 'decrement':
            $message = "cache :op#:id\n#key=:key\n#lifetime=:tag\n#time=$timelength\n";
            break;
          default:;
        }
        $log[] = array(
          "message" => $message,
          "args"    => array( 
                    ':op' => strtoupper($op),
                    ':key' => var_export( $key, true),
                    ':tag' => var_export( $group,true ),
                    ));
        $this->set(self::OPT_CACHE_LOG, $log);
      }
    }
    return null;
  }

  /**
   * getGetted
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return int
   */
  public function getGetted()
  {
    return $this->get(self::OPT_CACHE_GETTED);
  }

  /**
   * getSetted
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return int
   */
  public function getSetted()
  {
    return $this->get(self::OPT_CACHE_SETTED);
  }

  /**
   * getRejected
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return int
   */
  public function getRejected()
  {
    return $this->get(self::OPT_CACHE_REJECTED);
  }

  /**
   * getMTimed
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return int
   */
  public function getMTimed(){
    return $this->get(self::OPT_CACHE_MTIMED);
  }

  /**
   * save
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function save(){

  }

}
