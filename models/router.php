<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Event;
use Accido\Models\Request;
use Accido\Models\Response;
use Accido\Controller;
use Accido\View;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Router
 *
 * @package Router
 * @subpackage Model
 * 
 * 
 * @see Model
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 andrew scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2013 <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Router extends Model {
  /**
   * ATTR_HELPER_OPTION
   *
   * @const string
   */
  const ATTR_HELPER_OPTION              = 'router';

  /**
   * OPT_CURRENT_CONTROLLER_NAME
   *
   * @const string
   */
  const OPT_CURRENT_CONTROLLER_NAME     = 'current_controller_name';

  /**
   * OPT_CURRENT_CONTROLLER_ID
   *
   * @const string
   */
  const OPT_CURRENT_CONTROLLER_ID       = 'current_controller_id';

  /**
   * OPT_MODEL_ROUTER
   *
   * @const string
   */
  const OPT_MODEL_ROUTER                = 'model_router';

  /**
   * OPT_RUNNED
   * 
   * @const string
   */
  const OPT_RUNNED                      = 'runned';

  /**
   * OPT_CURRENT_URI
   *
   * @const string
   */
  const OPT_CURRENT_URI                 = 'current_uri';
  
  /**
   * OPT_MODEL_REQUEST
   *
   * @const string
   */
  const OPT_MODEL_REQUEST               = 'model_request';

  /**
   * OPT_ROUTER_CONFIG
   *
   * @const string
   */
  const OPT_ROUTER_CONFIG               = 'config';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                       = array(
    self::OPT_EVENTS                    => array(
      
    ),
    self::OPT_CURRENT_CONTROLLER_NAME   => '',
    self::OPT_MODEL_ROUTER              => null,
    self::OPT_RUNNED                    => false,
    self::OPT_CURRENT_URI               => null,
    self::OPT_CURRENT_CONTROLLER_ID     => 0,
    self::OPT_MODEL_REQUEST             => null,
    self::OPT_ROUTER_CONFIG             => null,
  );

  /**
   * sanitize_uri
   *
   * @param mixed $str
   * @uses OPT_MODEL_UTF8
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return array
   */
  private function sanitize_uri($str){
    $uri        = $str;
    $request    = null;
    $utf8       = $this->register_model('Utf8');
    if ($uri instanceof Request){
      $this->set(self::OPT_MODEL_REQUEST, $uri);
      $request  = $uri;
      $uri      = $uri->get(Request::OPT_REQUEST_URI);
    }
    else{
      $this->set(self::OPT_MODEL_REQUEST, $this->reuse(function(){
        return new Request;
      }));
      $request  = $this->get(self::OPT_MODEL_REQUEST);
    }
    if (is_array($uri)){
      $uri      = implode('/', $uri);
    }
    $uri        = $utf8->urldecode($uri);
    $uri        = preg_replace('/\?.*$/xsi', '', $uri);
    $uri        = preg_replace('/[\\/\\\\_\\-]++/xsi', '/', $uri);
    $uri        = trim($uri, '/');
    $uri        = (array)explode('/', $uri);
    if(empty($uri) || empty($uri[0])){
      $uri      = ['/'];
    }
    $this->ensure(!is_array($uri) || empty($uri), 'Uri not found in :e', array(':e' => $str));
    $request->set(Request::OPT_REQUEST_URI, $uri);
    return $uri;
  }

  /**
   * sanitize_controller
   *
   * @param mixed $ctrl
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  private function sanitize_controller($ctrl){
    $ctrl         = preg_replace('/(?:[^\pL\pN]|_|\\|\/)++/ui', ' ', trim($ctrl, '\\/_ '));
    $ctrl         = str_ireplace(' ', '\\', ucwords($ctrl));
    return $ctrl;
  }

  /**
   * resolve_controller
   *
   * @param string|array|Models_Request $uri
   * @param &array $params
   * @uses OPT_CURRENT_CONTROLLER_ID, OPT_CURRENT_CONTROLLER_NAME, OPT_CURRENT_URI, OPT_RUNNED
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string|false
   */
  private function resolve_controller($uri, &$params){
    $uri      = $this->sanitize_uri($uri);
    if (false === $this->get(self::OPT_RUNNED)){
      $this->set(self::OPT_CURRENT_URI, $uri);
    }
    $cn       = '';
    $gl       = $this->get(self::OPT_CURRENT_URI);
    $id       = $this->get(self::OPT_CURRENT_CONTROLLER_ID);
    for($i=0; $i <= $id; $i++){
      if(!isset($gl[$i])) return 'Null';
      if (0 !== strlen($gl[$i])){
        $cn   .= (strlen($cn) ? '\\' : ''). ucfirst(strtolower(preg_replace('/[^\pL\pN].*$/ui', '', $gl[$i])));
      }
    }
    $id++; 
    if (false === $this->get(self::OPT_RUNNED)){
      $cn     .= '\\Main';
    }
    else $cn  .= '\\' . ucfirst(strtolower($uri[0]));
    $cn       = $this->sanitize_controller($cn);
    if(!class_exists($cn, false) && $this['config.live']){
        $this->create_controller($cn);
    }
    $l        = count($uri);
    $param_id = $id;
    for($i=1; $i < $l; $i++){
      if (isset($gl[$param_id])){
        $name           = preg_replace('/[^\pL\pN].*$/ui', '', $uri[$i]);
        $value          = preg_replace('/^[\pL\pN]*[^\pL\pN]++/ui', '', $gl[$param_id++]);
        $params[$name]  = $value;
      }
      else break;
    }
    $this->set(self::OPT_CURRENT_CONTROLLER_ID, $id);
    $this->set(self::OPT_CURRENT_CONTROLLER_NAME, $cn);
    return $cn;
  }

  /**
   * create_controller
   *
   * @param mixed $classname
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  private function create_controller($classname){
    $namespace  = explode('\\', $classname);
    $classname  = array_pop($namespace);
    $namespace  = implode('\\', $namespace);
    $namespace  = empty($namespace) ? '' : '\\' . $namespace;
    $file = '/controllers/' . trim(str_ireplace(array('_','\\'), '/', strtolower($namespace . '\\' . $classname)), '/') . '.php';
    if (file_exists(PROJECT_ROOT . $file) || file_exists(CORE_ROOT . $file)) return;
    $file = PROJECT_ROOT . $file;
    $dir  = dirname($file);
    if (!is_dir($dir))
      mkdir($dir, 0700, true);
    $fr   = fopen($file, 'x');
    if ($fr){
      $content =
<<<EOC
<?php
namespace Accido\\Controllers{$namespace};
use Accido\Controller;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: $classname
 *
 * @package $classname
 * @subpackage Controller
 *
 * 
 * @see Controller
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version 0.1
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class $classname extends Controller{

  /**
   * template
   *
   * @var string
   */
  protected \$template = '';

  /**
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function action(){

  }

  /**
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function initialize(){

  }

  /**
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function finalize(){

  }

}
EOC;
      fwrite($fr, $content);
      fclose($fr);
    }
    else
      $this->ensure(true, "Cann't open file ':f' for controllers class :class"
        , array(':class' => $classname, ':f' => $file));
  }

  /**
   * init
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function init(){
    $this->add(self::OPT_MODEL_ROUTER, function(){
      return new Router;
    });
    $helper                 = $this->register_model('Helper');
    $option                 = $helper->get(self::ATTR_HELPER_OPTION);
    if(empty($option)){
      $option               = array(
        'live'              => false,
      );
      $helper->set(self::ATTR_HELPER_OPTION, $option);
      $helper->save();
    }
    $this->set(self::OPT_ROUTER_CONFIG, $option);
  }

  /**
   * execute
   *
   * @param mixed $uri
   * @param array $params
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function execute($uri, array $params = array()){
    $runned           = $this->get(self::OPT_RUNNED);
    if($this !== $runned && $runned instanceof self) return $runned->execute($uri, $params);
    $router           = $this->get(self::OPT_MODEL_ROUTER);
    $this->set(self::OPT_RUNNED, $router);
    $return           = $router->run($uri, $params);
    $this->set(self::OPT_RUNNED, $runned);
    return $return;
  }

  /**
   * run
   *
   * @param mixed $uri
   * @param array $params
   * @uses OPT_RUNNED
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function run($uri, array $params = array()){
    if ($this->get(self::OPT_RUNNED)) return null;
    $controller_name  = $this->resolve_controller($uri, $params);
    $this->set(self::OPT_RUNNED, $this);
    return Controller::execute($controller_name, $params, $this->get(self::OPT_MODEL_REQUEST));
  }

  /**
   * extend
   *
   * @param mixed $uri
   * @param View $view
   * @param array $params
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Option
   */
  public function extend($uri, View $view, array $params = array()){
    $runned                                     = $this->get(self::OPT_RUNNED);
    if (!$runned instanceof self) return null;
    if($this !== $runned) return $runned->extend($uri, $view, $params);
    
    $router                                     = $this->get(self::OPT_MODEL_ROUTER);
    $request                                    = null;
    $old_request                                = null;
    $old_uri                                    = null;
    $old_params                                 = null;
    
    if(!$uri instanceof Request){
      $old_request                              = $this->get(self::OPT_MODEL_REQUEST);
      $old_uri                                  = $old_request->get(Request::OPT_REQUEST_URI);
      $old_params                               = $old_request->get(Request::OPT_PARAMETER_VARS);
      $old_request->set(Request::OPT_REQUEST_URI, $uri);
      $request                                  = $old_request;
    }
    else{
      $request                                  = $uri;
    }
    
    $this->set(self::OPT_RUNNED, $router);
    $router->set(self::OPT_RUNNED, $router);
    $router[self::OPT_CURRENT_URI]              = $this[self::OPT_CURRENT_URI];
    $router[self::OPT_CURRENT_CONTROLLER_ID]    = $this[self::OPT_CURRENT_CONTROLLER_ID];
    $router[self::OPT_CURRENT_CONTROLLER_NAME]  = $this[self::OPT_CURRENT_CONTROLLER_NAME];

    $controller                                 = $router->resolve_controller($request, $params);

    $return                                     = Controller::extend($controller, $view, $params, $request);

    if(!empty($old_request)){
      $old_request->set(Request::OPT_REQUEST_URI, $old_uri);
      $old_request->set(Request::OPT_PARAMETER_VARS, $old_params);
    }

    return $return;
  }

}

