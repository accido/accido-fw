<?php 
namespace Accido\Models;
use Accido\Model;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Utils
 *
 * @package Utils
 * @subpackage System
 * 
 * 
 * @see Model
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 andrew scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2013 <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Utils extends Model{

  const ATTR_CALLBACK_IS_ARRAY                                  = 1;

  const ATTR_CALLBACK_IS_SCALAR                                 = 2;

  const ATTR_CALLBACK_IS_STRING                                 = 4;

  const ATTR_CALLBACK_IS_OBJECT                                 = 8;

  protected $vars                                               = array(
    self::OPT_EVENTS                                            => array(
      
    ),
  );

  protected function init(){

  }

  public function fill($length = null, $cell = null){
    if(empty($length)) return array();
    $result                                                     = array();
    for($index=0; $index < $length; ++$index){
      $temp                                                     = &$result[$index];
      $temp                                                     = empty($temp) ? array() : $temp;
      $counter                                                  = &$result[$length-$index-1];
      $counter                                                  = empty($counter) ? array() : $counter;
      for($jndex=0; $jndex < $index; ++$jndex){
        $temp[$jndex]                                           = $cell;
        $counter[$length-$index+$jndex]                         = $cell;
      }
      $temp[$index]                                             = $cell;
    }
    return $result;
  }

  public function sentences($text, $count = 1){
    if(preg_match("/^([^.!?]+?(?:(?>(?:\W\w{1,3}|\W\d+)\.(?1))|\.|!|\?)){{$count}}/xu", $text, $match)){
      return $match[0];
    }
    return $text;
  }

  public function is_callable($callback){
    if(empty($callback)) return false;
    $state                                                      = 0;
    if(is_string($callback)){
      $state                                                    = self::ATTR_CALLBACK_IS_STRING;
    }
    elseif(is_scalar($callback)){
      $state                                                    = self::ATTR_CALLBACK_IS_SCALAR;
    }
    elseif(is_array($callback)){
      $state                                                    = self::ATTR_CALLBACK_IS_ARRAY;
    }
    elseif(is_object($callback)){
      $state                                                    = self::ATTR_CALLBACK_IS_OBJECT;
    }
    if(empty($state)) return false;
    if(($state & self::ATTR_CALLBACK_IS_ARRAY) && 2 === count($callback) && array(0,1) === array_keys($callback)){
      $method_is_string                                         = is_string($callback[1]);
      if(is_string($callback[0]) && $method_is_string){
        return class_exists($callback[0], false) && method_exists($callback[0],$callback[1]);
      }
      return $method_is_string ? method_exists($callback[0], $callback[1]) : false;
    }
    if($state & self::ATTR_CALLBACK_IS_ARRAY) return false;
    if(($state & self::ATTR_CALLBACK_IS_STRING) && function_exists($callback)) return true;
    if(($state & self::ATTR_CALLBACK_IS_STRING) && class_exists($callback, false)) return method_exists($callback, '__invoke');
    if($state & self::ATTR_CALLBACK_IS_SCALAR) return false;
    if($state & self::ATTR_CALLBACK_IS_OBJECT) return method_exists($callback, '__invoke'); 
    return false;
  }

  /**
   * seek
   * 
   * @param array $array
   * @param int $offset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function seek(array &$array, $offset){
    reset($array);
    while(null !== ($key = key($array))){
      if ($key === $offset) return true;
      next($array);
    }
    return false;
  }
}
