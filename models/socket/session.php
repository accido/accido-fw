<?php
namespace Accido\Models\Socket;
use Accido\Model;
use Accido\Injector;
use Accido\Option;
use Accido\Stream;
use Accido\Models\Socket;
use Accido\Models\Response\Http\Header as HTTPHeader;
use Accido\Models\Synchronize;
use Accido\Models\Render\Socket\Relate as SR;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Session
 *
 * @package Socket
 * @subpackage Model
 * 
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Session extends Model {
  /**
   * @const string
   */
  const ATTR_CHUNKED_DELIMITER                            = "\n~~#~~\n";
  /**
   * @const string
   */
  const OPT_ID                                            = 'id';
  /**
   * @const string
   */
  const OPT_UTF8                                          = 'utf8';
  /**
   * @const string
   */
  const OPT_SOCKET                                        = 'socket';
  /**
   * @const string
   */
  const OPT_DATA                                          = 'data';

  protected $vars                                         = [
    self::OPT_ID                                          => null,
    self::OPT_UTF8                                        => null,
    self::OPT_SOCKET                                      => [],
    self::OPT_DATA                                        => [],
  ];

  public function capture(Injector $dic, $id){
    $this[self::OPT_ID]                                   = $id;
  }

  protected function init(){
    $this[self::OPT_UTF8]                                 = $this->register_model('Utf8');
  }

  /**
   * connect
   * 
   * @param Socket $socket
   * @param string $unique
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function connect(Socket $socket, $unique){
    $server                                               = HTTPHeader::ATTR_DEFAULT_SERVER;
    $timeout                                              = intval(Socket::ATTR_SOCKET_TIMEOUT);
    $header                                               = HTTPHeader::X_SOCKET . ': ' . HTTPHeader::TYPE_RELATE;
    $date                                                 = gmdate('D, d M Y H:i:s') . ' GMT';
    $version                                              = phpversion();
    $id                                                   = [SR::ATTR_RESPONSE_COOKIE, $this->id];
    $utf8                                                 = $this[self::OPT_UTF8];
    $id                                                   = $this->encode($utf8->json_encode($id));
    $data                                                 = <<<EOD
HTTP/1.1 200 Ok
Date: $date
Server: $server
$header
Keep-Alive: timeout=$timeout, max=99
Connection: Keep-Alive
Transfer-Encoding: chunked
Vary: Accept-Encoding
Content-Type: text/html
X-Powered-By: $version

$id
EOD;
    $socket->send($data);
    return $this->do_connect($socket, $unique);
  }

  /**
   * 
   * @param Socket $socket
   * @param string $unique
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  public function do_connect(Socket $socket, $unique){
    $socks                                                = &$this->ref(self::OPT_SOCKET);
    if(isset($socks[$socket->instance_hash])){
      return false;
    }
    $socks[$socket->instance_hash]                        = [
      'socket'                                            => $socket,
      'unique'                                            => $unique,
    ];
    return true;
  }

  /**
   * disconnect
   * 
   * @param Socket $socket
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function disconnect(Socket $socket){
    $socks                                                = &$this->ref(self::OPT_SOCKET);
    if(!isset($socks[$socket->instance_hash])){
      return false;
    }
    $unique                                               = $socks[$socket->instance_hash]['unique'];
    $sync                                                 = $this->register('Synchronize');
    $sync->derelate($this, $unique);
    $this->do_derelate($unique);
    unset($socks[$socket->instance_hash]);
    return true;
  }

  /**
   * 
   * @param mixed $unique
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function do_derelate($unique){
    $data                                                 = &$this->ref(self::OPT_DATA);
    foreach($data as $name => &$item){
      if(!isset($item[$unique])){
        continue;
      }
      list($model, $key, $stream)                         = $item[$unique];
      if($stream){
        $stream->destroy();
      }
      unset($item[$unique]);
    }
  }

  /**
   * pack
   * 
   * @param array $vars
   * @param array $sockets
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function pack(array $vars, array $sockets = null){
    $utf8                                                 = $this[self::OPT_UTF8];
    $data                                                 = $utf8->json_encode($vars);
    $sync                                                 = $this->register('Synchronize');
    $sync->push($this, $data);
    return $this->do_pack($data, $sockets);
  }

  /**
   * 
   * @param mixed $raw
   * @param array $sockets
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function do_pack($raw, array $sockets = null){
    if(empty($sockets)){
      $socks                                              = &$this->ref(self::OPT_SOCKET);
    }
    else{
      $socks                                              = $sockets;
    }
    if(empty($socks)){
      return false;
    }
    $raw                                                  = $this->encode($raw);
    foreach($socks as &$item){
      $socket                                             = $item['socket'];
      if(!$socket->alive()){
        $this->disconnect($socket);
        continue;
      }
      $socket->send($raw);
    }
    return true;
  }

  /**
   * 
   * @param string $raw
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function encode($raw){
    $raw                                                  .= self::ATTR_CHUNKED_DELIMITER;
    $length                                               = dechex(strlen($raw));
    $delim                                                = HTTPHeader::DELIMITER;
    $raw                                                  = $length . $delim . $raw . $delim;
    return $raw;
  }

  /**
   * unpack
   * 
   * @param string $str
   * @param Synchronize $sync
   * @param string $unique
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function unpack($str, Synchronize $sync, $unique){
    $sync->pull($this, $str, $unique);
    return $this->do_unpack($str, $unique);
  }

  /**
   * 
   * @param string $str
   * @param string $unique
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function do_unpack($str, $unique){
    $data                                                 = &$this->ref(self::OPT_DATA);
    $utf8                                                 = $this[self::OPT_UTF8];
    $request                                              = $utf8->json_decode($str);
    list($name, $update)                                  = $request;
    if(!isset($data[$name]) || !isset($data[$name][$unique])){
      return false;
    }
    list($model, $key, $stream)                           = $data[$name][$unique];
    if(!$model){
      return false;
    }
    $model->set($key, $update);
    return true;
  }

  /**
   * 
   * @param string $sess
   * @param string $unique
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function move($sess, $unique){
    $sync                                                 = $this->register('Synchronize');
    $sync->move($this, $sess, $unique);
    $this->do_move($sess, $unique);
  }

  /**
   * 
   * @param mixed $sess
   * @param mixed $unique
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function do_move($sess, $unique){
    $session                                              = $this->register_model('Socket/Session', $sess);

    $data                                                 = &$this->ref(self::OPT_DATA);
    $newdata                                              = &$session->ref(self::OPT_DATA);
    foreach($data as $name => &$item){
      if(isset($item[$unique])){
        list($model, $key, $stream)                       = $item[$unique];
        unset($item[$unique]);
        if($stream){
          $ancestor                                       = $stream->ancestor;
          $stream->destroy();
          $stream                                         = $ancestor
            ->then(function($data) use ($name, &$session){
              $session->pack([$name, $data]);
              return $data;
            })
            ->nocommit();
        }

        if(!isset($newdata[$name])){
          $newdata[$name]                                 = array();
        }
        $newdata[$name][$unique]                          = [$model, $key, $stream];
      }
    }

    $sockets                                              = &$this->ref(self::OPT_SOCKET);
    $newsocks                                             = &$session->ref(self::OPT_SOCKET);
    $update                                               = [SR::ATTR_RESPONSE_COOKIE, $sess];
    $utf8                                                 = $this[self::OPT_UTF8];
    $update                                               = $utf8->json_encode($update);
    foreach($sockets as $index => $sock){
      if($unique === $sock['unique']){
        unset($sockets[$index]);
        $session->do_connect($sock['socket'], $unique);
        $session->do_pack($update, [
          [
            'socket' => $sock['socket'],
          ],
        ]);
      }
    }
  }

}
