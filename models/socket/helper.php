<?php
namespace Accido\Models\Socket;
use Accido\Model;
use Accido\Socket;
use Accido\Injector;
use Accido\Controller;
use Accido\Models\Request;
use Accido\Models\Render\Socket\Relate;
use Accido\Models\Socket\Session;

defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Helper
 * @package Session
 * @subpackage Socket
 * 
 * @see Model
 * 
 * 
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Helper extends Model {

  const OPT_CONTROLLER                      = 'controller';

  protected $vars                           = [
    self::OPT_CONTROLLER                    => null,
  ];

  public function capture(Injector $dic, Controller $ctrl){
    $this[self::OPT_CONTROLLER]             = $ctrl;
  }

  /**
   * 
   * @uses OPT_CONTROLLER
   * @param string $newsess
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function session($newsess = null){
    $cookie                                 = &$this[self::OPT_CONTROLLER]->request->ref(Request::OPT_COOKIE_VARS);
    $return                                 = $cookie[Relate::ATTR_RESPONSE_COOKIE];
    if($newsess){
      $session                              = $this->register('Socket/Session', $return);
      $unique                               = $this->unique();
      $session->move($newsess, $unique);
      $cookie[Relate::ATTR_RESPONSE_COOKIE] = $newsess;
      $return                               = $newsess;
    }

    return $return;
  }

  /**
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function refresh(){
    return $this->session($this->key());
  }

  /**
   * 
   * @uses OPT_CONTROLLER
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @returnrstring
   */
  public function unique(){
    return $this[self::OPT_CONTROLLER]->request[Request::OPT_COOKIE_VARS][Relate::ATTR_UNIQUE_COOKIE];
  }

  /**
   * 
   * @uses
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function key(){
    return strtolower(urlencode(base64_encode(openssl_random_pseudo_bytes(32))));
  }
}
