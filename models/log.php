<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Event;
use Accido\View;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Log 
 * 
 * @uses Model
 * @package 
 * @version $id$
 * @copyright 2013 Accido
 * @author Andrew Scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
class Log extends Model implements \Accido\Log{

  /**
   * dispatcher
   *
   * @var mixed
   */
  static public $dispatcher                         = null;

  /**
   * OPT_DESTINITION_FILE
   *
   * @const string
   */
  const OPT_DESTINITION_FILE                        = 'destinition_file';

  const FUNC_ADD_LOG_MESSAGE                        = 'trigger';

  const OPT_LOG_DISPATCHER                          = 'dispatcher';

  const FUNC_DISPATCH_DEBUG                         = 'dispatchDebug';

  const FUNC_DISPATCH_ERROR                         = 'dispatchError';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                                   = array(
    self::OPT_EVENTS                                => array(
      'controllers_log_main'                        => Event::ATTR_NORMAL_EVENT_PRIORITY,
    ),
    self::OPT_DESTINITION_FILE                      => '/logs/core.log',
    self::FUNC_ADD_LOG_MESSAGE                      => null,
    self::OPT_LOG_DISPATCHER                        => null,
    self::FUNC_DISPATCH_DEBUG                       => null,
    self::FUNC_DISPATCH_ERROR                       => null,
  );

  /**
   * parse
   *
   * @param resource $log
   * @param int $offset
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return array
   */
  private function parse($log, $offset){
    $data       = fread($log, $offset);
    $temp       = (array)explode("\r\n", $data);
    $data       = array();
    $f          = true;
    $i          = -1;
    foreach($temp as $row){
      if (!preg_match('/\A\[[^\]]+\].{5}[^:]+:/xsi', $row)){
        if($f){
          $len = strlen($row) + strlen("\r\n");
          fseek($log, $len, SEEK_CUR);
        }
        else{
          $data[$i] .= '<p>' . $row . '</p>';
        }
      }
      else{
        $data[++$i] = $row;
        $f          = false;
      }
    }
    $result     = array();
    $count      = 0;
    foreach($data as $i => $row){
      $t = trim($row, "\r\n\s");
      if (preg_match('/\A\[([^\]]+)\].{5}([^:]+):\s*(.*).{1}in.{1}(.*).{1}on.{1}line.{1}(\d+)/xsi', $t, $matches)){
        $result[]     = array(
          'date'      => date('l dS \o\f F Y h:i:s A', strtotime($matches[1]))
          , 'type'    => $matches[2]
          , 'message' => $matches[3]
          , 'file'    => $matches[4]
          , 'line'    => $matches[5]);
        $count ++;
      }
      if (200 <= $count) break;
    }
    return $result;
  }

  // protected init() {{{ 
  /**
   * init
   * 
   * @access protected
   * @return void
   */
  protected function init(){
    $helper               = $this->register_model('Helper');
    $options              = $helper->get('log');
    if (empty($options)){
      $options            = array(
        'type'            => 'core',
        'display'         => 1,
        'level'           => E_ALL & ~E_STRICT & ~E_WARNING,
        'core'            => array(
          'enable'        => 1,
          'html'          => 0,
          'stream'        => '/logs/core.log',
        ),
      );
      $helper->set('log', $options);
      $helper->save();
    }
    ini_set('display_errors', $options['display']);
    error_reporting($options['level']);
    $logger               = $this;
    switch($options['type']){
      case 'monolog':
      $logger             = $this->register_model('Monolog');
      break;
      case 'phpconsole':
      $logger             = $this->register_model('PhpConsole');
      break;
      case 'core':
      ini_set('log_errors', $options['core']['enable']);
      ini_set('html_errors', $options['core']['html']);
      ini_set('error_log', PROJECT_ROOT . $options['core']['stream']);
      break;
      default:;
      break;
    }
    $this->set(self::OPT_LOG_DISPATCHER, $this);
    self::$dispatcher     = $logger[self::OPT_LOG_DISPATCHER];
    $this->set(self::OPT_LOG_DISPATCHER, self::$dispatcher);
    $this->set(self::FUNC_ADD_LOG_MESSAGE, $this->reuse(function($injector, $log_message, $log_tags) use ($logger){
          $logger->log($log_message, $log_tags);
        }));
    $this->set(self::FUNC_DISPATCH_DEBUG, $this->reuse(function($injector, $log_message, $log_tags) use ($logger) {
          $logger->log($log_message, $log_tags);
        }));
    $this->set(self::FUNC_DISPATCH_ERROR, $this->reuse(function($injector, $log_message, $log_tags) use ($logger) {
          $logger->log($log_message, $log_tags);
        }));
    
  }
  // }}}

  /**
   * log
   *
   * @param mixed $log_message
   * @param mixed $log_tags
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function log($log_message, $log_tags){
    if(!is_array($log_tags)) $log_tags    = array($log_tags);
    $log_tags                             = implode(', ', $log_tags);
    $log_message                          = is_string($log_message) ? $log_message : var_export($log_message, true);
    trigger_error($log_tags . ': ' . $log_message, E_USER_NOTICE);
  }

  /**
   * add_msg
   *
   * @param mixed $msg
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return int|bool
   */
  public function add_msg( $msg ){
  }

  /**
   * insert_msg
   *
   * @param mixed $msg
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool|int
   */
  public function insert_msg( $msg ){
    throw new \Accido\Exceptions\Log('Function deprecated. Use trigger_error function.');
    //return error_log($msg, 3, PROJECT_ROOT . $this->get(self::OPT_DESTINITION_FILE));
  }

  /**
   * delete_msg
   *
   * @param string|id $msg message or id
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function delete_msg($msg){

  }

  // public read_msg(id) {{{ 
  /**
   * read_msg
   * 
   * @param int $id 
   * @access public
   * @return string|bool
   */
  public function read_msg( $id ){

  }
  // }}}

  /**
   * event_controllers_log_main
   *
   * @param View $view
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function event_controllers_log_main(View $view){
    $log            = fopen(PROJECT_ROOT . $this->get(self::OPT_DESTINITION_FILE), 'a+b');
    fseek($log, 0, SEEK_END);
    $rows           = array();
    while(count($rows) < 200 && ($pos = ftell($log))){
      $offset       = $pos < 4096 ? $pos : 4096;
      fseek($log, -$offset, SEEK_CUR);
      $data         = $this->parse($log, $offset);
      fseek($log, -$offset, SEEK_CUR);
      $current      = end($data);
      do{
        $rows[]     = $current;
      }while(($current = prev($data)));
    }
    $view->logs     = $rows;

    $helper         = $this->register_model('Helper');
    $view->base_url = $helper->get(self::OPT_BASE_URL);
    $view->charset  = $helper->get(self::OPT_CHARSET_ENCODING);
    fclose($log);
  }

}
