<?php 
namespace Accido\Models;
use Accido\Model;
use Accido\Event;
use Accido\View;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * DB 
 * 
 * @uses Model
 * @package 
 * @version $id$
 * @copyright 2013 Accido
 * @author Andrew Scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
class DB extends Model {

  /**
   * ATTR_MYSQL 
   * 
   * @const string
   */
  const ATTR_MYSQL                = 'mysql';
  /**
   * @const string
   */
  const ATTR_SQL_DELIMITER        = '%%query%%';
  /**
   * @const string
   */
  const ATTR_OPTIONS              = 'database';
  /**
   * @const string
   */
  const OPT_INSTALLED             = 'database.installed';
  /**
   * OPT_DB_HOST 
   * 
   * @const string
   */
  const OPT_DB_HOST               = 'db_host';
  /**
   * OPT_DB_USER 
   * 
   * @const string
   */
  const OPT_DB_USER               = 'db_user';
  /**
   * OPT_DB_PASS 
   * 
   * @const string
   */
  const OPT_DB_PASS               = 'db_pass';
  /**
   * OPT_DB_NAME 
   * 
   * @const string
   */
  const OPT_DB_NAME               = 'db_name';

  /**
   * OPT_DB_SQL_TYPE 
   * 
   * @const string
   */
  const OPT_DB_SQL_TYPE           = 'sql_type';

  /**
   * OPT_PREFIX 
   * 
   * @const string
   */
  const OPT_PREFIX                = 'database.prefix';

  /**
   * OPT_REPOSITORIES 
   * 
   * @const string
   */
  const OPT_REPOSITORIES          = 'database.repositories';

  /**
   * OPT_REPOSITORIES_LOADED 
   * 
   * @const string
   */
  const OPT_REPOSITORIES_LOADED   = 'loaded';
  
  /**
   * OPT_DB 
   * 
   * @const string
   */
  const OPT_DB                    = 'db';

  /**
   * OPT_DB_CHARSET
   *
   * @const string
   */
  const OPT_DB_CHARSET            = 'db_charset';

  /**
   * OPT_MODEL_HELPER
   *
   * @const string
   */
  const OPT_MODEL_HELPER          = 'model_helper';

  /**
   * vars 
   * 
   * @var array
   * @access protected
   */
  protected $vars                 =  array(
    self::OPT_EVENTS              => array(
      'controllers_shutdown'      => Event::ATTR_LOW_EVENT_PRIORITY,
    ),
    self::OPT_DB                  => NULL,
    self::OPT_MODEL_HELPER        => null,
    self::OPT_REPOSITORIES_LOADED => false,
  );

  // private create_repository(name) {{{ 
  /**
   * create_repository
   * 
   * @param string $name 
   * @access private
   * @return void
   */
  private function create_repository( $name ){
    $namespace  = explode('\\', $name);
    $name       = array_pop($namespace);
    array_shift($namespace);
    $namespace  = implode('\\', $namespace);
    $file       = str_ireplace( array('_', '\\'), '/', strtolower( $namespace . '\\' . $name ) ) . '.php';
    if (file_exists(PROJECT_ROOT . '/' . $file) || file_exists(CORE_ROOT . '/' . $file)) return;
    $file       = PROJECT_ROOT . '/' . $file;
    $dir        = dirname($file);
    if ( !is_dir($dir) )
      mkdir($dir, 0700, TRUE);
    $file       = fopen($file, 'wb');
    $this->ensure( !is_resource( $file ), "Cann't open repository file($file).");
    $date       = date( DATE_RSS );
    $code =
<<<EOF
<?php 
namespace Accido\\$namespace;
use Accido\Model;
use Accido\Event;
use Accido\View;
use Accido\Models\DB;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * $name 
 * 
 * @uses Model
 * @package Repository
 * @version 1.0
 * @copyright $date Accido
 * @author Andrew Scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
class $name extends Model {

  /**
   * OPT_TABLE
   *
   * @const string
   */
  const OPT_TABLE                     = 'table';

  /**
   * vars 
   * 
   * @var array
   * @access protected
   */
  protected \$vars                    = array(
    self::OPT_EVENTS                  => array(
      'controllers_install'           => Event::ATTR_NORMAL_EVENT_PRIORITY,
    ),
    self::OPT_SQL_INIT_CODE           => array(
      /**
       * use {:prefix} construction for prefix table 
       * use {:charset} construction for charachter table
       * use %%query%% for inner delimiter in multi query
       */
      DB::ATTR_MYSQL                  => '',
    ),
    /**
     * table name without db prefix
     */
    self::OPT_TABLE                   => '',
    /**
     * model db container
     */
  );

  // protected init() {{{ 
  /**
   * init
   * 
   * @access protected
   * @return void
   */
  protected function init(){
  }
  // }}}

  // public event_controllers_install(View view) {{{ 
  /**
   * event_controllers_install
   * 
   * @param View \$view 
   * @access public
   * @return void
   */
  public function event_controllers_install( View \$view ){
    \$queries = \$view->queries;
    \$queries[] = (array)\$this->get( self::OPT_SQL_INIT_CODE );
    \$view->queries = \$queries;
  }
  // }}}

}
EOF;
    fwrite( $file, $code );
    fclose( $file );
  }
  // }}}

  // protected init() {{{ 
  /**
   * init
   * 
   * @access protected
   * @return void
   */
  protected function init(){
    $helper                     = $this->register_model('Helper');
    $db                         = null;
    $opt                        = $helper->get(self::ATTR_OPTIONS);
    if(empty($opt) || !isset($opt[self::OPT_DB_HOST])){
      $opt                      = [
        self::OPT_DB_HOST       => '127.0.0.1',
        self::OPT_DB_USER       => 'root',
        self::OPT_DB_PASS       => '',
        self::OPT_DB_NAME       => '',
        self::OPT_DB_CHARSET    => 'UTF-8',
        self::OPT_DB_SQL_TYPE   => self::ATTR_MYSQL,
        'repositories'          => [],
        'prefix'                => '',
      ];
      $helper->set(self::ATTR_OPTIONS, $opt);
      $helper->save();
    }
    $host                       = $opt[self::OPT_DB_HOST];
    $user                       = $opt[self::OPT_DB_USER];
    $pass                       = $opt[self::OPT_DB_PASS];
    $dbname                     = $opt[self::OPT_DB_NAME];
    $dbchar                     = $opt[self::OPT_DB_CHARSET];
    switch($opt[self::OPT_DB_SQL_TYPE]){
      case self::ATTR_MYSQL:
      try{
        $db = new \goDB( array(
          'host'                => $host,
          'username'            => $user,
          'passwd'              => $pass,
          'dbname'              => $dbname,
          'charset'             => $dbchar,
        ));
      }
      catch(\Exception $error){
        $this->ensure(true, 'Change options for DB connections. :msg', [':msg' => $error->getMessage()]);
      }
      break;
      default:;
    }
    $this->ensure(is_null($db), 'Try Set DB Type in Helper::sql_type');
    $this->set( self::OPT_DB, $db );
  }
  // }}}

  /**
   * shutdown
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function shutdown(){
    if($db = $this->get(self::OPT_DB)){
      $db->close();
    }
  }

  /**
   * load_repositories
   *
   * @uses OPT_MODEL_HELPER
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  public function load_repositories(){
    if($this[self::OPT_REPOSITORIES_LOADED]) return;
    $this[self::OPT_REPOSITORIES_LOADED]  = true;
    $helper                               = $this->register('Helper');
    $repos                                = $helper->get( self::OPT_REPOSITORIES );
    $repos                                = explode( ',', $repos );
    foreach($repos as $name){
      if (strlen($name)){
        $name                             = ucwords(str_ireplace('\\', ' ', trim(strtolower($name))));
        $name                             = str_ireplace(' ', '\\', $name);
        $name                             = 'Accido\\Models\\Repositories\\' 
                                            . ucfirst(trim(strtolower( $helper->get( self::OPT_DB_SQL_TYPE)))) 
                                            . '\\' . $name;
        $name                             = preg_replace('|[\\\\]+|ui', '\\', $name);
        if (!class_exists( $name, false)){
          $this->create_repository($name);
        }
        $short_name                       = str_ireplace('Accido\\Models\\', '', $name); 
        $this->register_model($short_name);
      }
    }
  }

  /**
   * event_controllers_shutdown
   *
   * @param View $view
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function event_controllers_shutdown(View $view){
    $this->shutdown();
  }

}
