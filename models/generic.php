<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Injector;
use Accido\Render;
use Accido\Polymorph;
use Accido\Controller;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Generic
 *
 * @package Generic
 * @subpackage Model
 * 
 * @see \Accido\Render
 * @see \Accido\Polymorph
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Generic extends Model implements Render,Polymorph{
  /**
   * @const string
   */
  const OPT_PROC                                              = 'proc';
  /**
   * @const string
   */
  const OPT_CONTROLLER                                        = 'controller';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                                             = array(
    self::OPT_PROC                                            => '',
    self::OPT_CONTROLLER                                      => null,
  );

  /**
   * capture
   * 
   * @param Injector $dic
   * @param Controller $ctrl
   * @param string $type
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function capture(Injector $dic, Controller $ctrl, $proc){
    $this[self::OPY_PROC]                                     = $proc;
    $this[self::OPT_CONTROLLER]                               = $ctrl;
  }

  /**
   * generic
   *
   * @param Controller $ctrl
   * @param string $proc
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function generic(Controller $ctrl, $proc){
    $_this                                                    = $this;
    $ctrl->commit()->then(function(\Accido\Generic $data) use ($proc){
      return $data->generate($proc);
    });
    return $ctrl;
  }

  /**
   * __invoke
   * 
   * @param Controller $ctrl
   * @param string $proc
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function __invoke(Controller $ctrl, $proc){
    return $this->generic($ctrl, $proc);
  }

  /**
   * done
   * 
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function done(){
    return $this->generic($this[self::OPT_CONTROLLER], $this[self::OPT_PROC]);
  }

  /**
   * fail
   * 
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function fail(){
    return $this->generic($this[self::OPT_CONTROLLER], $this[self::OPT_PROC]);
  }
}
