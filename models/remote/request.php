<?php 
namespace Accido\Models\Remote;
use Accido\Model;
use Accido\Injector;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Request
 *
 * @package Remote
 * @subpackage Request
 * 
 * 
 * @see Model
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Request extends Model{

  const FUNC_EXECUTE                                              = 'execute';

  const OPT_RESPONSE                                              = 'response';

  protected $vars                                                 = array(
    self::OPT_EVENTS                                              => array(),
    self::FUNC_EXECUTE                                            => null,
    self::OPT_RESPONSE                                            => null,
  );

  /**
   * init
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  protected function init(){
    $dic                                                          = self::$dic;
    $this->register_namespace('Requests', CORE_ROOT . '/requests/library');
    $dic->add('request', $this->reuse(function ($injector, $url, $headers = array(), $options = array()) {
      return \Requests::get($url, $headers, $options);
    }));
    $this->add(self::FUNC_EXECUTE, $dic->get_closure('request'));
  }

  /**
   * capture
   * 
   * @param Injector $injector
   * @param string $url
   * @param array $headers
   * @param array $options
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function capture(Injector $injector, $url, $headers = array(), $options = array()){
    $this->ensure(empty($url), 'Request error: empty url');
    $this->add(self::OPT_RESPONSE, $injector->request($url, $headers, $options));
  }
}
