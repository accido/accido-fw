<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Injector;
use Accido\Models\Synchronize;
use Closure;

class Transaction extends Model {

  const OPT_NAME                  = 'name';
  const OPT_CALLBACK              = 'callback';
  const OPT_SYNCHRONIZE           = 'synchronize';

  protected $vars                 = [
    self::OPT_NAME                => null,
    self::OPT_CALLBACK            => null,
    self::OPT_SYNCHRONIZE         => null,
  ];

  public function capture(Injector $dic, $name){
    $this[self::OPT_NAME]         = $name;
    $this[self::OPT_SYNCHRONIZE]  = $this->register_model('Synchronize');
  }

  /**
   * 
   * @param Closure $func
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function bind(Closure $func){
    $this[self::OPT_CALLBACK]     = $this->wrapClosure($func);
  }

  /**
   *  
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function run(){
    $args                         = (array)func_get_args();
    $sync                         = $this[self::OPT_SYNCHRONIZE];
    $trans                        = &$sync->ref(Synchronize::OPT_TRANSACTION);
    $trans[]                      = [
      'name'                      => $this[self::OPT_NAME],
      'params'                    => $args,
    ];
    $this->do_run($args);
  }

  /**
   * 
   * @param array $param
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function do_run(array $param){
    if(!($call = $this[self::OPT_CALLBACK])){
      return;
    }
    call_user_func_array($call, $param);
  }
}
