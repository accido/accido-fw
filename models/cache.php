<?php 
namespace Accido\Models;
use Accido\Model;
use Accido\Event;
use Accido\View;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Cache
 *
 * @package Cache
 * @subpackage Model
 *
 * 
 * @see Model
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Cache extends Model{

  /**
   * OPT_CACHE_DIR
   *
   * @const string
   */
  const OPT_CACHE_DIR               = 'cache_dir';

  /**
   * OPT_CACHE_DEFAULT_TIMEOUT
   *
   * @const string
   */ 
  const OPT_CACHE_DEFAULT_TIMEOUT   = 'default_timeout';

  const OPT_CACHE_ENABLED           = 'cache_enabled';

  const OPT_CACHE_PERSIST           = 'cache_persist';

  const OPT_CACHE_DEBUG             = 'cache_debug';

  const OPT_CACHE_ID                = 'cache_id';

  const OPT_CACHE_DRIVER            = 'cache_driver';

  const OPT_CACHE_GETTED            = 'cache_getted';

  const OPT_CACHE_SETTED            = 'cache_setted';

  const OPT_CACHE_FLUSHED           = 'cache_flushed';

  const OPT_CACHE_REJECTED          = 'cache_rejected';

  const OPT_CACHE_DELETED           = 'cache_deleted';

  const OPT_CACHE_OPTIONS           = 'cache';

  const OPT_MODEL_DRIVER            = 'model_driver';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                   = array(
    self::OPT_EVENTS                => array(
      'controllers_shutdown'        => Event::ATTR_NORMAL_EVENT_PRIORITY,
    ),
    self::OPT_CACHE_DIR             => '/cache/',
    self::OPT_CACHE_DEFAULT_TIMEOUT => 0,
    self::OPT_CACHE_ENABLED         => true,
    self::OPT_CACHE_PERSIST         => true,
    self::OPT_CACHE_ID              => '',
    self::OPT_CACHE_DEBUG           => false,
    self::OPT_CACHE_DRIVER          => null,
    self::OPT_CACHE_GETTED          => 0,
    self::OPT_CACHE_SETTED          => 0,
    self::OPT_CACHE_FLUSHED         => array(),
    self::OPT_CACHE_REJECTED        => 0,
    self::OPT_CACHE_DELETED         => 0,
    self::OPT_CACHE_OPTIONS         => array(),
    self::OPT_MODEL_DRIVER          => null,
  );

  protected $cache = array();
  protected $dirty = array();
  protected $cleaned = array();
  protected $np_groups = array();
  protected $global_groups = array();
  protected $persist_grps = array(  );
  protected $struct = array( 'write' => array(), 'read' => array(), 'delete' => array(), 'get' => array(), 'set' => array() );

  /**
   * getKey
   *
   * @param mixed $sid
   * @param array $group
   * @uses OPT_CACHE_ID
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  protected function getKey($sid, $group = array())
  {
    if (!is_array($group)) $group = array($group);
    $suffix     = '';
    $loc        = true;
    foreach( $group as $tag){
      $loc      = ( $loc && !in_array($tag, $this->global_groups) );
    }
    if ( $loc )
      $suffix   .= '`' . $this->get(self::OPT_CACHE_ID);

    return $sid . $suffix;
  }

  /**
   * encode
   *
   * @param mixed $key
   * @param mixed $data
   * @param mixed $sids
   * @param mixed $ttl
   * @uses OPT_MODEL_DRIVER
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  protected function encode($key, &$data, $sids, $ttl)
  {
    if ( !is_array($sids)) $sids = array($sids);
    return $this->get(self::OPT_MODEL_DRIVER)->set_with_tags( $this->getKey($key, $sids), $data, $ttl, $sids);
  }

  /**
   * decode
   *
   * @param mixed $key
   * @param mixed $group
   * @uses OPT_MODEL_DRIVER
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  protected function decode($key, $group, $default)
  {
    return $this->get(self::OPT_MODEL_DRIVER)->get_from_cache($this->getKey( $key, $group), $default);
  }

  /**
   * destroy
   *
   * @param mixed $key
   * @param mixed $group
   * @uses OPT_MODEL_DRIVER
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  protected function destroy( $key, $group)
  {
    return $this->get(self::OPT_MODEL_DRIVER)->delete_from_cache($this->getKey( $key, $group));
  }

  /**
   * log
   *
   * @param mixed $op
   * @param mixed $id
   * @uses OPT_CACHE_DEBUG
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  protected function log( $op, $id ){
    if ( $this->get(self::OPT_CACHE_DEBUG) ){
      if (!isset($this->struct[$op][$id]))
        $this->struct[$op][$id] = 0;
      $this->struct[$op][$id]++;
    }
  }

  /**
   * sanitize_key
   *
   * @param mixed $group
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  protected function sanitize_key( $group ){
    if ( is_array( $group ) )
      array_walk( $group, create_function( '&$value', '$value=trim($value);' ) );
    else $group = trim( $group );
    return $group;
  }

  /**
   * init
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function init(){
    $helper                     = $this->register_model('Helper');
    $options                    = (array)$helper->get(self::OPT_CACHE_OPTIONS);
    $defaults                   = array( 
      'dir'                     => '/cache/',
      'persist'                 => true,
      'enabled'                 => true,
      'ttl'                     => null,
      'debug'                   => false,
      'id'                      => 0,
      'driver'                  => 'File\\Tag',
      'persistent_group'        => array(
        
      ),
    );
    if (empty($options)){ 
      $options = $defaults;
      $helper->set(self::OPT_CACHE_OPTIONS, $options);
      $helper->save();
    }
    $options['persistent_group']= array_unique(array_merge($defaults['persistent_group'], $options['persistent_group']));
    $options                    = array_merge($defaults, $options);
    $this->set(self::OPT_CACHE_OPTIONS, $options);
    $this->set(self::OPT_MODEL_DRIVER, $this->register_model('Cache\\' . $options['driver']));
    foreach ($options as $key => $value) {
      if (method_exists($this, $key)) {
        $this->$key($value);
      }
    }
    $this->reset();
  }

  /**
   * fast_set
   *
   * @param mixed $key
   * @param mixed $data
   * @uses OPT_CACHE_DEBUG
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  protected function fast_set($key, $data)
  {
    //$this->cache[$key] = is_object( $data ) ? clone( $data ) : $data;
    $this->cache[$key] = $data;
    if ( $this->get(self::OPT_CACHE_DEBUG) ) $this->log( 'set', (string)$key );
  }

  /**
   * fast_get
   *
   * @param mixed $key
   * @uses OPT_CACHE_DEBUG cache
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  protected function fast_get($key)
  {
    $result = false;
    if (array_key_exists( $key, $this->cache)){
      $result = $this->cache[$key];
      $result = is_object($result) ? clone($result) : $result;
      if ( $this->get(self::OPT_CACHE_DEBUG) ) $this->log( 'get', $key );
    }

    return $result;
  }

  /**
   * dir
   *
   * @param mixed $value
   * @uses OPT_CACHE_DIR
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function dir($value = null){
    if ($value !== null)
      $this->set(self::OPT_CACHE_DIR, $value);
    $return = $this->get(self::OPT_CACHE_DIR);
    return $return;
  }

  /**
   * @param bool|null $persist
   *
   * @return bool|null
   */
  public function persist($persist = null)
  {
    if ($persist !== null)
      $this->set(self::OPT_CACHE_PERSIST, $persist);
    $return = $this->get(self::OPT_CACHE_PERSIST);
    return $return;
  }

  /**
   * @param bool|null $enabled
   *
   * @return bool|null
   */
  public function enabled($enabled = null)
  {
    if ($enabled !== null)
      $this->set(self::OPT_CACHE_ENABLED, $enabled);
    $return = $this->get(self::OPT_CACHE_ENABLED);
    return $return;
  }

  /**
   * __set
   *
   * @param mixed $key
   * @param mixed $val
   * @uses OPT_CACHE_ENABLED,OPT_CACHE_PERSIST
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  public function __set($key, $val)
  {
    if (self::OPT_CACHE_ENABLED === $key) {
      if (!$val) {
        $this->close();
        $this->set(self::OPT_CACHE_PERSIST, false);
      }
    }
    return parent::__set($key, $val);
  }

  /**
   * reset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return true
   */
  public function reset()
  {
    return true;
  }

  /**
   * add_to_cache
   *
   * @param mixed $key
   * @param mixed $data
   * @param mixed $group
   * @param mixed $ttl
   * @uses OPT_CACHE_REJECTED
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function add_to_cache($key, $data, $group, $ttl = null)
  {
    $cached_value = $this->get_from_cache( $key, $group );
    if ( false === $cached_value ) {
      return $this->set_to_cache($key, $data, $group, $ttl);
    }
    $this->set(self::OPT_CACHE_REJECTED, $this->get(self::OPT_CACHE_REJECTED) + 1);
    return false;
  }

  /**
   * incr
   *
   * @param mixed $key
   * @param int $offset
   * @param string $group
   * @uses OPT_CACHE_REJECTED
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function incr( $key, $offset = 1, $group = 'default' ){
    $cached_value = $this->get_from_cache( $key, $group );
    if (is_numeric($cached_value)){
      $cached_value += $offset;
      return $this->set_to_cache($key, $cached_value, $group);
    }
    $this->set(self::OPT_CACHE_REJECTED, $this->get(self::OPT_CACHE_REJECTED) + 1);
    return false;
  }

  /**
   * decr
   *
   * @param mixed $key
   * @param int $offset
   * @param string $group
   * @uses OPT_CACHE_REJECTED
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function decr( $key, $offset = 1, $group = 'default' ){
    $cached_value = $this->get_from_cache( $key, $group );
    if (is_numeric($cached_value)){
      $cached_value -= $offset;
      return $this->set_to_cache($key, $cached_value, $group);
    }
    $this->set(self::OPT_CACHE_REJECTED, $this->get(self::OPT_CACHE_REJECTED) + 1);
    return false;
  }

  /**
   * close
   *
   * @uses OPT_CACHE_DEBUG,OPT_CACHE_PERSIST,OPT_CACHE_REJECTED,dirty,cache,cleaned,np_groups
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return true
   */
  public function close()
  {
    if ( $this->get(self::OPT_CACHE_PERSIST) && !empty($this->dirty)) {
      foreach ($this->dirty as $key => $params) {
        foreach($params['group'] as $group)
        {
          if (in_array($group, $this->np_groups)){
            $this->set(self::OPT_CACHE_REJECTED, $this->get(self::OPT_CACHE_REJECTED) + 1);
            if ($this->get(self::OPT_CACHE_DEBUG)) $this->log('reject', $key);
            break;
          }
        }
        $cached_value = &$this->cache[$key];
        if ($cached_value !== null) {
          if ($this->get(self::OPT_CACHE_DEBUG)) $this->log('write', $key);
          if ($this->encode($key, $cached_value, $params['group'], $params['ttl'])){
            // success
          }
        }
      }
    }

    if ( $this->get(self::OPT_CACHE_PERSIST) && !empty( $this->cleaned)){
      foreach( $this->cleaned as $key => $group){
        foreach($group as $gindex)
        {
          if (in_array($gindex, $this->np_groups)){
            $this->set(self::OPT_CACHE_REJECTED, $this->get(self::OPT_CACHE_REJECTED) + 1);
            if ($this->get(self::OPT_CACHE_DEBUG)) $this->log('reject', $key);
            break;
          }
        }
        if ($this->get(self::OPT_CACHE_DEBUG)) $this->log( 'delete', $key );
        $this->destroy($key, $group);
      }
    }

    // Reset array cache, dirty or cleaned the load core system and i'm don't did it

    $this->get(self::OPT_MODEL_DRIVER)->save();
    $this->get(self::OPT_MODEL_DRIVER)->log();
    if ( $this->get(self::OPT_CACHE_DEBUG)) $this->info();
  }

  /**
   * delete
   *
   * @param mixed $key
   * @param mixed $group
   * @uses OPT_CACHE_DELETED,cache,dirty,cleaned
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function delete_from_cache($key, $group)
  {
    unset($this->cache[$key]);
    unset($this->dirty[$key]);
    $this->cleaned[$key] = $this->sanitize_key((array)$group);
    $this->set(self::OPT_CACHE_DELETED, $this->get(self::OPT_CACHE_DELETED) + 1);
    return true;
  }

  /**
   * delete_tag
   *
   * @param mixed $group
   * @uses OPT_MODEL_CACHE
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function delete_tag($group){
    $return = true;
    $group  = $this->sanitize_key((array)$group);
    foreach($group as $gindex){
      $return = ($return && $this->get(self::OPT_MODEL_DRIVER)->delete_tag($gindex));
    }
    foreach($this->dirty as $key => $params){
      $sindex = array_intersect($group, $params['group']);
      if (!empty($sindex)){
        $this->delete_from_cache($key, $params['group']);
      }
    }
    return $return;
  }

  /**
   * flush
   *
   * @uses OPT_CACHE_DEBUG OPT_CACHE_FLUSHED cache dirty
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function flush()
  {

    foreach ($this->cache as $group => $keys) {
      if (in_array($group, $this->np_groups)) {
        foreach (array_keys($keys) as $key) {
          unset($this->dirty[$key][$group]);
          if ( isset( $this->dirty[$key]) && empty( $this->dirty[$key])) unset( $this->dirty[$key]);
        }
        unset($this->cache[$group]);
      }
    }
    if ( $this->get(self::OPT_CACHE_DEBUG) ){
      $trace    = debug_backtrace();
      $trace    = $trace[2]['class'] . '.' . $trace[2]['function'] . ',';
      $flushed  = $this->get(self::OPT_CACHE_FLUSHED);
      if (!isset($flushed[$trace])){
        $flushed[$trace] = 1;
      }
      else $this->flushed[$trace]++;
      $this->set(self::OPT_CACHE_FLUSHED, $flushed);
    }
  }

  /**
   * set_to_cache
   *
   * @param mixed $key
   * @param mixed $data
   * @param mixed $group
   * @param mixed $ttl 
   * @uses OPT_CACHE_SETTED OPT_CACHE_ENABLED OPT_CACHE_DEFAULT_TIMEOUT cleaned dirty
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  public function set_to_cache($key, $data, $group, $ttl = null)
  {
    $this->set(self::OPT_CACHE_SETTED, $this->get(self::OPT_CACHE_SETTED) + 1);
    if (!$this->get(self::OPT_CACHE_ENABLED)) {
      return false;
    }

    if ($ttl === null)
      $ttl = $this->get(self::OPT_CACHE_DEFAULT_TIMEOUT);
    
    if (!is_array($group)) $group = array($group);

    $group = $this->sanitize_key( $group );
    unset($this->cleaned[$key]);
    $this->dirty[$key] = array( 'ttl' => $ttl, 'group' => $group);
    $this->fast_set($key, $data);
    return true;
  }

  /**
   * get_from_cache
   *
   * @param mixed $key
   * @param mixed $default
   * @param mixed $group
   * @uses OPT_CACHE_GETTED OPT_CACHE_ENABLED OPT_CACHE_PERSIST np_groups
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function get_from_cache($key, $default = false, $group = array())
  {
    $this->set(self::OPT_CACHE_GETTED, $this->get(self::OPT_CACHE_GETTED) + 1);
    $result = false;

    if (!$this->get(self::OPT_CACHE_ENABLED)) {
      return $default;
    }

    if (!is_array($group)) $group = array($group);
    $group = $this->sanitize_key( $group );

    $result = $this->fast_get($key);

    if ( $result === false &&
         $this->get(self::OPT_CACHE_PERSIST) && 
         !isset( $this->cleaned[$key])) {
      
      $data = $this->decode($key, $group, $default);
      
      if ( $data !== false && $data !== null && $data !== $default) {
        if ( $this->get(self::OPT_CACHE_DEBUG) ) $this->log('read', $key);
        $this->fast_set($key, $data);
        $result = $data;
      }
     
    }

    return false === $result ? $default : $result;
  }

  /**
   * replace
   *
   * @param mixed $key
   * @param mixed $data
   * @param mixed $group
   * @param mixed $ttl
   * @uses OPT_CACHE_ENABLED OPT_CACHE_REJECTED cache
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  public function replace_from_cache($key, $data, $group, $ttl = null)
  {
    $group = $this->sanitize_key( $group );
    if ($this->get(self::OPT_CACHE_ENABLED) 
        && array_key_exists( $key, $this->cache)) {
      return $this->set_to_cache($key, $data, $group, $ttl);
    }
    $this->set(self::OPT_CACHE_REJECTED, $this->get(self::OPT_CACHE_REJECTED) + 1);

    return false;
  }

  /**
   * persistent_groups
   *
   * @param array $groups
   * @uses persist_grps
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function persistent_groups( array $groups = null ){
    if ( $groups === null ) return $this->persist_grps;
    $groups = $this->sanitize_key( $groups );
    $this->persist_grps = array_unique(array_merge( $this->persist_grps, $groups ));
  }

  /**
   * non_persistent_groups
   *
   * @param array $groups 
   * @uses np_groups persist_grps global_groups
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function non_persistent_groups(array $groups = array())
  {
    if (empty($groups)){
      $this->np_groups = $groups;
      return;
    }
    try{
      $groups = $this->sanitize_key( $groups );
      $pl = empty( $this->np_groups ) ? array(  ) : array_values( $this->np_groups );
      $groups = array_diff( $groups, $this->persist_grps );
      $this->np_groups = array_unique(array_merge( $pl, $groups ));

      if ( !empty( $this->np_groups ) )
        $this->np_groups = array_combine($this->np_groups, $this->np_groups);
      sort($this->np_groups);
      $glob_gr = $this->global_groups;
      $this->global_groups = array(  );
      $this->global_groups($glob_gr);
    }
    catch( Exception $error ){
      $this->ensure(true, (string)$error);
    }
  }

  /**
   * global_groups
   *
   * @param array $groups
   * @uses global_groups np_groups persist_grps OPT_MODEL_DRIVER
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  public function global_groups(array $groups = array())
  {
    if (empty($groups)){
      $this->global_groups = $groups;
      return;
    }
    try{
      $groups = $this->sanitize_key( $groups );
      $gl = empty( $this->global_groups ) ? array(  ) : array_values( $this->global_groups );
      $pl = empty( $this->np_groups ) ? array(  ) : array_values( $this->np_groups );
      $groups = array_diff( $groups, array_unique(array_merge( $pl, $this->persist_grps )) );
      $this->get(self::OPT_MODEL_DRIVER)->global_tags($groups);

      $this->global_groups = array_unique(array_merge( $gl, $groups ));

      if( !empty( $this->global_groups ) )
        $this->global_groups = array_combine($this->global_groups, $this->global_groups);
      sort( $this->global_groups);
    }
    catch( Exception $error ){
      $this->ensure(true, (string)$error);
    }
  }



  /**
   * @param bool|null $ttl
   *
   * @return bool|null
   */
  public function ttl($ttl = null)
  {
    if ($ttl !== null)
      $this->set(self::OPT_CACHE_DEFAULT_TIMEOUT, $ttl);
    $return = $this->get(self::OPT_CACHE_DEFAULT_TIMEOUT);
    return $return;
  }

  /**
   * @param bool|null $id
   *
   * @return bool|null
   */
  public function id($id = null)
  {
    if ($id !== null){
      $this->set(self::OPT_CACHE_ID, $id);
      $this->get(self::OPT_MODEL_DRIVER)->id($id);
    }
    $return = $this->get(self::OPT_CACHE_ID);
    return $return;
  }

  /**
   * @param bool|null $mod
   *
   * @return bool|null
   */
  public function debug($mod = null)
  {
    if ($mod !== null)
      $this->set(self::OPT_CACHE_DEBUG, $mod);
    $return = $this->get(self::OPT_CACHE_DEBUG);
    return $return;
  }

  /**
   * driver
   *
   * @param string $cache
   * @uses OPT_CACHE_DRIVER
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function driver($cache = null)
  {
    if ($cache === null)
      return $this->get(self::OPT_CACHE_DRIVER);
    return $this->set(self::OPT_CACHE_DRIVER, $cache);
  }

  /**
   * info
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function info()
  {
    $set      = $this->get(self::OPT_MODEL_DRIVER)->getSetted();
    $get      = $this->get(self::OPT_MODEL_DRIVER)->getGetted();
    $rejected = $this->get(self::OPT_MODEL_DRIVER)->getRejected();
    $timed    = $this->get(self::OPT_MODEL_DRIVER)->getMTimed();
    $flush    = '';
    foreach ($this->get(self::OPT_CACHE_FLUSHED) as $trace => $num)
      $flush .= "$num/$trace, ";
    $flush = strlen($flush) ? $flush : 0;
    $npg = implode("', '", $this->np_groups);
    $pg  = implode("', '", $this->global_groups);
    $other = array_diff(array_keys($this->cache), array_unique(array_merge($this->np_groups, $this->global_groups)));
    sort( $other);
    $og  = implode("', '", $other);
    sort( $this->persist_grps );
    $rg  = implode( "', '", $this->persist_grps );
    $read = '';
    foreach( $this->struct['read'] as $gr => $count ){
      $read .= "== " . sprintf( "%-36s = %d\n", $gr, $count );
    }
    $write = '';
    foreach( $this->struct['write'] as $gr => $count ){
      $write .= "== " . sprintf( "%-36s = %d\n", $gr, $count );
    }
    $delete = '';
    foreach( $this->struct['delete'] as $gr => $count ){
      $delete .= "== " . sprintf( "%-36s = %d\n", $gr, $count );
    }
    $lset = '';
    foreach( $this->struct['set'] as $gr => $count ){
      $lset .= "== " . sprintf( "%-36s = %d\n", $gr, $count );
    }
    $lget = '';
    foreach( $this->struct['get'] as $gr => $count ){
      $lget .= "== " . sprintf( "%-36s = %d\n", $gr, $count );
    }
    
    $gd   = $this->get(self::OPT_CACHE_GETTED);
    $sd   = $this->get(self::OPT_CACHE_SETTED);
    $rd   = $this->get(self::OPT_CACHE_REJECTED);
    $dd   = $this->get(self::OPT_CACHE_DELETED);
    trigger_error('Cache Lazy get: ' . $gd, E_USER_NOTICE);
    trigger_error('Cache Lazy set: ' . $sd, E_USER_NOTICE);
    trigger_error('Cache Lazy reject: ' . $rd, E_USER_NOTICE);
    trigger_error('Cache Lazy flushed: ' . $flush, E_USER_NOTICE);
    trigger_error('Cache real deleted: ' . $dd, E_USER_NOTICE);
    trigger_error('Cache real set: ' . $set, E_USER_NOTICE);
    trigger_error('Cache real get: ' . $get, E_USER_NOTICE);
    trigger_error('Cache real reject: ' . $rejected, E_USER_NOTICE);
    trigger_error('Cache real MTimed: ' . $timed, E_USER_NOTICE);
    trigger_error('Cache non persistent groups: ' . $npg, E_USER_NOTICE);
    trigger_error('Cache global groups: ' . $pg, E_USER_NOTICE);
    trigger_error('Cache persistent groups: ' . $rg, E_USER_NOTICE);
    trigger_error('Cache other groups: ' . $og, E_USER_NOTICE);
    trigger_error("Cache read data:\r\n" . $read, E_USER_NOTICE);
    trigger_error("Cache write data:\r\n" . $write, E_USER_NOTICE);
    trigger_error("Cache delete data:\r\n" . $delete, E_USER_NOTICE);
    trigger_error("Cache set data:\r\n" . $lset, E_USER_NOTICE);
    trigger_error("Cache get data \r\n" . $lget, E_USER_NOTICE);
  }

  /**
   * event_controllers_shutdown
   *
   * @param View $view
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function event_controllers_shutdown(View $view){
    $this->close();
  }

}
