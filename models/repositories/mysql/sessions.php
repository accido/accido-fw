<?php
namespace Accido\Models\Repositories\Mysql;
use Accido\Model;
use Accido\Models\DB;
use Accido\Event;
use Accido\View;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Repositories_Mysql_Sessions 
 * 
 * @uses Model
 * @package Repository
 * @version 1.0
 * @copyright Mon, 16 Sep 2013 14:11:21 +0400 Accido
 * @author Andrew Scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
class Sessions extends Model {

  const OPT_TABLE                     = 'table';

  const OPT_DB                        = 'db';

  const OPT_MODEL_HELPER              = 'model_helper';

  const OPT_SESSIONS_LIFE             = 'sessions_life';

  const OPT_SESSIONS_NAME             = 'sessions_name';

  const OPT_ACCESS_ACTION             = 'access_action';

  const OPT_FINISHED                  = 'finished';

  /**
   * vars 
   * 
   * @var array
   * @access protected
   */
  protected $vars                           = array(
    self::OPT_EVENTS                        => array(
      'controllers_install'                 => Event::ATTR_NORMAL_EVENT_PRIORITY,
      'controllers_permissions'             => Event::ATTR_NORMAL_EVENT_PRIORITY,
      'controllers_session'                 => Event::ATTR_NORMAL_EVENT_PRIORITY,
      'controllers_shutdown'                => Event::ATTR_NORMAL_EVENT_PRIORITY,
    ),
    self::OPT_SQL_INIT_CODE                 => array(
      /**
       * use {:prefix} construction for prefix table 
       */
      DB::ATTR_MYSQL                 => '
      CREATE TABLE IF NOT EXISTS {:prefix}sessions  
        (
          sesskey VARCHAR(80) NOT NULL,
          expiry  INT(11) NOT NULL,
          value LONGTEXT,
          PRIMARY KEY(sesskey)
        )
        CHARACTER SET {:charset}
        COLLATE {:charset}_general_ci
        ENGINE=InnoDB
      ',
    ),

    self::OPT_TABLE                         => 'sessions',
    self::OPT_DB                            => null,
    self::OPT_SESSIONS_LIFE                 => 86000,
    self::OPT_SESSIONS_NAME                 => 'koobi',
    self::OPT_ACCESS_ACTION                 => 'alles',
    self::OPT_MODEL_HELPER                  => null,
    self::OPT_FINISHED                      => false,
  );

  // protected init() {{{ 
  /**
   * init
   * 
   * @access protected
   * @return void
   */
  protected function init(){
    session_set_save_handler(
	    array($this, "sess_open"),
	    array($this, "sess_close"),
	    array($this, "sess_read"),
	    array($this, "sess_write"),
	    array($this, "sess_destroy"),
	    array($this, "sess_gc"));
  }
  // }}}

  /**
   * session_shutdown
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function session_shutdown(){
    session_write_close();
  }

  // public event_controllers_install(View view) {{{ 
  /**
   * event_controllers_install
   * 
   * @param View $view 
   * @access public
   * @return void
   */
  public function event_controllers_install( View $view ){
    $queries = $view->queries;
    $queries[] = (array)$this->get( self::OPT_SQL_INIT_CODE );
    $view->queries = $queries;
  }
  // }}}

  /**
   * sess_open
   *
   * @param mixed $save_path
   * @param mixed $session_name
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return true
   */
  public function sess_open($save_path, $session_name) {
    return true;
  }

  /**
   * sess_close
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return true
   */
  public function sess_close() {
    return true;
  }

  /**
   * sess_read
   *
   * @param mixed $key
   * @uses OPT_DB, DB::OPT_PREFIX
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string|false
   */
  public function sess_read($key)
  {
    $db     = $this->register_model('DB')->get(DB::OPT_DB);
    $helper = $this->register_model('Helper');
    $prefix = $helper->get(DB::OPT_PREFIX);
    $table  = $this->get(self::OPT_TABLE);
    $qry    = "SELECT value FROM ".$prefix.$table." WHERE sesskey = ? AND expiry > ? LIMIT 1";
    $result = $db->query($qry, array($key, time()), 'el');  
    if ($result) {
      return $result;
    } else {
      $qry = "DELETE FROM ".$prefix.$table. " WHERE sesskey = ? AND expiry < ?";
      $ar  = $db->query($qry, array($key, time()), 'ar');
    }

    return false;
  }

  /**
   * sess_write
   *
   * @param mixed $key
   * @param mixed $val
   * @uses OPT_DB, DB::OPT_PREFIX
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function sess_write($key, $val) {
    if($this->get(self::OPT_FINISHED)) return;
    $this->set(self::OPT_FINISHED, true);
    $db     = $this->register_model('DB')->get(DB::OPT_DB);
    $helper = $this->register_model('Helper');
    $prefix = $helper->get(DB::OPT_PREFIX);
    $table  = $this->get(self::OPT_TABLE);
    $expiry = time() + $this->get(self::OPT_SESSIONS_LIFE);
    $expire_datum = date("d.m.Y, H:i:s", $expiry);
    $value  = $val;

    $qry    = "INSERT IGNORE INTO ".$prefix.$table. " VALUES (?, ?, ?)";
    $id     = false;
    try{
      $id   = $db->query($qry, array($key, $expiry, $value), 'id');
    }
    catch(Exception $e){

    }
    if (!$id)
    {
      $qry  = "UPDATE ".$prefix.$table. " SET expiry = ?, value = ?  WHERE sesskey = ?  AND expiry > ?";
      $id   = $db->query($qry, array($expiry, $value, $key, time()), 'ar');
    }

    return $id;
  }

  /**
   * sess_destroy
   *
   * @param mixed $key
   * @uses OPT_DB, DB::OPT_PREFIX
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function sess_destroy($key) {
    $db     = $this->register_model('DB')->get(DB::OPT_DB);
    $helper = $this->register_model('Helper');
    $prefix = $helper->get(DB::OPT_PREFIX);
    $table  = $this->get(self::OPT_TABLE);
    $qry    = "DELETE FROM ".$prefix.$table. " WHERE sesskey = ?";
    $ar     = $db->query($qry, array($key), 'ar'); 

    return $ar;
  }

  /**
   * sess_gc
   *
   * @param mixed $maxlifetime
   * @uses OPT_DB, DB::OPT_PREFIX
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function sess_gc($maxlifetime) {
    $db     = $this->register_model('DB')->get(DB::OPT_DB);
    $helper = $this->register_model('Helper');
    $prefix = $helper->get(DB::OPT_PREFIX);
    $table  = $this->get(self::OPT_TABLE);
    $qry    = "DELETE FROM ".$prefix.$table. " WHERE expiry < ?";
    $ar     = $db->query($qry, array(time()), 'ar');

    return $ar;
  }

  /**
   * event_controllers_permissions
   *
   * @param View $view
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   * @throw Exceptions_Permissions
   */
  public function event_controllers_permissions(View $view){
    $area     = isset($view->area) && strlen($view->area) ? $view->area : 1;
    $action   = isset($view->action) ? $view->action : '';
    $global   = $this->get(self::OPT_ACCESS_ACTION);
    if (isset($_SESSION[$action . $area]) &&  $_SESSION[$action . $area] == 1) return true;
    if (isset($_SESSION[$global . $area]) && ($_SESSION[$global . $area] == 1)) return true;
    if (isset($_SESSION[$global]) && ($_SESSION[$global] == 1)) return true;
    throw new Accido\Exceptions\Permission('access denied.');
  }

  /**
   * event_controllers_session
   *
   * @param View $view
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function event_controllers_session(View $view){
    $id = session_id();
    if (empty($id)){
      session_start();
      session_name($this->get(self::OPT_SESSIONS_NAME));
    }
  }

  /**
   * event_controllers_shutdown
   *
   * @param View $view
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function event_controllers_shutdown(View $view){
    $this->session_shutdown();
  }

}
