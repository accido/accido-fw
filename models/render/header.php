<?php
namespace Accido\Models\Render;
use Accido\Render;
use Accido\Models\Response;
use Accido\Model;
use Accido\Controller;
use Accido\Models\Socket;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Header
 *
 * @package Render
 * @subpackage Model
 * 
 * @see Render
 * 
 * @see Model
 * @final
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
final class Header extends Model implements Render{

  /**
   * vars
   *
   * @var array
   */
  protected $vars                           = array(
    self::OPT_EVENTS                        => array(
      
    ),
  );

  /**
   * init
   *
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  protected function init(){

  }

  /**
   * __invoke
   *
   * @param Controller $ctrl
   * @param array $headers
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function __invoke(Controller $ctrl, array $headers = array()){
    $ctrl->stream->commit()->then(function(Controller $ctrl) use(&$headers){
        if(headers_sent()) return $ctrl;
        foreach($headers as $index => $item){
          $header                                 = $index . ': ' . $item;
          header($i . ': ' . $j, true);
        }
        return $ctrl;
      });
    return $ctrl;
  }
}

