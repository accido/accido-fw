<?php
namespace Accido\Models\Render;
use Accido\Models\DB;
use Accido\Render;
use Accido\Model;
use Accido\Controller;
use Accido\Polymorph;
use Accido\Injector;
use Accido\Query;
use Accido\Models\Transaction;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Query
 *
 * @package Render
 * @subpackage Polymorph
 * 
 * @see Render
 * @see Polymorph
 * 
 * @see Model
 * @final
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
final class Query extends Model implements Render,Polymorph{

  /**
   * OPT_CONTROLLER
   *
   * @const string
   */
  const OPT_CONTROLLER                      = 'controller';
  /**
   * OPT_DATA
   *
   * @const string
   */
  const OPT_DATA                            = 'data';
  /**
   * OPT_FETCH
   *
   * @const string
   */
  const OPT_FETCH                           = 'fetch';
  /**
   * OPT_RESULT
   *
   * @const string
   */
  const OPT_RESULT                          = 'result';
  /**
   * OPT_COMPLETED
   *
   * @const string
   */
  const OPT_COMPLETED                       = 'completed';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                           = array(
    self::OPT_EVENTS                        => array(
      
    ),
    self::OPT_CONTROLLER                    => null,
    self::OPT_DATA                          => null,
    self::OPT_FETCH                         => null,
    self::OPT_RESULT                        => null,
    self::OPT_COMPLETED                     => false,
  );

  /**
   * init
   *
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  protected function init(){

  }

  /**
   * capture
   * 
   * @param Injector $dic
   * @param Controller $ctrl
   * @param array $data
   * @param string $fetch
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function capture(Injector $dic, Controller $ctrl = null, array $data = array(), $fetch = 'no'){
    $this[self::OPT_FETCH]                  = $fetch;
    $this[self::OPT_DATA]                   = $data;
    $this[self::OPT_CONTROLLER]             = $ctrl;
  }

  /**
   * query
   * 
   * @param Controller $ctrl
   * @param array $data
   * @param string $fetch
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function query(Controller $ctrl, array $data, $fetch){
    $_this                                  = $this;
    $ctrl->stream->commit()->then(function(Controller $ctrl) use(&$_this, &$data, &$fetch, &$transaction){
        if(!empty($_this[self::OPT_CONTROLLER]) && $_this[self::OPT_COMPLETED]) return $_this[self::OPT_RESULT];
        $result                             = null;
        $db                                 = $_this->register_model('DB')->get(DB::OPT_DB);
        $prefix                             = $_this->register_model('Helper')->get(DB::OPT_PREFIX);
        $db->transactionRun(function() use(&$db, &$ctrl, &$prefix, &$result, &$data, &$fetch){
            $view                           = $ctrl->view;
            $view->prefix                   = $prefix;
            $query                          = $view->render();
            $result                         = $db->query($query, $data, $fetch);
            return $result;
          });
        $_this[self::OPT_RESULT]            = $result;
        $_this[self::OPT_COMPLETED]         = true;
        return $result;
      });
    return $ctrl;
  }

  /**
   * __invoke
   *
   * @param Controller $ctrl
   * @param array $data
   * @param string $fetch
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function __invoke(Controller $ctrl, array $data = array(), $fetch = 'no', $transaction = ''){
    return $this->query($ctrl, $data, $fetch);
  }

  /**
   * done
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function done(){
    return $this->query($this[self::OPT_CONTROLLER],
                        $this[self::OPT_DATA],
                        $this[self::OPT_FETCH]);
  }

  /**
   * fail
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function fail(){
    return $this->query($this[self::OPT_CONTROLLER],
                        $this[self::OPT_DATA],
                        $this[self::OPT_FETCH]);
  }
}
