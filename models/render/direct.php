<?php
namespace Accido\Models\Render;
use Accido\Model;
use Accido\Controller;
use Accido\Injector;
use Accido\Render;
use Accido\Models\Request;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Direct
 *
 * @package Render
 * @subpackage Model
 * 
 * @see Render
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Direct extends Model implements Render {

  protected $vars                               = [];

  /**
   * sanitize_path
   * 
   * @param string $path
   *
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  protected function sanitize_path($path){
    return rtrim($path, DIRECTORY_SEPARATOR);
  }

  public function capture(Injector $dic, $path){
    if(is_array($path)){
      foreach($path as $regex => $base){
        $this->vars[$regex]                     = $this->sanitize_path($base);
      }
      return;
    }
    $this->vars['.*']                           = $this->sanitize_path($path);
  }

  public function __invoke(Controller $ctrl){
    $base                                       = $this->vars;
    $ctrl->commit()->then(function(Controller $ctrl) use($base) {
      $file                                     = implode('/', $ctrl->request[Request::OPT_REQUEST_URI]);
      foreach($base as $regex => $path){
        if(!preg_match($regex, $file, $match)){
          continue;
        }
        $file                                   = $path . DIRECTORY_SEPARATOR . $file;
        break;
      }
      $ctrl->view->set_filename($file);
      return $ctrl;
    });
    return $ctrl;
  }

}
