<?php
namespace Accido\Models\Render;
use Accido\Models\DB;
use Accido\Model;
use Accido\Render;
use Accido\Controller;
use Accido\Polymorph;
use Accido\Injector;
use Accido\Query;
use Accido\Models\Transaction;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: MultiQuery
 *
 * @package Render
 * @subpackage Polymorph
 * 
 * @see Render
 * @see Polymorph
 * 
 * @see Model
 * @final
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
final class MultiQuery extends Model implements Render,Polymorph {

  /**
   * VAR_CONTROLLER
   *
   * @const string
   */
  const VAR_CONTROLLER                      = 'controller';
  /**
   * VAR_DATA
   *
   * @const string
   */
  const VAR_DATA                            = 'data';
  /**
   * VAR_FETCH
   *
   * @const string
   */
  const VAR_FETCH                           = 'fetch';
  /**
   * VAR_RESULT
   *
   * @const string
   */
  const VAR_RESULT                          = 'result';
  /**
   * VAR_COMPLETED
   *
   * @const string
   */
  const VAR_COMPLETED                       = 'completed';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                           = array(
    self::OPT_EVENTS                        => array(
      
    ),
    self::VAR_CONTROLLER                    => null,
    self::VAR_DATA                          => null,
    self::VAR_FETCH                         => null,
    self::VAR_RESULT                        => null,
    self::VAR_COMPLETED                     => false,
  );

  /**
   * init
   *
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  protected function init(){

  }

  /**
   * capture
   * 
   * @param Injector $dic
   * @param Controller $ctrl
   * @param array|null $data
   * @param array|null $fetch
   * @param bool $transaction
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function capture(Injector $dic, Controller $ctrl = null, $data = null, $fetch = null){
    $this[self::VAR_FETCH]                  = $fetch;
    $this[self::VAR_DATA]                   = $data;
    $this[self::VAR_CONTROLLER]             = $ctrl;
  }

  /**
   * multi_query
   *
   * @param Controller $ctrl
   * @param array|null $data
   * @param array|null $fetch
   * @param bool $transaction
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @todo %%query%% - query separator
   * @return Controller
   */
  public function multi_query(Controller $ctrl, $data = null, $fetch = null, Transaction $transaction = null){
    $_this                                  = $this;
    $ctrl->commit()->then(function(Controller $ctrl) use(&$data, &$fetch, &$transaction, &$_this){
        if(!empty($_this[self::VAR_CONTROLLER]) && $_this[self::VAR_COMPLETED]){
          return $_this[self::VAR_RESULT];
        }
        $result                             = array();
        $db                                 = $_this->register_model('DB')->get(DB::OPT_DB);
        $view                               = $ctrl->view;
        $view->prefix                       = $_this->register_model('Helper')->get(DB::OPT_PREFIX);
        $query                              = $view->render();
        $temp_query                         = explode('%%query%%', $query);
        if(1 < count($temp_query)) 
          $query                            = $temp_query;
        if(null === $data){
          $data                             = array();
          foreach($temp_query as $temp){
            if(!empty($temp)){
              $data[]                       = array();
            }
          }
        }
        $result                             = $db->multiQuery($query, $data, $fetch, true);
        $_this[self::VAR_RESULT]            = $result;
        $_this[self::VAR_COMPLETED]         = true;
        return $result;
      });
    return $ctrl;
  }

  
  /**
   * __invoke
   * 
   * @param Controller $ctrl
   * @param array|null $data
   * @param array|null $fetch
   * @param bool $transaction
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function __invoke(Controller $ctrl, $data = null, $fetch = null){
    return $this->multi_query($ctrl, $data, $fetch);
  }

  /**
   * done
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function done(){
    return $this->multi_query($this[self::VAR_CONTROLLER],
                              $this[self::VAR_DATA],
                              $this[self::VAR_FETCH]);
  }

  /**
   * fail
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function fail(){
    return $this->multi_query($this[self::VAR_CONTROLLER],
                              $this[self::VAR_DATA],
                              $this[self::VAR_FETCH]);
  }
}
