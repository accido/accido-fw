<?php
namespace Accido\Models\Render\Socket;
use Accido\Model;
use Accido\Render;
use Accido\Controller;
use Accido\Models\Request;
use Accido\Loader;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Relate
 *
 * @package Render
 * @subpackage Socket
 * 
 * @see Render
 * 
 * @see Model
 * @final
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
final class Relate extends Model implements Render {

  /**
   * @const string
   */
  const ATTR_RESPONSE_COOKIE                                    = 'x-socket-id';
  /**
   * @const string
   */
  const ATTR_UNIQUE_COOKIE                                      = 'x-unique-id';

  /**
   * @const string
   */
  const ATTR_EXPIRY                                             = 3600;

  public function __invoke(Controller $ctrl){
    $ctrl->commit()->then(function($ctrl){
      $cookie                                                   = &$ctrl->request->ref(Request::OPT_COOKIE_VARS);
      $loader                                                   = new Loader(self::$dic);
      $helper                                                   = $loader->instance('Socket/Helper', $ctrl);
      if(!isset($cookie[self::ATTR_RESPONSE_COOKIE])){
        $key                                                    = $helper->key();
        $ctrl->response->header->cookie(self::ATTR_RESPONSE_COOKIE, $key, self::ATTR_EXPIRY);
        $cookie[self::ATTR_RESPONSE_COOKIE]                     = $key;
      }

      $key                                                      = $helper->key();
      $ctrl->response->header->cookie(self::ATTR_UNIQUE_COOKIE, $key, 0);
      $cookie[self::ATTR_UNIQUE_COOKIE]                         = $key;
      return $ctrl;
    });
    return $ctrl;
  }

}
