<?php
namespace Accido\Models\Render\Socket;
use Accido\Model;
use Accido\Render;
use Accido\Controller;
use Accido\Models\Request;
use Accido\Models\Socket;
use Accido\Models\Action\Init\Server;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Reader
 *
 * @package Socket
 * @subpackage Render
 * 
 * @see Render
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
Class Reader extends Model implements Render {

  const ATTR_READ_LENGTH                                = 32768;

  public function __invoke(Controller $ctrl){
    $ctrl
      ->commit()
      ->promise(function($ctrl, $resolve, $reject){
        if(!($ctrl instanceof Controller)){
          $reject($ctrl);
          return;
        }
        $socket                                         = $ctrl->request[Request::OPT_SOCKET];
        $resource                                       = $socket[Socket::OPT_SOCKET];
        $server                                         = $socket[Socket::OPT_SERVER];
        $write                                          = &$server->ref(Server::OPT_WRITE);
        $name                                           = (int)$resource;
        $write[$name]                                   = $resource;
        $content                                        = &$socket->ref(Socket::OPT_CONTENT);
        $file                                           = $ctrl->view->get_filename();
        if(empty($file)){
          $reject($ctrl);
          return;
        }
        $file                                           = fopen($file, 'rb');
        if(!is_resource($file)){
          $reject($ctrl);
          return;
        }
        while(!feof($file)){
          $content                                      .= fread($file, Reader::ATTR_READ_LENGTH);
          $write[$name]                                 = $resource;
          yield;
        }
        fclose($file);
        $resolve($ctrl);
      });
    return $ctrl;
  }
}
