<?php
namespace Accido\Models\Render;
use Accido\Model;
use Accido\Injector;
use Accido\Controller;
use Accido\Render;
use Accido\Models\Response;
use Accido\Models\Response\Http\Header;
use Accido\Exceptions\Option as Error;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Mime
 *
 * @package Render
 * @subpackage Header
 * 
 * @see Render
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Mime extends Model implements Render {
  const OPT_JAVASCRIPT                                = 'js';
  const OPT_STYLESHEET                                = 'css';
  const OPT_IMAGE_GIF                                 = 'gif';
  const OPT_IMAGE_JPG                                 = 'jpg';
  const OPT_IMAGE_JPEG                                = 'jpeg';

  protected $vars                                     = [
    self::OPT_JAVASCRIPT                              => 'application/x-javascript',
    self::OPT_STYLESHEET                              => 'text/css',
    self::OPT_IMAGE_GIF                               => 'image/gif',
    self::OPT_IMAGE_JPEG                              => 'image/jpeg',
    self::OPT_IMAGE_JPG                               => 'image/jpg',
  ];

  public function __invoke(Controller $ctrl){
    $_this                                            = $this;
    $ctrl->commit()->then(function(Controller $ctrl) use ($_this) {
      $res                                            = $ctrl->response;
      $file                                           = $ctrl->view->get_filename();
      $_this->ensure(!file_exists($file), ': file not exists', [':file' => $file], $res);
      $_this->ensure(!preg_match('/\.(\w+)$/', $file, $match), 'mime type not found', [], $res);
      $mime                                           = $_this[$match[1]];
      $res->http([
        Header::CONTENT_TYPE                          => $mime  
      ]);
      return $ctrl;
    });
    return $ctrl;
  }

  public function ensure($error, $msg, array $params = array(), Response $res){
    if(!$error){
      return;
    }
    $throw                                            = new Error();
    $throw->detail                                    = $this->resolve_error($msg, $params);
    $res->http([
      Header::CONNECTION                              => 'closed',
      Header::SERVER                                  => Header::ATTR_DEFAULT_SERVER,
      ], 404, 'Not Found');
    throw $throw;
  }
}
