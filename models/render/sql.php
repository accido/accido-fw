<?php
namespace Accido\Models\Render;
use Accido\Model;
use Accido\Models\Render;
use Accido\Polymorph;
use Accido\Injector;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: SQL
 *
 * @package Transaction
 * @subpackage Render
 * 
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
final class SQL extends Model implements Render,Polymorph {

    /**
   * OPT_CONTROLLER
   *
   * @const string
   */
  const OPT_CONTROLLER                      = 'controller';
  /**
   * OPT_RESULT
   *
   * @const string
   */
  const OPT_RESULT                          = 'result';
  /**
   * OPT_COMPLETED
   *
   * @const string
   */
  const OPT_COMPLETED                       = 'completed';
                                                                                                                            
  /**
   * vars
   *
   * @var array
   */
  protected $vars                           = array(
    self::OPT_CONTROLLER                    => null,
    self::OPT_RESULT                        => null,
    self::OPT_COMPLETED                     => false,
  );
                                                                                                                            
  /**
   * capture
   * 
   * @param Injector $dic
   * @param Controller $ctrl
   * @param array $data
   * @param string $fetch
   * @param string $transaction
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function capture(Injector $dic, Controller $ctrl = null){
    $this[self::OPT_CONTROLLER]             = $ctrl;
  }
                                                                                                                            
  /**
   * query
   * 
   * @param Controller $ctrl
   * @param array $data
   * @param string $fetch
   * @param string $transaction
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function query(Controller $ctrl){
    $_this                                  = $this;
    $ctrl->stream->commit()->then(function(Controller $ctrl) use(&$_this){
        if(!empty($_this[self::OPT_CONTROLLER]) && $_this[self::OPT_COMPLETED]) return $_this[self::OPT_RESULT];
        $prefix                             = $_this->register_model('Helper')->get(DB::OPT_PREFIX);
        $view                               = $ctrl->view;
        $view->prefix                       = $prefix;
        $query                              = $view->render();
        $_this[self::OPT_RESULT]            = $query;
        $_this[self::OPT_COMPLETED]         = true;
        return $query;
      });
    return $ctrl;
  }
                                                                                                                            
  /**
   * __invoke
   *
   * @param Controller $ctrl
   * @param array $data
   * @param string $fetch
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function __invoke(Controller $ctrl){
    return $this->query($ctrl);
  }
                                                                                                                            
  /**
   * done
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function done(){
    return $this->query($this[self::OPT_CONTROLLER]);
  }
                                                                                                                            
  /**
   * fail
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function fail(){
    return $this->query($this[self::OPT_CONTROLLER]);
  }

}
