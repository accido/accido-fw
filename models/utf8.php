<?php 
namespace Accido\Models;
use Accido\Model;
use Accido\Event;
defined('CORE_ROOT') or die('No direct script access.');
require_once CORE_ROOT . '/utf8/lib/portable-utf8.php';
require_once CORE_ROOT . '/utf8/lib/utf8.class.php';
/**
 * Class: Utf8
 *
 * @package UTF-8
 * @subpackage Model
 *
 * 
 * @see Model
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version 0.1
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Utf8 extends Model{

  /**
   * OPT_UTF8_MAPPER
   *
   * @const string
   */ 
  const OPT_UTF8_MAPPER           = 'utf8_mapper';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                 = array(
    self::OPT_EVENTS              => array(
    ),
    self::OPT_UTF8_MAPPER         => array(
      
    ),
  );

  /**
   * init
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function init(){

  }

  /**
   * load_mapper
   *
   * @param mixed $charset
   * @uses OPT_UTF8_MAPPER
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Utf8
   */
  protected function load_mapper($charset){
    $key      = self::OPT_UTF8_MAPPER . '.' . $charset;
    $this->register_param($this->vars, $key, new \utf8($charset));
    return $this->get($key);
  }

  /**
   * to_utf8
   *
   * @param mixed $array
   * @param mixed $charset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function to_utf8($array, $charset = CP1250){
    return $this->load_mapper($charset)->strToUtf8($array);
  }

  /**
   * from_utf8
   *
   * @param mixed $array
   * @param mixed $charset
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function from_utf8($array, $charset = CP1250){
    return $this->load_mapper($charset)->utf8ToStr($array);
  }

  /**
   * convert_encoding
   *
   * @param mixed $array
   * @param string $current
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function convert_encoding($array, $encoding = 'UTF-8', $current = '', $check = false){
    $result = $array;
    if(!$check && (empty($encoding) || empty($current) || strtolower($encoding) === strtolower($current))){
      return $result;
    }
    else{
      $check = true;
    }
    if (is_string($array)){
      if ('UTF-8' === $encoding){
        $result = $this->to_utf8($array, $current);
      }
      elseif('UTF-8' === $current){
        $result = $this->from_utf8($array, $encoding);
      }
      else{
        $result = $this->from_utf8($this->to_utf8($array, $current), $encoding);
      }
    }
    elseif(is_array($array)){
      $result = array();
      foreach($array as $k => $v){
        $result[$this->convert_encoding($k, $encoding, $current, $check)] = $this->convert_encoding($v, $encoding, $current, $check);
      }
    }
    return $result;
  }

  /**
   * json_encode
   *
   * @param array $array
   * @param string $current
   * @param string $destinition
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function json_encode(array $array, $current = '', $destinition = ''){
    // @todo maybe not needed?
    if (empty($current)){
      $current = $this->register_model('Helper')->get(self::OPT_CHARSET_ENCODING);
    }
    $array = json_encode($this->convert_encoding($array, 'UTF-8', $current));
    if(empty($destinition)) $destinition = $current;
    return $this->convert_encoding($array, $destinition, 'UTF-8');
  }

  /**
   * json_decode
   *
   * @param mixed $json
   * @param string $current
   * @param string $destinition
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return array
   */
  public function json_decode($json, $current = '', $destinition = ''){
    // @todo maybe not needed?
    if (empty($current)){
      $current = $this->register_model('Helper')->get(self::OPT_CHARSET_ENCODING);
    }
    $json  = json_decode($this->convert_encoding($json, 'UTF-8', $current), true);
    if(empty($destinition)) $destinition = $current;
    return $this->convert_encoding($json, $destinition, 'UTF-8'); 
  }

  /**
   * urldecode
   *
   * @param mixed $url
   * @param string $current
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function urldecode($url, $current = ''){
    $url = $this->convert_encoding($url, 'UTF-8', $current);
    $url = preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($url));
    return html_entity_decode($url, null, 'UTF-8');
  }

  /**
   * __call
   *
   * @param mixed $args
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function __call($name , array $args){
    if (function_exists('utf8_' . $name)){
      return call_user_func_array('utf8_' . $name, $args);
    }
    elseif(function_exists($name)){
      return call_user_func_array($name, $args);
    }
    else{
      return parent::__call($name, $args);
    }
  }

}
