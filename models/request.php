<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Event;
use Accido\Models\Socket;
use Accido\Models\Action\Init\Server;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Request
 *
 * @package Request
 * @subpackage Model
 *
 * 
 * @see Model
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Request extends Model{

  /**
   * ATTR_DEFAULT_REQUEST_CHARSET
   *
   * @const string
   */
  const ATTR_DEFAULT_REQUEST_CHARSET      = 'UTF-8';

  /**
   * OPT_POST_VARS
   *
   * @const string
   */
  const OPT_POST_VARS                     = 'post';

  /**
   * OPT_GET_VARS
   *
   * @const string
   */
  const OPT_GET_VARS                      = 'get';

  /**
   * OPT_COOKIE_VARS
   *
   * @const string
   */
  const OPT_COOKIE_VARS                  = 'cookie';

  /**
   * OPT_SERVER_VARS
   *
   * @const string
   */
  const OPT_SERVER_VARS                   = 'server';
  
  /**
   * OPT_ENV_VARS
   *
   * @const string
   */
  const OPT_ENV_VARS                      = 'env';
    
  /**
   * OPT_PARAMETER_VARS
   *
   * @const string
   */
  const OPT_PARAMETER_VARS                = 'parameter';

  /**
   * OPT_REQUEST_HEADERS
   *
   * @const string
   */
  const OPT_REQUEST_HEADERS               = 'header';

  /**
   * OPT_MODEL_HELPER
   *
   * @const string
   */
  const OPT_MODEL_HELPER                  = 'model_helper';

  /**
   * OPT_REQUEST_CHARSET
   *
   * @const string
   */
  const OPT_REQUEST_CHARSET               = 'request_charset';

  /**
   * OPT_PARAMS_CHARSET
   *
   * @const string
   */
  const OPT_PARAMS_CHARSET                = 'params_charset';

  /**
   * OPT_MODEL_UTF8
   *
   * @const string
   */
  const OPT_MODEL_UTF8                    = 'model_utf8';

  /**
   * OPT_REQUEST_URI
   *
   * @const string
   */
  const OPT_REQUEST_URI                   = 'uri';

  /**
   * OPT_REQUEST_LANG
   * 
   *
   * @const string
   */
  const OPT_REQUEST_LANG                  = 'lang';
  
  /**
   * OPT_REQUEST_PATH
   * 
   *
   * @const string
   */
  const OPT_REQUEST_PATH                  = 'path';

  /**
   * OPT_IS_GLOBAL
   * 
   *
   * @const string
   */
  const OPT_IS_GLOBAL                     = 'is_global';
  /**
   *
   *
   * @const string
   */
  const OPT_SOCKET                        = 'socket';
  /**
   * @const string
   */
  const OPT_BODY                          = 'body';

  /**
   * resolve_type
   *
   * @var array
   */
  public $resolve_type                   = array(
    self::OPT_POST_VARS                   => INPUT_POST,
    self::OPT_GET_VARS                    => INPUT_GET,
    self::OPT_COOKIE_VARS                 => INPUT_COOKIE,
    self::OPT_SERVER_VARS                 => INPUT_SERVER,
    self::OPT_ENV_VARS                    => INPUT_ENV,
  );

  /**
   * vars
   *
   * @var array
   */
  protected $vars                       = array(
    self::OPT_EVENTS                    => array(
      
    ),
    self::OPT_POST_VARS                 => array(
      
    ),
    self::OPT_GET_VARS                  => array(
      
    ),
    self::OPT_COOKIE_VARS               => array(
      
    ),
    self::OPT_SERVER_VARS               => array(
      
    ),
    self::OPT_ENV_VARS                  => array(
      
    ),
    self::OPT_REQUEST_HEADERS           => array(
      
    ),
    self::OPT_PARAMETER_VARS            => array(
      
    ),
    self::OPT_MODEL_HELPER              => null,
    self::OPT_REQUEST_CHARSET           => '',
    self::OPT_PARAMS_CHARSET            => '',
    self::OPT_MODEL_UTF8                => null,
    self::OPT_REQUEST_URI               => '',
    self::OPT_BASE_URL                  => '',
    self::OPT_REQUEST_PATH              => '',
    self::OPT_REQUEST_LANG              => '',
    self::OPT_CHARSET_ENCODING          => '',
    self::OPT_IS_GLOBAL                 => false,
    self::OPT_SOCKET                    => null,
    self::OPT_BODY                      => null,
  );

  /**
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function set_cookie(){
    $header                             = $this->get_header('Cookie');
    if(empty($header)){
      return;
    }
    $data                               = explode('; ', $header);
    $cookie                             = [];
    foreach($data as $item){
      list($name, $value)               = explode('=', $item);
      $cookie[$name]                    = urldecode($value);
    }
    $this->set(self::OPT_COOKIE_VARS, $cookie);
  }

  /**
   * set_locale
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function set_locale(){
    $charset  = $this[self::OPT_CHARSET_ENCODING];
    $locale   = !empty($charset) ? $this[self::OPT_REQUEST_LANG] . '.' . $charset : $this[self::OPT_REQUEST_LANG];
    if(!empty($locale)){
      setlocale(LC_ALL, $locale);
    }
    if(!empty($charset) && function_exists('mb_internal_encoding')){
      mb_internal_encoding($charset);
    }
  }

  /**
   * set_charset
   *
   * @uses OPT_MODEL_HELPER,OPT_REQUEST_CHARSET
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function set_charset(){
    $find             = false;
    $current          = strtoupper($this[self::OPT_CHARSET_ENCODING]);
    if (strlen($s     = $this->get_header('Accept-Charset'))){
      $s              = (array)explode(';', $s);
      $s              = (array)explode(',', $s[0]);
      array_walk($s,function(&$value){
        $value        = trim(strtoupper($value));
      });
      if(!empty($s)){
        if(in_array(self::ATTR_DEFAULT_REQUEST_CHARSET, $s)){
          $s          = self::ATTR_DEFAULT_REQUEST_CHARSET;
        }
        elseif(in_array($current, $s)){
          $s          = $current;
        }
        else{
          $s          = (string)$s[0];
        }
        $this->set(self::OPT_REQUEST_CHARSET, $s);
        $find         = true;
      }
    }
    elseif(strlen($s  = $this->get_header('Content-Type'))){
      if(preg_match('/charset\\s*=\\s*([^\\s\\r\\n;]++)/iu', $s, $matches)){
        $this->set(self::OPT_REQUEST_CHARSET, strtoupper(trim($matches[1])));
        $find         = true;
      }
    }
    if(!$find){
      $this->set(self::OPT_REQUEST_CHARSET, self::ATTR_DEFAULT_REQUEST_CHARSET);
    }
  }

  /**
   * set_lang
   *
   * @uses OPT_REQUEST_LANG
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function set_lang(){
    if (strlen($s = $this->get_header('Accept-Language'))){
      $this->set(self::OPT_REQUEST_LANG, trim($s));
    }
    else{
      $lang       = $this->register_model('Helper')->get(self::OPT_LANGUAGE);
      $this->set(self::OPT_REQUEST_LANG, trim($lang));
    }
  }

  /**
   * apache_request_headers
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return array
   */
  public function apache_request_headers() {
    if (function_exists('apache_request_headers')){
      return apache_request_headers();
    }
    $arh                = array();
    $rx_http            = 'HTTP_';
    foreach($_SERVER as $key => $val) {
      if(0 === strncmp($key, $rx_http, 5)){
        $arh_key        = strtolower(substr($key, 5));
        $arh_key        = str_ireplace('_', ' ', $arh_key);
        $arh_key        = str_ireplace(' ', '-', ucwords($arh_key));
        $arh[$arh_key]  = $val;
      }
    }
    return( $arh );
  }


  /**
   * parse
   *
   * @param mixed $id
   * @uses OPT_PARAMS_CHARSET, OPT_POST_VARS, OPT_GET_VARS, OPT_SERVER_VARS, OPT_COOKIE_VARS
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public  function parse($id){
    $args           = array();
    $array          = array();
    $current        = $this->get(self::OPT_CHARSET_ENCODING);
    $charset        = $this->get(self::OPT_REQUEST_CHARSET);
    switch($id){
      case INPUT_POST:
      $name         = self::OPT_POST_VARS;
      if(function_exists('mb_http_input')){
        $pchars     = $this->set(self::OPT_PARAMS_CHARSET, mb_http_input('P'));
        if(!empty($pchars) && 'pass' !== $pchars){
          $charset  = $pchars;
        }
      }
      parse_str(file_get_contents('php://input'), $array);
      if($charset === $current){
        $this->set($name, $array);
        return;
      }
      break;
      case INPUT_GET:
      $name         = self::OPT_GET_VARS;
      $temp         = 0;
      $request_uri  = urldecode($this->get('server.REQUEST_URI', FILTER_SANITIZE_ENCODED));
      if(false !== ($temp = strpos($request_uri, '?'))){
        $gets       = substr($request_uri, $temp);
        $gets       = trim($gets, '?');
        parse_str($gets, $array);
      }
      if(function_exists('mb_http_input')){
        $pchars     = $this->set(self::OPT_PARAMS_CHARSET, mb_http_input('G'));
        if(!empty($pchars) && 'pass' !== $pchars){
          $charset  = $pchars;
        }
      }
      if($charset === $current){
        $this->set($name, $array);
        return;
      }
      break;
      case INPUT_SERVER:
      $name         = self::OPT_SERVER_VARS;
      $array        = &$_SERVER;
      if(self::ATTR_DEFAULT_REQUEST_CHARSET === $current){
        $this->set($name, $array);
        return;
      }
      $this->set(self::OPT_PARAMS_CHARSET, self::ATTR_DEFAULT_REQUEST_CHARSET);
      break;
      case INPUT_COOKIE:
      $name         = self::OPT_COOKIE_VARS;
      $array        = &$_COOKIE;
      if(function_exists('mb_http_input')){
        $pchars     = $this->set(self::OPT_PARAMS_CHARSET, mb_http_input('C'));
        if(!empty($pchars) && 'pass' !== $pchars){
          $charset  = $pchars;
        }
      }
      if($charset === $current){
        $this->set($name, $array);
        return;
      }
      break;
      case INPUT_ENV:
      $name         = self::OPT_ENV_VARS;
      $array        = &$_ENV;
      if(self::ATTR_DEFAULT_REQUEST_CHARSET === $current){
        $this->set($name, $array);
        return;
      }
      $this->set(self::OPT_PARAMS_CHARSET, self::ATTR_DEFAULT_REQUEST_CHARSET);
      break;
    }

    foreach($array as $param_key => $param_value){
      $args[$param_key] = array(
        'filter'  => FILTER_CALLBACK,
        'options' => array($this, 'sanitize_input'),
      );
    }

    $this->set($name, filter_var_array($array, $args));
  }

  /**
   * resolve_uri
   *
   * @uses OPT_REQUEST_URI
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  public function resolve_uri(){
    $helper   = $this->register_model('Helper');
    $base_url = $helper->get(self::OPT_BASE_URL);
    $url      = '';
    $path     = '';
    $uri      = array('/');
    if (isset($this['server.PHP_SELF'])){
      $self   = urldecode($this->get('server.PHP_SELF', FILTER_SANITIZE_ENCODED));
      $suri   = urldecode($this->get('server.REQUEST_URI', FILTER_SANITIZE_ENCODED));
      $sport  = '' . $this->get('server.SERVER_PORT', FILTER_SANITIZE_NUMBER_INT);
      $sprot  = $this->get('server.SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
      $sname  = urldecode($this->get('server.SERVER_NAME', FILTER_SANITIZE_ENCODED));
      $sname  = preg_replace('/:.*/ui', '', $sname);
      $uri    = (array)explode('?', $self);
      $uri    = preg_split('#\\/[\\w_\\-\\.]+\\.php#u', $uri[0]);
      $base   = preg_replace('/\\/[\\w_\\-\\.]+\\.php(?:\\/)?/ui', '/', $self);
      $suri   = preg_replace('/\\/[\\w_\\-\\.]+\\.php(?:\\/)?/ui', '/', $suri);
      if ('/' === $base) $base = '';
      $ruri   = str_ireplace($base, '', $suri);
      $ruri   = (array)explode('?', $ruri);
      $ruri   = trim($ruri[0], '/\\');
      if(!isset($uri[1])) $uri[1] = '';
      $uri[1] .= empty($ruri) ? '' : '/' . $ruri;
      $prot = (array)explode('/', $sprot);
      $prot = strtolower($prot[0]);
      $port = 0 !== strlen($sport) ? ':' . $sport : '';
      $host = $sname;
      $url  = trim($uri[0], '/\\');
      $path = empty($url) ? '/' : '/' . $url . '/';
      $url  = rtrim($prot . '://' . $host . $port . '/' . $url, '/');
      if(empty($base_url)){
        $helper->set(self::OPT_BASE_URL, $url);
      }
      $uri    = (array)explode('/', trim($uri[1], '\\/'));
      if(empty($uri) || empty($uri[0])) $uri = array('/');
    }
    $this->set(self::OPT_REQUEST_URI, $uri);
    $this->set(self::OPT_BASE_URL, $url);
    $this->set(self::OPT_REQUEST_PATH, $path);
  }

  /**
   * init
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function init(){
    $this->set(self::OPT_CHARSET_ENCODING, $this->register_model('Helper')->get(self::OPT_CHARSET_ENCODING));
  }

  /**
   * init_global
   *
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function init_global(){
    if($this->get(self::OPT_IS_GLOBAL)) return;
    $headers = $this->apache_request_headers();
    $this->set(self::OPT_REQUEST_HEADERS, $headers);
    $this->set_charset();
    $this->set_lang();
    $this->set_locale();

    $this->parse(INPUT_POST);
    $this->parse(INPUT_SERVER);
    $this->parse(INPUT_COOKIE);
    $this->parse(INPUT_GET);

    $this->resolve_uri();
    $this->set(self::OPT_IS_GLOBAL, true);
  }

  /**
   * sanitize_input
   *
   * @param mixed $value
   * @uses OPT_MODEL_UTF8
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function sanitize_input($value){
    if (!empty($value)){
      $charset      = $this->get(self::OPT_REQUEST_CHARSET);
      $pcs          = $this->get(self::OPT_PARAMS_CHARSET);
      if (!empty($pcs) && 'pass' !== $pcs){
        $charset    = $pcs;
      }
      $ac           = $this->get(self::OPT_CHARSET_ENCODING);
      if(!empty($charset) && $ac !== $charset){
        $utf8       = $this->register_model('Utf8');
        $value      = $utf8->convert_encoding($value, $ac, $charset);
      }
    }
    return $value;
  }

  /**
   * get_header
   *
   * @param string $name
   * @uses OPT_REQUEST_HEADERS
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  public function get_header($name){
    $name           = self::OPT_REQUEST_HEADERS . '.' . $name;
    if(!$this->exists($this->vars, $name)) return '';
    return $this[$name];
  }

  /**
   * get
   *
   * @param mixed $key
   * @param string $filter
   * @param string $option
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function get($key, $filter = '', $option = ''){
    $var      = parent::get($key);
    if(!empty($filter)){
      if(is_int($filter) && !is_array($var)){
        $args = array($var, $filter);
        if(!empty($option)) $args[] = $option;
        $var  = call_user_func_array('filter_var', $args);
      }
      else{
        $var  = filter_var_array($var, $filter);
      }
    }
    
    return $var;
  }

  /**
   * set
   *
   * @param mixed $key
   * @param mixed $value
   * @param string $charset
   * @uses OPT_PARAMS_CHARSET
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function set($key, $value, $charset = ''){
    if(!empty($charset)){
      $this->set(self::OPT_PARAMS_CHARSET, $charset);
      if(is_array($value)){
        $args         = array();
        $params       = array();
        foreach($value as $sid => $item){
          if(is_object($item)){
            continue;
          }
          $params[$sid] = $item;
          $args[$sid] = array(
            'filter'  => FILTER_CALLBACK,
            'options' => array($this, 'sanitize_input'),
          );
        }
        if(!empty($params)){
          $params       = filter_var_array($params, $args);
          foreach($params as $index => $item){
            $value[$index] = $item;
          }
        }
      }
      else{
        $value        = filter_var($value, FILTER_CALLBACK, array($this, 'sanitize_input'));
      }
    }
    return parent::set($key, $value);
  }

  /**
   * redirect
   *
   * @param mixed $loc
   * @param mixed $abs
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function redirect($loc, $abs = FALSE, $convert = TRUE)
  {
    if( ! $abs && preg_match('/^http(s)?\:\/\//', $loc) ) {
      $abs	  = TRUE;
    }
    if( ! $abs ) {
      if( $loc{0} != '/' ) {
        $loc	= $this->get(self::OPT_BASE_URL) . '/' .$loc;
      }
    }
    if ($convert){
      $utf8     = $this->register_model('Utf8');
      $ac       = $this->register_model('Helper')->get(self::OPT_CHARSET_ENCODING);
      $loc      = $utf8->convert_encoding($loc, 'UTF-8', $ac);
    }
    if( ! headers_sent() ) {
      header('Location: '.$loc);
    }
    echo '<meta http-equiv="refresh" content="0;url='.$loc.'" />';
    echo '<script type="text/javascript"> self.location = "'.$loc.'"; </script>';
    exit;
  }

  /**
   * resolve_http_header
   * 
   * @param string $data
   * @param Socket $socket
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function resolve_http_header($data, Socket $socket){
    $headers                                            = &$this->ref(self::OPT_REQUEST_HEADERS);
    $server                                             = array();
    $position                                           = strpos($data, "\r\n\r\n");
    $header                                             = trim(substr($data, 0, $position), " \r\n\s");
    preg_match('/^([^\r\n\s]+) ([^\r\n\s]+) ([^\r\n\s]+)/u', $header, $match);
    $method                                             = $match[1];
    $uri                                                = $match[2];
    $transport                                          = $match[3];
    $header                                             = trim(str_ireplace($match[0], '', $header), " \r\n\s");
    preg_match_all('/^([^:]+): (.+)$/um', $header, $match, PREG_SET_ORDER);
    foreach($match as $item){
      $name                                             = trim($item[1], " \r\n");
      $value                                            = trim($item[2], " \r\n");
      $headers[$name]                                   = $value;
      $name                                             = 'HTTP_' . strtoupper(str_ireplace('-', '_', $name));
      $server[$name]                                    = $value;
    }
    $this->set(self::OPT_REQUEST_HEADERS, $headers);
    $this->set_charset();
    $this->set_lang();
    $this->set_locale();
    $this->set_cookie();
    $this->set(self::OPT_PARAMS_CHARSET, null);
    $uri                                                = preg_replace('#^(?:.*?//)?' . $headers['Host'] . '(?::[0-9]+)?#i', '', $uri);
    list($remote, $remote_port)                         = explode(':', $socket[Socket::OPT_NAME_REMOTE]);
    list($local, $local_port)                           = explode(':', $socket[Socket::OPT_NAME_LOCAL]);
    $server                                             += [
      'REQUEST_URI'                                     => '/index.php' . $uri,
      'REQUEST_METHOD'                                  => $method,
      'PHP_SELF'                                        => '/index.php' . $uri,
      'GATEWAY_INTERFACE'                               => $transport,
      'SERVER_ADDR'                                     => $local,
      'SERVER_NAME'                                     => $headers['Host'],
      'SERVER_PROTOCOL'                                 => 'http',
      'QUERY_STRING'                                    => $headers['Host'] . '/index.php' . $uri,
      'DOCUMENT_ROOT'                                   => CORE_ROOT . '/public_html',
      'REMOTE_ADDR'                                     => $remote,
      'SCRIPT_FILENAME'                                 => CORE_ROOT . '/public_html/index.php',
      'SERVER_PORT'                                     => $local_port,
      'REMOTE_PORT'                                     => $remote_port,
    ];
    $this->set(self::OPT_SERVER_VARS, $server);
    $this->parse(INPUT_GET);
    $this->resolve_uri();
    $this->set(self::OPT_IS_GLOBAL, true);
    $this->set(self::OPT_SOCKET, $socket);
  }

  /**
   * resolve_http_body
   * 
   * @param mixed $body
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function resolve_http_body($body){
    $this[self::OPT_BODY]                               = $body;
    $type                                               = $this->get_header('Content-Type');
    if('multipart/form-data' !== $type || 'application/x-www-form-urlencoded' !== $type){
      return;
    }
    $current_ch                                         = $this->get(self::OPT_CHARSET_ENCODING);
    $request_ch                                         = $this->get(self::OPT_REQUEST_CHARSET);
    parse_str($body, $post_vars);
    if($current_ch !== $request_ch){
      $args                                             = array();
      foreach($post_vars as $param_key => $param_value){
        $args[$param_key]                               = array(
          'filter'                                      => FILTER_CALLBACK,
          'options'                                     => array($this, 'sanitize_input'),
        );
      }
      $post_vars                                        = filter_var_array($post_vars, $args);
    }
    $this->set(self::OPT_POST_VARS, $post_vars);
  }

}
