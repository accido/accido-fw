<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Header as H;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Header
 *
 * @package Header
 * @subpackage Model
 * 
 * @abstract
 *
 * @see \Accido\Header
 * @see Model
 *
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
abstract class Header extends Model implements H {

  /**
   * do_resolve
   * 
   * @param array $status
   * @param array $header
   *
   * @throw \Accido\Exceptions\Model
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string|false
   */
  public function do_resolve(array $status, array $header){
    $result                                   = implode(" ", $status);
    $result                                   .= empty($result) ? '' : "\r\n";
    if(empty($header)){
      $this->ensure(empty($result), 'Header are empty');
      return $result . "\r\n";
    }
    array_walk($header, function($item, $name) use (&$result) {
      $item                                   = (array)$item;
      foreach($item as $content){
        $result                               .= $name . ': ' . $content . "\r\n";
      }
    });
    $result                                   .= "\r\n";
    return $result;
  }

  abstract function resolve();

}
