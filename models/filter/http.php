<?php
namespace Accido\Models\Filter;
use Accido\Model;
use Accido\Controller;
use Accido\Filter;
use Accido\Injector;
use Accido\Option;
use Accido\Models\Request;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: HTTP
 *
 * @package Filter
 * @subpackage HTTP
 * 
 * @see Filter
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class HTTP extends Model implements Filter {

  protected $vars                                 = [
    Request::OPT_REQUEST_HEADERS                  => [],
    Request::OPT_REQUEST_URI                      => '',
    Request::OPT_GET_VARS                         => [],
    Request::OPT_POST_VARS                        => [],
    Request::OPT_ENV_VARS                         => [],
    Request::OPT_SERVER_VARS                      => [],
    Request::OPT_COOKIE_VARS                      => [],
  ];

  /**
   * controller
   *
   * @var Controller
   */
  protected $controller                           = null;

  public function capture(Injector $dic, array $rule =null){
    if($rule){
      $this->resolve_rule($rule);
    }
  }

  /**
   * resolve_rule
   * 
   * @param array $rule
   * @uses
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function resolve_rule(array $rule){
    foreach($rule as $index => $item){
      if(isset($this[$index])){
        $this[$index]                             = $item;
      }
      else{
        $this->add($index, $item);
      }
    }
  }

  /**
   * filter
   * 
   * @param Controller $ctrl
   * @param array $rule
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function filter(Controller $ctrl, array $rule){
    $match                                        = $ctrl->commit();
    $all                                          = [];
    $range                                        = [];
    $position                                     = count($match->merged);
    $req                                          = $ctrl->request;
    $vars                                         = $this->vars;
    $this->resolve_rule($rule);
    foreach($this->vars as $index => $item){
      if(!empty($item)){
        if(is_array($item)){
          foreach($item as $jndex => $item_rule){
            $match->listen($req[$index . '.' . $jndex]);
            $all[$index . '.' . $jndex]           = $item_rule;
            $range[$index . '.' . $jndex]         = ++$position;
          }
          continue;
        }
        if(Request::OPT_REQUEST_URI === $index && is_array($req[$index])){
          $match->listen(implode('/', $req[$index]));
        }
        else{
          $match->listen($req[$index]);
        }
        $all[$index]                              = $item;
        $range[$index]                            = ++$position;
      }
    }
    $this->vars                                   = $vars;
    $match
      ->filter($range, $all);
    return $ctrl;
  }

  /**
   * __invoke
   * 
   * @param Controller $ctrl
   * @param array|null $rule
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function __invoke(Controller $ctrl, array $rule = null){
    $rule                                         = $rule ? $rule : $this->vars;
    return $this->filter($ctrl, $rule);   
  }

}
