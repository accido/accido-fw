<?php
namespace Accido\Models\Response\Http;
use Accido\Models\Response\Header as ResponseHeader;
use Accido\Models\Http\Header as HeaderInterface;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Header
 *
 * @package Header
 * @subpackage HTTP
 * 
 * 
 * @see H
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Header extends ResponseHeader implements HeaderInterface {
  /**
   * @const string
   */
  const DELIMITER                             = "\r\n";
  /**
   * @const string
   */
  const TYPE_RELATE                           = 'application/vnd+relate+json';
  /**
   * @const string
   */
  const TYPE_SYNC                             = 'application/vnd+sync+json';
  /**
   * @const string
   */
  const CODE_200                              = 200;
  /**
   * @const string
   */
  const CODE_403                              = 403;
  /**
   * @const string
   */
  const CODE_404                              = 404;
  /**
   * @const string
   */
  const CONNECTION                            = 'Connection';
  /**
   * @const string
   */
  const SERVER                                = 'Server';
  /**
   * @const string
   */
  const CONTENT_TYPE                          = 'Content-Type';
  /**
   * @const string
   */
  const CONTENT_LENGTH                        = 'Content-Length';
  /**
   * @const string
   */
  const KEEP_ALIVE                            = 'Keep-Alive';
  /**
   * @const string
   */
  const CACHE_CONTROL                         = 'Cache-Control';
  /**
   * @const string
   */
  const X_SOCKET                              = 'X-Socket';
  /**
   * @const string
   */
  const X_SOCKET_ID                           = 'X-Socket-Id';
  /**
   * @const string
   */
  const SET_COOKIE                            = 'Set-Cookie';

  /**
   * 
   * @param string $name
   * @param string $val
   * @param int $expires
   * @param string $path
   * @param string $domain
   * @param bool $secure
   * @param bool $httpOnly
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function cookie($name, $val, $expires = 0, $path = '/', $domain = null, $secure = false, $httpOnly = false){
    $header                                   = &$this->ref(self::OPT_HEADER);
    if(!isset($header[self::SET_COOKIE])){
      $header[self::SET_COOKIE]               = [];
    }
    $val                                      = urlencode($val);
    $cookie                                   = " $name=$val";
    if($expires > 0){
      $expires                                = gmdate('D, d M Y H:i:s \G\M\T', time() + $expires);
    }
    $cookie                                   .= "; Expires=$expires; Path=$path";
    if(!empty($domain)){
      $cookie .= "; Domain=$domain";
    }
    if($secure){
      $cookie .= "; Secure";
    }
    if($httpOnly){
      $cookie .= "; HttpOnly";
    }
    $header[self::SET_COOKIE][]               = $cookie;
  }

}
