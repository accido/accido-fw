<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Injector;
use Accido\Option;
use Accido\Models\Request;
use Accido\Models\Router;
use Accido\Models\Action\Init\Server;
use Accido\Models\Socket\Session;
use Accido\Models\Response\Http\Header as HTTPHeader;
use Accido\Models\Render\Socket\Relate as SR;
use stdclass;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Socket
 *
 * @package Server
 * @subpackage Model
 * 
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Socket extends Model {
  /**
   * @const string
   */
  const ATTR_READ_LENGTH                                  = 1024;
  /**
   * @const string
   */
  const ATTR_WRITE_LENGTH                                 = 1024;
  /**
   * @const string
   */
  const ATTR_SOCKET_TIMEOUT                               = 17.0;
  /**
   * @const string
   */
  const ATTR_BROKEN_PIPE                                  = -1;
  /**
   * @const string
   */
  const OPT_SOCKET                                        = 'socket';
  /**
   * @const string
   */
  const OPT_REQUEST                                       = 'request';
  /**
   * @const string
   */
  const OPT_DATA                                          = 'data';
  /**
   * @const string
   */
  const OPT_CONTENT                                       = 'content';
  /**
   * @const string
   */
  const OPT_SERVER                                        = 'server';
  /**
   * @const string
   */
  const OPT_NAME_REMOTE                                   = 'name';
  /**
   * @const string
   */
  const OPT_NAME_LOCAL                                    = 'local';
  /**
   * @const string
   */
  const OPT_TIMER                                         = 'timer';
  /**
   * @const stirng
   */
  const OPT_READ                                          = 'read';
  /**
   * @const stirng
   */
  const OPT_WRITE                                         = 'write';
  /**
   * @const string
   */
  const OPT_CLOSED                                        = 'closed';

  protected $vars                                         = [
    self::OPT_SOCKET                                      => null,
    self::OPT_REQUEST                                     => null,
    self::OPT_DATA                                        => null,
    self::OPT_CONTENT                                     => null,
    self::OPT_SERVER                                      => null,
    self::OPT_NAME_REMOTE                                 => null,
    self::OPT_NAME_LOCAL                                  => null,
    self::OPT_TIMER                                       => null,
    self::OPT_READ                                        => null,
    self::OPT_WRITE                                       => null,
    self::OPT_CLOSED                                      => false,
  ];

  public function capture(Injector $dic, Server $server, $item){
    $this[self::OPT_SERVER]                               = $server;
    $this[self::OPT_READ]                                 = $this->read_generator();
    $this[self::OPT_WRITE]                                = $this->write_generator();
    $this->connect($item);
  }

  /**
   * connect
   * 
   * @param resource $socket
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function connect($socket){
    $this[self::OPT_SOCKET]                               = $socket;
    $remote                                               = stream_socket_get_name($socket, true);
    $this[self::OPT_NAME_REMOTE]                          = $remote;
    $this[self::OPT_NAME_LOCAL]                           = stream_socket_get_name($socket, false);
    $this[self::OPT_TIMER]                                = microtime(true);
  }

  /**
   * read_generator
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return \Generator
   */
  public function read_generator(){
    $data                                                 = &$this->ref(self::OPT_DATA);
    $socket                                               = $this[self::OPT_SOCKET];
    $server                                               = $this[self::OPT_SERVER];
    $sync                                                 = $this->register('Synchronize');
    $read                                                 = &$server->ref(Server::OPT_READ);
    $write                                                = &$server->ref(Server::OPT_WRITE);
    $content                                              = &$this->ref(self::OPT_CONTENT);
    $start                                                = $this[self::OPT_TIMER];
    $name                                                 = (int)$socket;
    $closed                                               = &$this->ref(self::OPT_CLOSED);
    $length                                               = 0;
    $position                                             = 0;
    $end                                                  = false;
    while(true){
      $temp                                               = yield;
      $timeout                                            = (microtime(true) - $start) > self::ATTR_SOCKET_TIMEOUT;
      $remote                                             = stream_socket_get_name($socket, true);
      if(empty($remote)){
        unset($write[$name]);
        $timeout                                          = true;
      }
      if($timeout){
        unset($read[$name]);
        $this->disconnect();
        return;
      }
      $part                                               = stream_socket_recvfrom($socket, self::ATTR_READ_LENGTH);
      $data                                               .= $part;
      if((!$length && ($position = strpos($data, "\r\n\r\n"))) ||
        ($length && $length === strlen($data)))
      {
        if(!$length){
          $request                                        = new Request();
          $request->resolve_http_header($data, $this);
          $length                                         = intval($request->get_header('Content-Length'));
          $data                                           = substr($data, $position + 4);
          if (strlen($data) < $length) continue;
        }
        $request->resolve_http_body($data);
        $type                                             = $request->get_header(HTTPHeader::X_SOCKET);
        if(HTTPHeader::TYPE_RELATE === $type){
          $method                                         = strtolower($request['server.REQUEST_METHOD']);
          $session                                        = $this->register_model('Socket/Session', $request[Request::OPT_COOKIE_VARS . '.' . SR::ATTR_RESPONSE_COOKIE]);
          if('get' === $method){
            $unique                                       = $request[Request::OPT_COOKIE_VARS . '.' . SR::ATTR_UNIQUE_COOKIE];
            $session->connect($this, $unique);
            continue;
          }
          if($length && 'post' === $method){
            $unique                                       = $request[Request::OPT_COOKIE_VARS . '.' . SR::ATTR_UNIQUE_COOKIE];
            $session->unpack($data, $sync, $unique);
            $data                                         = '';
            $length                                       = 0;
            unset($read[$name]);
            $write[$name]                                 = $socket;
            $content                                      = <<<EOC
HTTP/1.1 200 Ok
Connection: closed

EOC;
            $this->disconnect();
            return;
          }
          continue;
        }
        $router                                           = new Router();
        $data                                             = '';
        $length                                           = 0;
        unset($read[$name]);
        $router->run($request);
          //->head()
          //->destroy();
        continue;
      }
    }
  }

  /**
   * write_generator
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return \Generator
   */
  public function write_generator(){
    $content                                              = &$this->ref(self::OPT_CONTENT);
    $socket                                               = $this[self::OPT_SOCKET];
    $server                                               = $this[self::OPT_SERVER];
    $read                                                 = &$server->ref(Server::OPT_READ);
    $write                                                = &$server->ref(Server::OPT_WRITE);
    $name                                                 = (int)$socket;
    $closed                                               = &$this->ref(self::OPT_CLOSED);
    while(true){
      $temp                                               = yield;
      $size                                               = strlen($content);
      $count                                              = stream_socket_sendto($socket, $content, 0);
      if($count < $size && 0 < $count){
        $content                                          = substr($content, $count);
        continue;
      }
      if(self::ATTR_BROKEN_PIPE === $count){
        unset($read[$name]);
        $closed                                           = true;
      }
      $content                                            = null;
      unset($write[$name]);
      if($closed && $this->disconnect()){
        return;
      }
    }
  }

  /**
   * send
   * 
   * @param mixed $str
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function send($str){
    $content                                              = &$this->ref(self::OPT_CONTENT);
    $server                                               = $this[self::OPT_SERVER];
    $socket                                               = $this[self::OPT_SOCKET];
    $name                                                 = (int)$socket;
    $write                                                = &$server->ref(Server::OPT_WRITE);
    $content                                              .= $str;
    $write[$name]                                         = $socket;
  }

  /**
   * disconnect
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function disconnect(){
    if(empty($this->vars)){
      return true;
    }
    $server                                               = $this[self::OPT_SERVER];
    $socket                                               = $this[self::OPT_SOCKET];
    $name                                                 = (int)$socket;
    $read                                                 = &$server->ref(Server::OPT_READ);
    $write                                                = &$server->ref(Server::OPT_WRITE);
    $this[self::OPT_CLOSED]                               = true;
    if(!isset($read[$name]) && !isset($write[$name])){
      $this->close();
      return true;
    }
    return false;
  }

  /**
   * close
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function close(){
    if(!isset($this[self::OPT_SOCKET])) return;
    $socket                                               = $this[self::OPT_SOCKET];
    stream_socket_shutdown($socket, STREAM_SHUT_RDWR);
    fclose($socket);
    $this->destroy();
  }

  /**
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  public function alive(){
    return isset($this[self::OPT_CLOSED]) && !$this[self::OPT_CLOSED];
  }

}
