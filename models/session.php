<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Event;
use Accido\View;
use Accido\Models\Request;
use Accido\HashMap;
use Accido\SessionAdapter;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Session
 *
 * @package Session
 * @subpackage Model
 *
 * 
 * @see Model
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
class Session extends Model{

  const ATTR_SESSION                                        = 'session';

  const OPT_SESSION_OPTIONS                                 = 'option';

  const OPT_SESSION_DATA                                    = 'data';

  const OPT_SESSION_DIRTY                                   = 'dirty';

  const OPT_DEFAULT_OPTIONS                                 = 'default';

  const OPT_SESSION_REMOTE_ADDR                             = 'remote_addr';

  const OPT_SESSION_COOKIE_DOMAIN                           = 'domain';

  const OPT_SESSION_COOKIE_PATH                             = 'path';

  const OPT_SESSION_USER_AGENT                              = 'user-agent';

  const OPT_SESSION_ID                                      = 'id';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                                           = array(
    self::OPT_EVENTS                                        => array(
      'controllers_shutdown'                                => Event::ATTR_NORMAL_EVENT_PRIORITY,
    ),
    self::OPT_SESSION_OPTIONS                               => null,
    self::OPT_SESSION_DATA                                  => array(),
    self::OPT_SESSION_DIRTY                                 => array(),
    self::OPT_DEFAULT_OPTIONS                               => array(
      'cookie_name'                                         => 'ASESSION',
      'expiration'                                          => 600,
      'time_to_update'                                      => 300,
      'match_ip'                                            => true,
      'match_useragent'                                     => true,
      'size'                                                => 13900,
      'name'                                                => 'core',
      'end_on_close'                                        => true,
    ),
    self::OPT_SESSION_REMOTE_ADDR                           => '',
    self::OPT_SESSION_COOKIE_DOMAIN                         => '',
    self::OPT_SESSION_COOKIE_PATH                           => '',
    self::OPT_SESSION_USER_AGENT                            => '',
    self::OPT_SESSION_ID                                    => null,
  );

  /**
   * init
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  protected function init(){
    $helper                                                 = $this->register_model('Helper');
    $options                                                = $helper[self::ATTR_SESSION];
    if(!$options){
      $options                                              = $this->get(self::OPT_DEFAULT_OPTIONS);
      $helper->set(self::ATTR_SESSION, $options);
      $helper->save();
    }
    $this->set(self::OPT_SESSION_OPTIONS, $options);
    $request                                                = $this->register_model('Request');
    $request->init_global();
    $this->set(self::OPT_SESSION_REMOTE_ADDR, $request['server.REMOTE_ADDR']);
    $this->set(self::OPT_SESSION_USER_AGENT, $request->get_header('User-Agent'));
    $this->set(self::OPT_SESSION_COOKIE_DOMAIN, $request['server.SERVER_NAME']);
    $this->set(self::OPT_SESSION_COOKIE_PATH, $request[Request::OPT_REQUEST_PATH]);
  }

  /**
   * validate_session
   *
   * @param mixed $name
   * @param mixed $cookie_session
   * @throws \Accido\Exceptions\Session
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function validate_session($name, $cookie_session){
    $cache                                                  = $this->register_model('Cache');
    $data                                                   = $cache->get_from_cache($cookie_session, false, array('session', 'session-' . $name));
    if($data instanceof HashMap){
      if(!isset($data[new \Accido\StringBinary('session_timestamp')]) || !isset($data[new \Accido\StringBinary('session_id')])){
        throw new \Accido\Exceptions\Session;
      }
      $timestamp                                            = $data[new \Accido\StringBinary('session_timestamp')];
      $timestamp                                            = $timestamp->__toString();
      $session_id                                           = $data[new \Accido\StringBinary('session_id')];
      $session_id                                           = $session_id->__toString();
      $cookie_temp                                          = $timestamp . '`' . $session_id . '`' . $name;
      if($this['option.match_ip']){
        $cookie_temp                                        .= '`' . $this[self::OPT_SESSION_REMOTE_ADDR];
      }
      if($this['option.match_useragent']){
        $cookie_temp                                        .= '`' . $this[self::OPT_SESSION_USER_AGENT];
      }
      if($cookie_session !== md5($cookie_temp)){
        throw new \Accido\Exceptions\Session;
      }
      $this->set('dirty.' . $name . '.cache', $cookie_session);
      $this->set(self::OPT_SESSION_ID, $cookie_session);
      return $data;
    }
    else{
      throw new \Accido\Exceptions\Session;
    }
  }

  /**
   * create_session
   *
   * @param mixed $name
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return HashMap|null
   */
  public function create_session($name){
    $factory                                                = new \Accido\StringBinaryFactory;
    $data                                                   = new HashMap($factory, $factory, $this['option.size']);
    $timestamp                                              = time();
    $session_id                                             = uniqid();
    $data[new \Accido\StringBinary('session_timestamp')]    = new \Accido\StringBinary($timestamp . '');
    $data[new \Accido\StringBinary('session_id')]           = new \Accido\StringBinary($session_id . '');
    $cookie_session                                         = $timestamp . '`' . $session_id . '`' . $name;
    if($this['option.match_ip']){
      $cookie_session                                       .= '`' . $this[self::OPT_SESSION_REMOTE_ADDR];
    }
    if($this['option.match_useragent']){
      $cookie_session                                       .= '`' . $this[self::OPT_SESSION_USER_AGENT];
    }
    $cookie_session                                         = md5($cookie_session);
    $expiry                                                 = $this['option.end_on_close'] ? 0 : time() + $this['option.expiration'];
    if (setrawcookie($this['option.cookie_name'] . '-' . strtoupper($name), 
            $cookie_session,
            $expiry,
            $this[self::OPT_SESSION_COOKIE_PATH],
            $this[self::OPT_SESSION_COOKIE_DOMAIN],
            false, true)){
      $cache                                                = $this->register_model('Cache');
      $cache->set_to_cache($cookie_session, $data, array('session', 'session-' . $name), $this['option.expiration']);
      $this->set('dirty.' . $name . '.cache', $cookie_session);
      $this->set(self::OPT_SESSION_ID, $cookie_session);
      return $data;
    }
    throw new \Accido\Exceptions\Session('Session setups failed.');
  }

  /**
   * start
   *
   * @param string $name
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function start($name = ''){
    $name                                                     = empty($name) ? $this['option.name'] : $name;
    $request                                                  = $this->register_model('Request');
    $this->register_param($this->vars, self::OPT_SESSION_DIRTY . '.' . $name, array('cache' => '', 'status' => false));
    $this->register_param($this->vars,
        self::OPT_SESSION_DATA . '.' . $name,
        $this->reuse(function($container, $name) use ($request){
          return $request->stream('cookie.' . $container['option.cookie_name'] . '-' . strtoupper($name))
            ->then(function($scookie) use ($container, $name){
              return $container->validate_session($name, $scookie);
            })
            ->fail(function() use ($container, $name){
              return $container->create_session($name);
            })->storage->detail;
    }));
    $session                                                  = $this->call(self::OPT_SESSION_DATA . '.' . $name, $name);
    $return                                                   = new SessionAdapter;
    $dirty                                                    = &$this->vars[self::OPT_SESSION_DIRTY];
    $return->write                                            = function($value_key, $value_data) use ($session, &$dirty, $name){
      $dirty[$name]['status']                                 = true;
      if(!($value_key instanceof Binary)){
        $value_key                                            = new \Accido\StringBinary((string)$value_key);
      }
      if(!($value_data instanceof Binary)){
        $value_data                                           = new \Accido\StringBinary((string)$value_data);
      }
      return $session[$value_key]                             = $value_data;
    };
    $return->read                                             = function($value_key) use ($session){
      if(!($value_key instanceof Binary)){
        $value_key                                            = new \Accido\StringBinary((string)$value_key);
      }
      return $session[$value_key];
    };
    $return->id                                               = $this[self::OPT_SESSION_ID];

    return $return;

  }

  /**
   * event_controllers_shutdown
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function event_controllers_shutdown(){
    foreach($this[self::OPT_SESSION_DIRTY] as $session_name => $session_dirty){
      if($session_dirty['status']){
        $cache                                            = $this->register_model('Cache');
        $session_data                                     = $this->call(self::OPT_SESSION_DATA . '.' . $session_name, $session_name);
        $cache->set_to_cache($session_dirty['cache'], $session_data, array('session', 'session-' . $session_name), $this['option.expiration']);
      }
    }
  }

}
