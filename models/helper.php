<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Event;
use Accido\Models\DB;
use Accido\Models\Application;
use Accido\View;
use Exception;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Helper 
 * 
 * @uses Model
 * @package 
 * @version $id$
 * @copyright 2013 Accido
 * @author Andrew Scherbakov <kontakt.asch@gmail.com> 
 * @license PHP Version 5.2 {@link http://www.php.net/license/}
 */
class Helper extends Model{

  const OPT_APPLICATION                   = 'application';

  /**
   * OPT_INSTALLED
   *
   * @const string
   */
  const OPT_INSTALLED                     = DB::OPT_INSTALLED;

  /**
   * vars
   *
   * @var array
   */
  protected $vars                         = array(
    self::OPT_EVENTS                      => array(
      'controllers_shutdown'              => Event::ATTR_LOW_EVENT_PRIORITY,  
    ),
  );

  /**
   * dirty 
   * 
   * @var mixed
   * @access private
   */
  private $dirty = FALSE;

  // protected init() {{{ 
  /**
   * init
   * 
   * @access protected
   * @return void
   */
  protected function init(){
    try{
      $vars = @include PROJECT_ROOT . '/helper.php';
    }
    catch( Exception $e ){
    }
    $this->vars = empty($vars) ? array() : $vars;
  }
  // }}}

  // public __set(key,value) {{{ 
  /**
   * __set
   * 
   * @param string|int $key 
   * @param mixed $value 
   * @access public
   * @return mixed
   */
  public function __set( $key, $value ){
    $this->dirty = TRUE;
    if(!$this->register_param($this->vars, $key, $value))
      parent::__set($key, $value);
    return $value;
  }
  // }}}

  /**
   * __get
   *
   * @param mixed $key
   * @uses vars
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return mixed
   */
  public function __get($key){
    if($this->register_param($this->vars, $key, self::ATTR_NULL_PARAMETER))
      return self::ATTR_NULL_PARAMETER;
    else
      return parent::__get($key);
  }

  // public save() {{{ 
  /**
   * save
   * 
   * @access public
   * @throws Exceptions_Helper Cann't open file
   * @return void
   */
  public function save(){
    if ( $this->dirty ){
      $file = fopen( PROJECT_ROOT . '/helper.php', 'wb');
      $this->ensure( !is_resource( $file ), 'Cann\'t open config file for write.' ); 
      $code = '<?php return ' . var_export( $this->vars, TRUE ) . ';';
      fwrite( $file, $code );
      fclose( $file );
    }
  }
  // }}}

  /**
   * event_controllers_shutdown
   *
   * @param View $view
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   */
  public function event_controllers_shutdown(View $view){
    $this->save();
  }

}
