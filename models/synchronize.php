<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Injector;
use Accido\Controller;
use Accido\Models\Socket\Session as SocketSession;
use Accido\Models\Action\Init\Server;
use Closure;

class Synchronize extends Model {

  /**
   * @const string
   */
  const ATTR_HEADER_BLOCK         = 4;
  /**
   * @const string
   */
  const ATTR_HEADER_FORMAT        = 'V';
  /**
   * @const string
   */
  const ATTR_OUT                  = 'out';
  /**
   * @const string
   */
  const ATTR_IN                   = 'in';
  /**
   * @const string
   */
  const ATTR_DEL                  = 'del';
  /**
   * @const string
   */
  const ATTR_MOVE                 = 'move';
  /**
   * @const string
   */
  const ATTR_TRANSACTION          = 'tr';
  /**
   * @const string
   */
  const ATTR_RESOURCE_NAME        = 'sync/process';
  /**
   * @const string
   */
  const ATTR_RESOURCE_EXT         = '.sock';
  /**
   * @const int
   */
  const ATTR_RESOURCE_DIR_PERM    = 0775;
  /**
   * @const int
   */
  const ATTR_READ_SIZE            = 16;
  /**
   * @const Closure
   */
  const OPT_CALLBACK              = 'callback';
  /**
   * @const string
   */
  const OPT_INCOME                = 'income';
  /**
   * @const string
   */
  const OPT_LISTEN                = 'listen';
  /**
   * @const string
   */
  const OPT_OUTCOME               = 'outcome';
  /**
   * @const string
   */
  const OPT_POINTER               = 'pointer';
  /**
   * @const string
   */
  const OPT_CURRENT_DATA          = 'data';
  /**
   * @const string
   */
  const OPT_CURRENT_SIZE          = 'size';
  /**
   * @const string
   */
  const OPT_ID                    = 'id';
  /**
   * @const string
   */
  const OPT_MAX_ID                = 'max_id';
  /**
   * @const string
   */
  const OPT_TRANSACTION           = 'transaction';

  protected $vars                 = [
    self::OPT_CALLBACK            => null,
    self::OPT_INCOME              => null,
    self::OPT_LISTEN              => [],
    self::OPT_OUTCOME             => [],
    self::OPT_POINTER             => 0,
    self::OPT_CURRENT_DATA        => [],
    self::OPT_ID                  => 0,
    self::OPT_MAX_ID              => 0,
    self::OPT_TRANSACTION         => [],
  ];

  /**
   * 
   * @param mixed $data
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function save($data){
    $out                          = &$this->ref(self::OPT_OUTCOME);
    $data                         = serialize($data);
    $data                         = pack(self::ATTR_HEADER_FORMAT, strlen($data)) . $data;
    $id                           = $this[self::OPT_ID];
    for($index = 1; $index <= $this[self::OPT_MAX_ID]; ++$index){
      if($index === $id){
        continue;
      }
      if(!isset($out[$index])){
        $errno                    = 0;
        $error_string             = '';
        $socket                   = stream_socket_client(
          $this->url($index),
          $errno,
          $error_string,
          30,
          STREAM_CLIENT_ASYNC_CONNECT | STREAM_CLIENT_CONNECT);
        stream_set_blocking($socket, 0);
        $out[$index]              = $socket;
      }
      stream_socket_sendto($out[$index], $data);
    }
  }

  /**
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return bool
   */
  private function do_check(){
    $data                         = &$this->ref(self::OPT_CURRENT_DATA);
    $server                       = $this[self::OPT_INCOME];
    $listen = $temp               = $this[self::OPT_LISTEN];
    array_unshift($listen, $server);
    $write                        = [];
    $except                       = [];
    if(stream_select($listen, $write, $except, 0)){
      foreach($listen as $item){
        if($item === $server){
          $item                   = stream_socket_accept($server, 0);
          stream_set_blocking($item, 0);
          stream_set_timeout($item, 1);
          $temp[]                 = $item;
          $this[self::OPT_LISTEN] = $temp;
        }
        $index                    = (int)$item;
        if(!isset($data[$index])){
          $data[$index]           = [
            'data'                => '',
            'size'                => false,
          ];
        }
        $data[$index]['data'] .= stream_socket_recvfrom($item, self::ATTR_READ_SIZE);
      }
    }

    $clean                        = true;
    foreach($data as &$item){
      $clean                      &= empty($item['data']);
      if((!isset($item['size']) || false === $item['size']) && self::ATTR_HEADER_BLOCK <= strlen($item['data'])){
        $size                     = substr($item['data'], 0, self::ATTR_HEADER_BLOCK);
        $size                     = unpack(self::ATTR_HEADER_FORMAT, $size);
        $size                     = $size[1];
        $item['data']             = substr($item['data'], self::ATTR_HEADER_BLOCK);
        $item['size']             = $size;
      }
      elseif(!isset($item['size']) || false === $item['size']){
        continue;
      }
      if(strlen($item['data']) >= $item['size']){
        $message                  = substr($item['data'], 0, $item['size']);
        $item['data']             = substr($item['data'], $item['size']);
        $item['size']             = false;
        $this->relate($message);
      }
    }

    $trans                        = &$this->ref(self::OPT_TRANSACTION);
    if($clean && !empty($trans)){
      foreach($trans as $item){
        $this->save([
          'mode'                  => self::ATTR_TRANSACTION,
          'name'                  => $item['name'],
          'params'                => $item['params'],
        ]);
      }
      $trans                      = [];
    }
  }

  /**
   * 
   * @param string $data
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return
   */
  private function relate($data){
    $data                         = unserialize($data);
    switch($data['mode']){
      case self::ATTR_OUT: 
        $session                  = $this->register_model('Socket/Session', $data['id']);
        $session->do_unpack($data['data'], $data['unique']);
        break;
      case self::ATTR_TRANSACTION:
        $trans                    = $this->register_model('Transaction', $data['name']);
        $trans->do_run($data['params']);
        break;
      case self::ATTR_IN:
        $session                  = $this->register_model('Socket/Session', $data['id']);
        $session->do_pack($data['data']);
        break;
      case self::ATTR_DEL:
        $session                  = $this->register_model('Socket/Session', $data['id']);
        $session->do_derelate($data['unique']);
        break;
      case self::ATTR_MOVE:
        $session                  = $this->register_model('Socket/Session', $data['id']);
        $session->do_move($data['session'], $data['unique']);
        break;
    }
  }

  /**
   * 
   * @param mixed $id
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  private function filename($id){
    return PROJECT_ROOT . '/' . self::ATTR_RESOURCE_NAME . '.' . $id . self::ATTR_RESOURCE_EXT;
  }

  /**
   * 
   * @param mixed $id
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return string
   */
  private function url($id){
    return 'unix://' . $this->filename($id);
  }

  /**
   * 
   * @param Server $server
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function resource($id, $count){
    $this[self::OPT_ID]           = $id;
    $this[self::OPT_MAX_ID]       = $count;
    $file                         = $this->filename($id);
    $dir                          = dirname($file);
    if(!is_dir($dir)){
      mkdir($dir, self::ATTR_RESOURCE_DIR_PERM, true);
    }
    $errno                        = 0;
    $error_string                 = '';
    @unlink($file);
    $socket                       = stream_socket_server(
      $this->url($id),
      $errno,
      $error_string,
      STREAM_SERVER_BIND | STREAM_SERVER_LISTEN);
    stream_set_blocking($socket, 0);
    $this[self::OPT_INCOME]       = $socket;
    $this[self::OPT_ID]           = $id;
    $this[self::OPT_MAX_ID]       = $count;
  }

  /**
   * 
   * @param SocketSession $rel
   * @param string $raw
   * @param string $unique
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function pull(SocketSession $rel, $raw, $unique){
    $data                           = [
      'id'                          => $rel[SocketSession::OPT_ID],
      'mode'                        => self::ATTR_OUT,
      'data'                        => $raw,
      'unique'                      => $unique,
    ];
    $this->save($data);
  }

  /**
   * 
   * @param SocketSession $rel
   * @param string $raw
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function push(SocketSession $rel, $raw){
    $data                           = [
      'id'                          => $rel[SocketSession::OPT_ID],
      'mode'                        => self::ATTR_IN,
      'data'                        => $raw,
    ];
    $this->save($data);
  }

  /**
   * 
   * @param SocketSession $rel
   * @param mixed $unique
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function derelate(SocketSession $rel, $unique){
    $data                           = [
      'id'                          => $rel[SocketSession::OPT_ID],
      'mode'                        => self::ATTR_DEL,
      'unique'                      => $unique,
    ];
    $this->save($data);
  }

  /**
   * 
   * @param SocketSession $rel
   * @param string $sess
   * @param string $unique
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function move(SocketSession $rel, $sess, $unique){
    $data                           = [
      'id'                          => $rel[SocketSession::OPT_ID],
      'mode'                        => self::ATTR_MOVE,
      'unique'                      => $unique,
      'session'                     => $sess,
    ];
    $this->save($data);
  }

  /**
   * 
   * @since $id$
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function check(){
    $this->do_check();
  }

}
