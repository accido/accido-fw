<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Injector;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Argument
 *
 * @package Injector
 * @subpackage Model
 * 
 * @final
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
final class Argument extends Model {
  /**
   * __related
   *
   * @var string
   */
  public $__related                                               = null;
  /**
   * __instance
   *
   * @var Model
   */
  public $__instance                                              = null;
  /**
   * __index
   *
   * @var int
   */
  public $__index                                                 = null;
  /**
   * __hash
   *
   * @var string
   */
  public $__hash                                                  = null;
  /**
   * __similar
   *
   * @var array
   */
  public $__similar                                               = array();

  public function capture(Injector $dic, $related, array $args, $index = null){
    $this->vars                                                   = $args;
    $this->__related                                              = $related;
    $this->__hash                                                 = spl_object_hash($this);
    $this->injector                                               = $dic;
    $this->__index                                                = null === $index ? $dic->get_hash($args) : $index;
  }

  /**
   * remove
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function remove(){
    unset($this->injector->related[$this->__hash]);
    unset($this->injector->singletons[$this->__related][$this->__index]);
    unset($this->injector->args[$this->__instance->instance_hash]);
  }

  /**
   * @return void
   */
  public function destroy(){
    $this->remove();
    $this->injector                                               = null;
    $this->vars                                                   = null;
    $this->instance_args                                          = null;
    $this->__similar                                              = null;
    $this->__instance                                             = null;
  }

  /**
   * relate
   * 
   * @param object $instance
   * @uses $__instance, $__hash, $__related, $__index, $instance_args
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function relate($instance){
    $this->do_relate($instance); 
    if(isset($this->injector->related[$this->__hash])){
      $this->__similar[]                                          = $instance;
      return;
    }
    $this->__instance                                             = $instance;
    $this->injector->singletons[$this->__related][$this->__index] = $this;
    $this->injector->related[$this->__hash]                       = $instance;
  }

  /**
   * do_relate
   * 
   * @param mixed $instance
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function do_relate($instance){
    $is_object                                                    = is_object($instance);
    if($is_object && !empty($instance->instance_hash) && isset($this->injector->args[$instance->instance_hash])){
      $oldest                                                     = $this->injector->args[$instance->instance_hash];
      $oldest->remove();
    }
    if($is_object){
      if($this !== $instance->instance_args){
        $instance->is_instance_related                            = false;
      }
      $hash                                                       = null;
      if(empty($instance->instance_hash)){
        $hash = $instance->instance_hash                          = spl_object_hash($instance);
      }
      else{
        $hash                                                     = $instance->instance_hash;
      }
      $this->injector->args[$hash]                                = $this;
      $instance->instance_args                                    = $this;
    }
  }

  /**
   * recapture
   * 
   * @uses $__instance,$vars
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  public function recapture(){
    if(!$this->__instance->is_instance_related){
      call_user_func_array(array($this->__instance, 'capture'), $this->vars);
      $this->__instance->is_instance_related                      = true;
    }
    foreach($this->__similar as $item){
      if(is_object($item) && !$item->is_instance_related){
        call_user_func_array(array($item, 'capture'), $this->vars);
        $item->is_instance_related                                = true;
      }
    }
  }

}
