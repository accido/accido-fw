<?php
namespace Accido\Models;
use Accido\Model;
use Accido\Option;
use Accido\Controller;
use Accido\Async as AsyncInterface;
use Generator;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Async
 *
 * @package Async
 * @subpackage Model
 * 
 * 
 * @see \Accido\Async
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Async extends Model implements AsyncInterface {

  /**
   * queue
   *
   * @var array
   */
  public $queue                                             = [];

  /**
   * timeout in mcs
   *
   * @var int
   */
  public $timeout                                           = self::ATTR_TICK_MICROSEC;
  /**
   * factory
   *
   * @var Generator
   */
  public $factory                                           = null;
  /**
   * tick
   *
   * @var Generator
   */
  public $tick                                              = null;
  /**
   * async or not
   *
   * @var bool
   */
  public $async                                             = self::ATTR_ASYNC;
  /**
   * halt application
   *
   * @var bool
   */
  public $is_halt                                           = false;
  /**
   * memory limit
   *
   * @var string
   */
  public $memory_limit                                      = self::ATTR_512M;

  public function __invoke(Controller $ctrl){
    $_this                                                  = $this;
    $ctrl->commit()->then(function(Controller $ctrl) use(&$_this) {
      $_this->run();
      return $ctrl;
    });
    return $ctrl;
  }

  public function run(){
    ini_set('max_execution_time', 0);
    ini_set('memory_limit', $this->memory_limit);
    ini_set('implicit_flush', 1);
    ini_set('output_buffering', 0);
    gc_enable();
    if($this->async){
      Option::$next_tick                                    = $this->async();
    }
    $is_halt                                                = &$this->is_halt;
    $this
      ->handle('shutdown')
      ->then(function(Controller $ctrl) use (&$is_halt){
        $ctrl
          ->commit()
          ->then(function(Controller $ctrl) use (&$is_halt){
            $is_halt                                        = true;
            return $ctrl;
          });
      });
    $this->factory                                          = $this->factory();
    $this->tick                                             = $this->tick();
    $this->listen();
  }

  public function factory(){
    yield;
  }

  public function listen(){
    $is_halt                                                = &$this->is_halt;
    while(!$is_halt){
      $this->next();
    }
    yield;
  }

  public function shutdown(){

  }
  
  public function tick(){
    $queue                                                  = &$this->queue;
    $executed                                               = [];
    $time                                                   = 0;
    $lock                                                   = 0;
    $tick_timeout                                           = $this->timeout / 1000000;
    while(true){
      yield;
      $current                                              = (float)time() + (float)microtime();
      if($current - $time < $tick_timeout){
        continue;
      }
      $time                                                 = $current;
      $executed                                             = $queue;
      $queue                                                = [];
      foreach($executed as $item){
        call_user_func($item);
      }
    }
  }

  public function async(){
    $queue                                                  = &$this->queue;
    while(true){
      $item                                                 = yield;
      if(!empty($item)){
        $queue[]                                            = $item;
      }
    }
  }

  public function next(){
    if($this->async){
      $this->tick->next();
    }
  }

}
