<?php
namespace Accido\Models\Action\Init;
use Accido\Model;
use Accido\Action;
use Accido\Injector;
use Accido\Controller;
use Accido\Models\Socket;
use Accido\Models\Async as AsyncApplication;
use Accido\Option;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Server
 *
 * @package Server
 * @subpackage Model
 * 
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Server extends AsyncApplication implements Action {
  /**
   * @const string
   */
  const OPT_LISTEN_PORT                                     = 'listen';
  /**
   * @const string
   */
  const OPT_SOCKET                                          = 'socket';
  /**
   * @const string
   */
  const OPT_SSL_ENABLED                                     = 'ssl';
  /**
   * @const string
   */
  const OPT_IP                                              = 'ip';
  /**
   * @const string
   */
  const OPT_HOST                                            = 'host';
  /**
   * @const string
   */
  const OPT_COUNT                                           = 'count';
  /**
   * @const string
   */
  const OPT_READ                                            = 'read';
  /**
   * @const string
   */
  const OPT_WRITE                                           = 'write';
  /**
   * @const string
   */
  const OPT_EXCEPT                                          = 'except';
  /**
   * @const string
   */
  const OPT_NAME                                            = 'name';
  /**
   * @const string
   */
  const OPT_WORKER                                          = 'worker';
  /**
   * @const string
   */
  const OPT_ID                                              = 'id';
  /**
   * @const string
   */
  const OPT_SYNCHRONIZE                                     = 'synchronize';

  protected $vars                                           = [
    self::OPT_LISTEN_PORT                                   => null,
    self::OPT_SOCKET                                        => null,
    self::OPT_SSL_ENABLED                                   => null,
    self::OPT_IP                                            => null,
    self::OPT_HOST                                          => null,
    self::OPT_NAME                                          => null,
    self::OPT_COUNT                                         => 0,
    self::OPT_READ                                          => [],
    self::OPT_WRITE                                         => [],
    self::OPT_EXCEPT                                        => [],
    self::OPT_WORKER                                        => 0,
    self::OPT_ID                                            => 0,
    self::OPT_SYNCHRONIZE                                   => null,
  ];

  protected function init(){
    $helper                                                 = $this->register_model('Helper');
    if(!isset($helper['server'])){
      $helper->set('server', [
        'listen'                                            => '80',
        'timeout'                                           => self::ATTR_TICK_MICROSEC,
        'ssl'                                               => false,
        'host'                                              => '127.0.0.1',
        'name'                                              => '127.0.0.1',
        'memory_limit'                                      => '512M',
        'async'                                             => true,
        'worker'                                            => 1,
      ]);
      $helper->save();
    }
    $option                                                 = $helper['server'];
    foreach($option as $index => $item){
      $this->$index                                         = $item;
    }
  }

  /**
   * socket_server
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return resurce
   */
  public function socket_server(){
    if($this[self::OPT_SSL_ENABLED]){
      $ctx                                                  = stream_context_create(
        ['ssl'                                              =>
          [
            'verify_peer'                                   => false,
            'allow_self_signed'                             => true
          ]
        ]);
      $errno                                                = 0;
      $error_string                                         = '';
      $socket                                               = stream_socket_server(
        'ssl://'.$this[self::OPT_HOST].':' . $this[self::OPT_LISTEN_PORT],
        $errno,
        $error_string,
        STREAM_SERVER_BIND | STREAM_SERVER_LISTEN,
        $ctx);
      stream_set_blocking($socket, 0);
      return $socket;
    }
    else{
      $errno                                                = 0;
      $error_string                                         = '';
      $socket                                               = stream_socket_server(
        'tcp://'.$this[self::OPT_HOST].':' . $this[self::OPT_LISTEN_PORT],
        $errno,
        $error_string,
        STREAM_SERVER_BIND | STREAM_SERVER_LISTEN);
      stream_set_blocking($socket, 0);
      return $socket;
    }
  }

  /**
   * socket_client
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return resurce
   */
  public function socket_client(){
    $socket                                                 = null;
    if($this[self::OPT_SSL_ENABLED]){
      $ctx                                                  = stream_context_create(
        ['ssl'                                              =>
          [
            'verify_peer'                                   => false,
            'allow_self_signed'                             => true
          ]
        ]);
      $errno                                                = 0;
      $error_string                                         = '';
      $socket                                               = stream_socket_client(
        'ssl://'.$this[self::OPT_HOST].':' . $this[self::OPT_LISTEN_PORT],
        $errno,
        $error_string,
        30,
        STREAM_CLIENT_ASYNC_CONNECT | STREAM_CLIENT_CONNECT,
        $ctx);
      stream_set_blocking($socket, 0);
      return $socket;
    }
    else{
      $errno                                                = 0;
      $error_string                                         = '';
      $socket                                               = stream_socket_client(
        'tcp://'.$this[self::OPT_HOST].':' . $this[self::OPT_LISTEN_PORT],
        $errno,
        $error_string,
        30,
        STREAM_CLIENT_ASYNC_CONNECT | STREAM_CLIENT_CONNECT);
      stream_set_blocking($socket, 0);
      return $socket;
    }
  }

  public function factory(){
    $client                                                 = [];
    $read                                                   = &$this->ref(self::OPT_READ);
    $write                                                  = &$this->ref(self::OPT_WRITE);
    $except                                                 = &$this->ref(self::OPT_EXCEPT);
    $index                                                  = 0;
    while(true){
      $data                                                 = yield;
      $control                                              = $data['control'];
      foreach($data['read'] as $index => $item){
        $name                                               = (int)$item;
        if($item === $control){
          $item                                             = stream_socket_accept($control, 0);
          if(!$item){
            continue;
          }
          stream_set_blocking($item, 0);
          stream_set_timeout($item, 1);
          $name                                             = (int)$item;
          $client[$name]                                    = $this->register_model('Socket', $this, $item);
          $read[$name]                                      = $item;
          continue;
        }
        if(isset($client[$name][Socket::OPT_READ]) && $client[$name][Socket::OPT_READ]->valid()){
          $client[$name][Socket::OPT_READ]->next();
        }
      }
      foreach($data['write'] as $item){
        $name                                               = (int)$item;
        if(isset($client[$name][Socket::OPT_WRITE]) && $client[$name][Socket::OPT_WRITE]->valid()){
          $client[$name][Socket::OPT_WRITE]->next();
        }
      }
      $except                                               = [];
    }
  }

  public function listen(){
    $control                                                = $this->socket_server();
    $cread                                                  = &$this->ref(self::OPT_READ);
    $cwrite                                                 = &$this->ref(self::OPT_WRITE);
    $cexcept                                                = &$this->ref(self::OPT_EXCEPT);
    $is_halt                                                = &$this->is_halt;
    $tick_timeout                                           = $this->timeout;
    $factory                                                = $this->factory;
    $cread[(int)$control]                                   = $control;
    $this->worker();
    $sync                                                   = $this[self::OPT_SYNCHRONIZE];
    while(!$is_halt){
      $this->next();
      $sync->check();
      $read                                                 = $cread;
      $write                                                = $cwrite;
      $except                                               = $cexcept;
      if(stream_select($read, $write, $except, 0, $tick_timeout)){
        $factory->send(array(
          'control'                                         => $control,
          'read'                                            => $read,
          'write'                                           => $write,
          'except'                                          => $except,
        ));
      }
    }
    $this->shutdown($control);
  }

  public function worker(){
    $id                                                     = &$this->ref(self::OPT_ID);
    $count                                                  = $this[self::OPT_WORKER];
    for($index = $count, $pid = 1; $index > 1 && $pid; --$index){
      $id++;
      $pid = \pcntl_fork();
      if(!$pid){
        $sync                                               = $this->register('Synchronize');
        $sync->resource($id, $count);
        $this[self::OPT_SYNCHRONIZE]                        = $sync;
      }
    }
    if($pid){
      $id++;
      $sync                                                 = $this->register('Synchronize');
      $sync->resource($id, $count);
      $this[self::OPT_SYNCHRONIZE]                          = $sync;
    }
  }

  public function shutdown($control = null){
    if(is_resource($control)){
      stream_socket_shutdown($control, STREAM_SHUT_RDWR);
      fclose($control);
    }
  }

}

