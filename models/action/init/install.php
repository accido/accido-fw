<?php
namespace Accido\Models\Action\Init;
use Accido\Model;
use Accido\Controller;
use Accido\View;
use Accido\Action;
use Accido\Models\DB;
use Accido\Models\Helper;
use Accido\Event;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Install
 *
 * @package Action
 * @subpackage Model
 * 
 * @see Action
 *
 * @final
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
final class Install extends Model implements Action{
  /**
   * OPT_MODEL_HELPER
   *
   * @const string
   */
  const OPT_MODEL_HELPER                                      = 'helper';

  /**
   * vars
   *
   * @var array
   */
  protected $vars                                             = array(
    self::OPT_EVENTS                                          => array(
      'controllers_install'                                   => Event::ATTR_EXPRESS_EVENT_PRIORITY,
      'controllers_shutdown'                                  => Event::ATTR_NORMAL_EVENT_PRIORITY,
    ),
    Helper::OPT_INSTALLED                                     => false,
    self::OPT_MODEL_HELPER                                    => null,
  );
  
  /**
   * init
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return void
   */
  protected function init(){}

  /**
   * __invoke
   * 
   * @param Controller $ctrl
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function __invoke(Controller $ctrl){
    $_this                                                    = $this;
    $ctrl->commit()->then(function(Controller $ctrl) use($_this){
      $helper                                                 = $_this->register('Helper');
      $_this[self::OPT_MODEL_HELPER]                          = $helper;
      $installed                                              = $helper->get(Helper::OPT_INSTALLED);
      $_this[Helper::OPT_INSTALLED]                           = $installed;
      if (true !== $installed){
        try{
          $dbmodel                                            = $_this->register('DB');
          $dbmodel->load_repositories();
        }
        catch(\Accido\Exceptions\Model $error){
          trigger_error($error->getMessage(), E_USER_NOTICE);
        }
        catch(\Exception $error){
          trigger_error($error->getMessage(), E_USER_NOTICE);
        }
        Controller::execute('Install');
      }
      return $ctrl;
    });
    return $ctrl;
  }

  /**
   * event_controllers_install
   *
   * @param View $view
   * @param Controller $ctrl
   * @uses OPT_MODEL_HELPER 
   * 
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license GNU GPL v3.0 http://www.gnu.org/licenses/gpl.txt
   *
   * @return void
   */
  public function event_controllers_install(View $view, Controller $ctrl){
    $_this                                                    = $this;
    $ctrl->handle()
      ->then(function(Controller $ctrl) use ($_this){
        $view                                                 = $ctrl->view;
        $view->queries                                        = array(array());
        $helper                                               = $_this->register('Helper');
        $view->type                                           = $helper->get(DB::OPT_DB_SQL_TYPE);
        $view->prefix                                         = $helper->get(DB::OPT_PREFIX);
        $view->charset                                        = $helper->get(DB::OPT_DB_CHARSET);
        return $ctrl;
      })
      ->then($this->register('Polymorph', 'Render/MultiQuery'));
  }

  /**
   * event_controllers_shutdown
   *
   * @param View $view
   * @param Controller $ctrl
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license GNU GPL v3.0 http://www.gnu.org/licenses/gpl.txt
   *
   * @return void
   */
  public function event_controllers_shutdown(View $view, Controller $ctrl){
    if (true !== $this[Helper::OPT_INSTALLED]){
      $helper                                                 = $this[self::OPT_MODEL_HELPER];
      $helper->set(Helper::OPT_INSTALLED, true);
    }
  }

}

