<?php
namespace Accido\Models\Action\Init;
use Accido\Model;
use Accido\Controller;
use Accido\Action;
defined('CORE_ROOT') or die('No direct script access.');
/**
 *  Class: Profiler
 *
 * @package Action
 * @subpackage Model
 * 
 * @see Action
 * 
 * @see Model
 * @author Andrew Scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2014 Andrew Scherbakov
 *
 * The MIT License (MIT)
 * Copyright (c) 2014 Andrew Scherbakov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class Profiler extends Model implements Action{

  /**
   * __invoke
   * 
   * @param Controller $ctrl
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2014 andrew scherbakov
   * @license MIT http://opensource.org/licenses/MIT
   *
   * @return Controller
   */
  public function __invoke(Controller $ctrl){
    if(function_exists('xhprof_enable')){
      $_this                                                    = $this;
      $ctrl->commit()->then(function(Controller $ctrl) use($_this){
        register_shutdown_function(array($_this, 'shutdown'));
        return $ctrl;
      });
      xhprof_enable(XHPROF_FLAGS_CPU);
    }
    return $ctrl;
  }

  /**
   * shutdown
   *
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   * @license GNU GPL v3.0 http://www.gnu.org/licenses/gpl.txt
   *
   * @return void
   */
  public function shutdown(){
    # Останавливаем профайлер
    $xhprof_data                                                = xhprof_disable();

    # Сохраняем отчет и генерируем ссылку для его просмотра
    include_once "xhprof/xhprof_lib/utils/xhprof_lib.php";
    include_once "xhprof/xhprof_lib/utils/xhprof_runs.php";
    $xhprof_runs                                                = new \XHProfRuns_Default();
    $run_id                                                     = $xhprof_runs->save_run($xhprof_data, "xhprof_test");
    echo "Report: <a href='/xhprof/index.php?run=$run_id&source=xhprof_test' target='_blank'>profiler</a>"; # Хост, который Вы настроили ранее на GUI профайлера
  }

}
