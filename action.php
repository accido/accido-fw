<?php 
namespace Accido;
defined('CORE_ROOT') or die('No direct script access.');
/**
 * Class: Action
 *
 * @package Action
 *
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 * @license MIT http://opensource.org/licenses/MIT
 */
interface Action{
}
